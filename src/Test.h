#pragma once
#include <filesystem>

namespace Tmpl8 {

	struct TestPoint {
		float x, y;
		uint color;
		TestPoint(float x, float y, uint color) : x(x), y(y), color(color) {}
	};

	struct TestRotation {
		uint p1, p2;
		float rotation;
		TestRotation(uint p1, uint p2, float rotation) : p1(p1), p2(p2), rotation(rotation) {}
	};

	class Test
	{
		public:
			static std::tuple<std::vector<Spiroplot2D>, std::vector<string>> RunConfig(std::vector<TestPoint> points, std::vector<TestRotation> rotations, int pointSize, bool exportDataPoints, string dir, int iterations)
			{
				PlotState plotState = LIFO;

				std::vector<Spiroplot2D> spiroplots = std::vector<Spiroplot2D>();
				std::vector<string> fileNames = std::vector<string>();
				std::string out_dir = dir;

				if (exportDataPoints)
				{
					out_dir += "datapoints/";

					if (!std::filesystem::exists(out_dir))
						std::filesystem::create_directories(out_dir);
				}

				for (int i = 0; i < static_cast<int>(PrecisionLevel::EnumSize); i++)
				{
					PrecisionLevel precision = (PrecisionLevel)i;

					Spiroplot2D spiroplot2D = Spiroplot2D(800, 800, pointSize, plotState);
					//Spiroplot2D spiroplot2D = Spiroplot2D(1200, 1200, pointSize);

					for (auto point = points.begin(); point != points.end(); ++point) {
						spiroplot2D.AddPoint(point->x, point->y, point->color, precision);
					}

					for (auto rotation = rotations.begin(); rotation != rotations.end(); ++rotation) {
						spiroplot2D.AddRotation(&spiroplot2D.points[rotation->p1], &spiroplot2D.points[rotation->p2], precision, rotation->rotation);
					}
					
					// DELETE LATER
					//spiroplot2D.CenterPoints(precision);

					if(!exportDataPoints)
						Iterate(iterations, spiroplot2D);
					else
						IterateAndExport(iterations, spiroplot2D, out_dir, precision);

					spiroplots.push_back(spiroplot2D);
					fileNames.push_back(GenerateFileName(precision));
				}

				// Export config file
				generateConfigFile(points, rotations, pointSize, exportDataPoints, dir, iterations);

				return std::make_tuple(spiroplots, fileNames);;
			}

			static void generateConfigFile(std::vector<TestPoint> points, std::vector<TestRotation> rotations, int pointSize, bool exportDataPoints, string dir, int iterations)
			{
				string fileName = "config.txt";
				std::ofstream outfile(dir + fileName);
				std::stringstream ss;

				ss << "point size: " << pointSize << "\n";
				ss << "iterations: " << iterations << "\n";
				
				if (exportDataPoints)
				{
					int totalDataPoints = (iterations - 1) % n == 0 ? iterations / n : iterations / n + 1;
					ss << "data points: " << totalDataPoints << "\n";
				}

				ss << "\nPoints\n";
				for (int i = 0; i < points.size(); i++)
					ss << "(" << points[i].x << ", " << points[i].y << ", " << (points[i].color & 0xFFFFFF) << ")""\n";

				ss << "\nRotations\n";
				for (int i = 0; i < rotations.size(); i++)
					ss << "<" << rotations[i].p1 << ", " << rotations[i].p2 << ", " << rotations[i].rotation << ">""\n";

				outfile << ss.rdbuf();
				outfile.close();
			}

			static std::tuple<std::vector<Spiroplot2D>, std::vector<string>> Config1(int pointSize, bool exportDataPoints, string dir, int iterations)
			{
				std::vector<TestPoint> points = std::vector<TestPoint>();
				std::vector<TestRotation> rotations = std::vector<TestRotation>();

				points.push_back(TestPoint(200, 500, 0xFF0000));
				points.push_back(TestPoint(500, 500, 0x00FF00));
				points.push_back(TestPoint(500, 200, 0x0000FF));

				rotations.push_back(TestRotation(0, 1, 90));
				rotations.push_back(TestRotation(1, 2, 90));

				return RunConfig(points, rotations, pointSize, exportDataPoints, dir, iterations);
			}

			static std::tuple<std::vector<Spiroplot2D>, std::vector<string>> Config2(int pointSize, bool exportDataPoints, string dir, int iterations)
			{
				std::vector<TestPoint> points = std::vector<TestPoint>();
				std::vector<TestRotation> rotations = std::vector<TestRotation>();

				points.push_back(TestPoint(200, 500, 0xFF0000));
				points.push_back(TestPoint(500, 500, 0x00FF00));
				points.push_back(TestPoint(500, 200, 0x0000FF));

				rotations.push_back(TestRotation(0, 1, 1));
				rotations.push_back(TestRotation(1, 2, 1));

			    return RunConfig(points, rotations, pointSize, exportDataPoints, dir, iterations);
			}

			static std::tuple<std::vector<Spiroplot2D>, std::vector<string>> Config3(int pointSize, bool exportDataPoints, string dir, int iterations)
			{
				std::vector<TestPoint> points = std::vector<TestPoint>();
				std::vector<TestRotation> rotations = std::vector<TestRotation>();

				points.push_back(TestPoint(200, 500, 0xFF0000));
				points.push_back(TestPoint(500, 500, 0x00FF00));
				points.push_back(TestPoint(500, 200, 0x0000FF));

				rotations.push_back(TestRotation(0, 1, 1));
				rotations.push_back(TestRotation(1, 2, 90));
				rotations.push_back(TestRotation(0, 2, 1));

				return RunConfig(points, rotations, pointSize, exportDataPoints, dir, iterations);
			}

			static std::tuple<std::vector<Spiroplot2D>, std::vector<string>> Config4(int pointSize, bool exportDataPoints, string dir, int iterations)
			{
				std::vector<TestPoint> points = std::vector<TestPoint>();
				std::vector<TestRotation> rotations = std::vector<TestRotation>();

				points.push_back(TestPoint(200, 500, 0xFF0000));
				points.push_back(TestPoint(500, 500, 0x00FF00));
				points.push_back(TestPoint(500, 200, 0x0000FF));

				rotations.push_back(TestRotation(2, 1, 1));
				rotations.push_back(TestRotation(1, 0, 1));
				rotations.push_back(TestRotation(2, 1, 1));
				rotations.push_back(TestRotation(2, 0, 1));

				return RunConfig(points, rotations, pointSize, exportDataPoints, dir, iterations);
			}

			static std::tuple<std::vector<Spiroplot2D>, std::vector<string>> Config5(int pointSize, bool exportDataPoints, string dir, int iterations)
			{
				std::vector<TestPoint> points = std::vector<TestPoint>();
				std::vector<TestRotation> rotations = std::vector<TestRotation>();

				points.push_back(TestPoint(200, 500, 0xFF0000));
				points.push_back(TestPoint(500, 500, 0x00FF00));
				points.push_back(TestPoint(500, 200, 0x0000FF));

				rotations.push_back(TestRotation(0, 1, 1));
				rotations.push_back(TestRotation(1, 2, 43));
				rotations.push_back(TestRotation(2, 0, 1));

				return RunConfig(points, rotations, pointSize, exportDataPoints, dir, iterations);
			}

			static std::tuple<std::vector<Spiroplot2D>, std::vector<string>> Config6(int pointSize, bool exportDataPoints, string dir, int iterations)
			{
				std::vector<TestPoint> points = std::vector<TestPoint>();
				std::vector<TestRotation> rotations = std::vector<TestRotation>();

				points.push_back(TestPoint(200, 500, 0xFF0000));
				points.push_back(TestPoint(500, 500, 0x00FF00));
				points.push_back(TestPoint(500, 200, 0x0000FF));

				rotations.push_back(TestRotation(0, 1, 16));
				rotations.push_back(TestRotation(1, 2, 179));

				return RunConfig(points, rotations, pointSize, exportDataPoints, dir, iterations);
			}

			static std::tuple<std::vector<Spiroplot2D>, std::vector<string>> Config7(int pointSize, bool exportDataPoints, string dir, int iterations)
			{
				std::vector<TestPoint> points = std::vector<TestPoint>();
				std::vector<TestRotation> rotations = std::vector<TestRotation>();

				points.push_back(TestPoint(200, 500, 0xFF0000));
				points.push_back(TestPoint(500, 500, 0x00FF00));
				points.push_back(TestPoint(500, 200, 0x0000FF));

				rotations.push_back(TestRotation(0, 1, 172));
				rotations.push_back(TestRotation(1, 2, 98));

				return RunConfig(points, rotations, pointSize, exportDataPoints, dir, iterations);
			}

			// BIG PLOT
			static std::tuple<std::vector<Spiroplot2D>, std::vector<string>> Config8(int pointSize, bool exportDataPoints, string dir, int iterations) 
			{
				std::vector<TestPoint> points = std::vector<TestPoint>();
				std::vector<TestRotation> rotations = std::vector<TestRotation>();

				points.push_back(TestPoint(0, 600, 0xFF0000));
				points.push_back(TestPoint(600, 600, 0x00FF00));
				points.push_back(TestPoint(600, 0, 0x0000FF));

				rotations.push_back(TestRotation(0, 1, 90));
				rotations.push_back(TestRotation(1, 2, 90));

				return RunConfig(points, rotations, pointSize, exportDataPoints, dir, iterations);
			}

			// SMALL PLOT
			static std::tuple<std::vector<Spiroplot2D>, std::vector<string>> Config9(int pointSize, bool exportDataPoints, string dir, int iterations)
			{
				std::vector<TestPoint> points = std::vector<TestPoint>();
				std::vector<TestRotation> rotations = std::vector<TestRotation>();

				points.push_back(TestPoint(300, 450, 0xFF0000));
				points.push_back(TestPoint(450, 450, 0x00FF00));
				points.push_back(TestPoint(450, 300, 0x0000FF));

				rotations.push_back(TestRotation(0, 1, 90));
				rotations.push_back(TestRotation(1, 2, 90));

				return RunConfig(points, rotations, pointSize, exportDataPoints, dir, iterations);
			}


			static void Iterate(int iterations, Spiroplot2D& spiroplot2D)
			{
				for (int i = 0; i < iterations; i++)
				{
					spiroplot2D.Iterate();
				}
			}

			static const uint n = 100;

			static void IterateAndExport(int iterations, Spiroplot2D& spiroplot2D, string dir, PrecisionLevel precision)
			{
				string fileName = GenerateFileName(precision) + ".csv";
				std::ofstream outfile(dir + fileName);
				std::stringstream ss;

				int totalPoints = spiroplot2D.points.size() - 1;

				for (int i = 0; i <= totalPoints; i++)
				{
					if (i != totalPoints)
						ss << "x" << i << ",y" << i << ",";
					else
						ss << "x" << i << ",y" << i << "\n";
				}

				for (int i = 0; i < iterations; i++)
				{
					spiroplot2D.Iterate();

					// Save datapoints at every n rotations
					if (i % n == 0 || i == iterations - 1)
					{
						for (int j = 0; j <= totalPoints; j++)
						{
							if (j != totalPoints)
								ss << Calculator::toString(spiroplot2D.points[j].GetX()) << "," << Calculator::toString(spiroplot2D.points[j].GetY()) << ",";
							else
								ss << Calculator::toString(spiroplot2D.points[j].GetX()) << "," << Calculator::toString(spiroplot2D.points[j].GetY()) << "\n";
						}

						outfile << ss.rdbuf();
						ss.str("");
						ss.clear();

						// Write to the file every ~10000 points.
						if (ss.tellp() >= 10000 * (sizeof(double) * 2)) {
							outfile << ss.rdbuf();
							ss.str("");
							ss.clear();
						}
					}
				}

				// Write any remaining points.
				outfile << ss.rdbuf();
				outfile.close();
			}
		private:
			static string GenerateFileName(PrecisionLevel precision)
			{
				if (precision == PrecisionLevel::FLOAT)
					return "32-bit";
				else if (precision == PrecisionLevel::DOUBLE)
					return "64-bit";
				else if (precision == PrecisionLevel::MPFR_80)
					return "80-bit";
				else if (precision == PrecisionLevel::MPFR_128)
					return "128-bit";
				else if (precision == PrecisionLevel::MPFR_256)
					return "256-bit";
				else
					throw std::invalid_argument("Invalid precision type");
			}
	};
}

