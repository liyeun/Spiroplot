#pragma once
#include "spiroplot.h"
#include "Test.h"
#include "../lib/stb_image_write.h"
#include <filesystem>
/*

#include <mpfr.h>
#include <cmath>
#include <limits>
#include <iomanip>
*/

namespace Tmpl8 {
class Scene
{
public:
    int pointSize = 3;
    Scene()
    {
        /*
        Point2 p1(1234567890, 54321);
        Point2 p2(1111111111, 12345);

        Point2 center((p1.x + p2.x) / 2.0, (p1.y + p2.y) / 2.0);

        long long i = 1;

        for (int i = 0; i < 400000000; i++) {
            p1 = p1.rotate(center, 90);
            p2 = p2.rotate(center, 90);
        }

        int2 res1 = int2((int)std::round(p1.x), std::round(p1.y));
        int2 res2 = int2((int)std::round(p2.x), std::round(p2.y));

        std::cout << "iteraion : " << i << std::endl;
        std::cout << "p1: (" << res1.x << ", " << res1.y << ")\n";
        std::cout << "p2: (" << res2.x << ", " << res2.y << ")\n";
        if (res1.x != 0 || res1.y != 400 || res2.x != 800 || res2.y != 400)
        {
            std::cout << "End : Total rotations = " << 400000000 * i << std::endl;
            //break;
        }

            i++;
        */

        
        Number f, d, mpfr80, mpfr128, mpfr256;

        f = 1.0f;
        d = 1.0;

        mpfr_prec_t mpfr_precision = 80;
        MpfrWrapper mpfr80_value(mpfr_precision);
        mpfr_set_d(mpfr80_value.value, 1.0, MPFR_RNDN);
        mpfr80 = mpfr80_value;

        mpfr_precision = 128;
        MpfrWrapper mpfr128_value(mpfr_precision);
        mpfr_set_d(mpfr128_value.value, 1.0, MPFR_RNDN);
        mpfr128 = mpfr128_value;

        mpfr_precision = 256;
        MpfrWrapper mpfr256_value(mpfr_precision);
        mpfr_set_d(mpfr256_value.value, 1.0, MPFR_RNDN);
        mpfr256 = mpfr256_value;

        f = Calculator::cos(f);
        d = Calculator::cos(d);
        mpfr80 = Calculator::cos(mpfr80);
        mpfr128 = Calculator::cos(mpfr128);
        mpfr256 = Calculator::cos(mpfr256);

        f = Calculator::divideByTwo(f);
        d = Calculator::divideByTwo(d);
        mpfr80 = Calculator::divideByTwo(mpfr80);
        mpfr128 = Calculator::divideByTwo(mpfr128);
        mpfr256 = Calculator::divideByTwo(mpfr256);

        f = Calculator::add(f,f);
        d = Calculator::add(d,d);
        mpfr80 = Calculator::add(mpfr80, mpfr80);
        mpfr128 = Calculator::add(mpfr128, mpfr128);
        mpfr256 = Calculator::add(mpfr256, mpfr256);

        f = Calculator::mul(f, f);
        d = Calculator::mul(d, d);
        mpfr80 = Calculator::mul(mpfr80, mpfr80);
        mpfr128 = Calculator::mul(mpfr128, mpfr128);
        mpfr256 = Calculator::mul(mpfr256, mpfr256);

        f = Calculator::sin(f);
        d = Calculator::sin(d);
        mpfr80 = Calculator::sin(mpfr80);
        mpfr128 = Calculator::sin(mpfr128);
        mpfr256 = Calculator::sin(mpfr256);

        int i = 10;
        while (i < 10)
        {
            f = Calculator::add(f, f);
            d = Calculator::add(d, d);
            mpfr80 = Calculator::add(mpfr80, mpfr80);
            mpfr128 = Calculator::add(mpfr128, mpfr128);
            mpfr256 = Calculator::add(mpfr256, mpfr256);
            i++;
        }

        f = Calculator::sub(Calculator::sub(f, f), f);
        d = Calculator::sub(Calculator::sub(d, d), d);
        mpfr80 = Calculator::sub(Calculator::sub(mpfr80, mpfr80), mpfr80);
        mpfr128 = Calculator::sub(Calculator::sub(mpfr128, mpfr128), mpfr128);
        mpfr256 = Calculator::sub(Calculator::sub(mpfr256, mpfr256), mpfr256);

        /*
        mpfr_prec_t new_mpfr_precision = 80;
        Calculator::changeToMpfrPrecision(f, new_mpfr_precision);
        Calculator::changeToMpfrPrecision(d, new_mpfr_precision);
        Calculator::changeToMpfrPrecision(mpfr80, new_mpfr_precision);
        Calculator::changeToMpfrPrecision(mpfr128, new_mpfr_precision);
        Calculator::changeToMpfrPrecision(mpfr256, new_mpfr_precision);
        */

        /*
        Calculator::changeToDouble(f);
        Calculator::changeToDouble(d);
        Calculator::changeToDouble(mpfr80);
        Calculator::changeToDouble(mpfr128);
        Calculator::changeToDouble(mpfr256);
        */
        Calculator::print(f);
        Calculator::print(d);
        Calculator::print(mpfr80);
        Calculator::print(mpfr128);
        Calculator::print(mpfr256);

        std::cout << Calculator::toString(f) << std::endl;
        std::cout << Calculator::toString(d) << std::endl;
        std::cout << Calculator::toString(mpfr80) << std::endl;
        std::cout << Calculator::toString(mpfr128) << std::endl;
        std::cout << Calculator::toString(mpfr256) << std::endl;

        PrecisionLevel defPrecision = PrecisionLevel::MPFR_256;
        PlotState plotState = LIFO;

        //spiroplot2D = Spiroplot2D(2000, 2000, pointSize, plotState);
        spiroplot2D = Spiroplot2D(800, 800, pointSize, plotState);
        /*
        spiroplot2D.AddPoint(200, 200, 0xFF0000);
        spiroplot2D.AddPoint(600, 600, 0x00FF00);

        spiroplot2D.AddRotation(&spiroplot2D.points[0], &spiroplot2D.points[1], 1);
        */
        

        
        spiroplot2D.AddPoint(200, 500, 0xFF0000, defPrecision);
        spiroplot2D.AddPoint(500, 500, 0x00FF00, defPrecision);
        spiroplot2D.AddPoint(500, 200, 0x0000FF, defPrecision);

        spiroplot2D.AddRotation(&spiroplot2D.points[0], &spiroplot2D.points[1], defPrecision, 90);
        spiroplot2D.AddRotation(&spiroplot2D.points[1], &spiroplot2D.points[2], defPrecision, 90);
        
        
        
        /*
        spiroplot2D.AddPoint(250, 550, 0xFF0000, defPrecision);
        spiroplot2D.AddPoint(550, 550, 0x00FF00, defPrecision);
        spiroplot2D.AddPoint(550, 250, 0x0000FF, defPrecision);
        spiroplot2D.AddPoint(250, 250, 0xFFFF00, defPrecision);

        spiroplot2D.AddRotation(&spiroplot2D.points[0], &spiroplot2D.points[1], defPrecision, 45);
        spiroplot2D.AddRotation(&spiroplot2D.points[1], &spiroplot2D.points[2], defPrecision, 45);
        spiroplot2D.AddRotation(&spiroplot2D.points[2], &spiroplot2D.points[3], defPrecision, 45);
        spiroplot2D.AddRotation(&spiroplot2D.points[3], &spiroplot2D.points[0], defPrecision, 45);
        */

        /*
        int scale = 1;
        spiroplot2D.AddPoint(250 * scale, 550 * scale, 0x00FFFF, defPrecision);
        spiroplot2D.AddPoint(550 * scale, 550 * scale, 0xFF0000, defPrecision);
        spiroplot2D.AddPoint(550 * scale, 250 * scale, 0x00FF00, defPrecision);
        spiroplot2D.AddPoint(250 * scale, 250 * scale, 0xFFFF00, defPrecision);

        spiroplot2D.AddRotation(&spiroplot2D.points[0], &spiroplot2D.points[1], defPrecision, -35);
        spiroplot2D.AddRotation(&spiroplot2D.points[2], &spiroplot2D.points[3], defPrecision, 88);
        spiroplot2D.AddRotation(&spiroplot2D.points[3], &spiroplot2D.points[0], defPrecision, 92);
        spiroplot2D.AddRotation(&spiroplot2D.points[1], &spiroplot2D.points[2], defPrecision, 35);
        */

        /*
        spiroplot2D.AddPoint(100, 500, 0xFF0000, defPrecision);
        spiroplot2D.AddPoint(400, 500, 0x00FF00, defPrecision);
        spiroplot2D.AddPoint(400, 200, 0x0000FF, defPrecision);

        spiroplot2D.AddPoint(700, 500, 0xFFFF00, defPrecision);

        spiroplot2D.AddRotation(&spiroplot2D.points[0], &spiroplot2D.points[1], defPrecision, 45);
        spiroplot2D.AddRotation(&spiroplot2D.points[1], &spiroplot2D.points[2], defPrecision, 90);
        spiroplot2D.AddRotation(&spiroplot2D.points[2], &spiroplot2D.points[0], defPrecision, -45);

        spiroplot2D.AddRotation(&spiroplot2D.points[1], &spiroplot2D.points[3], defPrecision, 45);
        spiroplot2D.AddRotation(&spiroplot2D.points[2], &spiroplot2D.points[3], defPrecision, -45);
        */
        spiroplot2D.CenterAndScalePoints(defPrecision);
        


        /*
        std::list<Point> testPoints;
        testPoints.push_back(Point(200, 200, 0xFF0000));
        testPoints.push_back(Point(400, 200, 0x00FF00));
        testPoints.push_back(Point(400, 400, 0x0000FF));

        std::list<Rotation> testRotations;
        testRotations.push_back(Rotation(0, 1, 90));
        testRotations.push_back(Rotation(1, 2, 90));

        spiroplot2D.Import(testPoints, testRotations);
        */


        /*
        std::ofstream outfile("output.txt");
        std::stringstream ss;

        for (const auto& point : spiroplot2D.points) {
            ss << Calculator::returnFloat(point.GetX()) << " " << Calculator::returnFloat(point.GetY()) << "\n";

            // Write to the file every 10000 points.
            //}if (ss.tellp() >= 10000 * (sizeof(double) * 2 + 3)) { // 2 doubles and a space, newline, and potential minus sign
            outfile << ss.rdbuf();
            ss.str("");
            ss.clear();
            //}
        }


        ss << Calculator::toString(f) << std::endl;
        ss << Calculator::toString(d) << std::endl;
        ss << Calculator::toString(mpfr80) << std::endl;
        ss << Calculator::toString(mpfr128) << std::endl;
        ss << Calculator::toString(mpfr256) << std::endl;

        // Write any remaining points.        outfile << ss.rdbuf();
        */

        std::string exportDir = std::string("Config10");
        bool exportDataPoints = true;
        //std::tuple<std::vector<Spiroplot2D>, std::vector<string>> Config = Test::Config9(0, exportDataPoints, "plots/" + exportDir + "/", 10000000);
        //ExportConfigTest(Config, exportDir);

        spiroplot3D = Spiroplot3D(800, 800, 800, 0, plotState);

        isInitialized = true;
    }

    void UpdatePointSize()
    {
        spiroplot2D.ChangePointSize(pointSize);
    }


    void UpdatePrecision(PrecisionLevel precisionLevel)
    {
        spiroplot2D.UpdatePrecision(precisionLevel);
    }

    void UpdatePlotState(PlotState plotState)
    {
        spiroplot2D.UpdatePlotState(plotState);
    }

    void Export(string filename)
    {
        Export(filename, spiroplot2D);
    }

    void Export3D(string filename)
    {
        spiroplot3D.Export(filename);
    }

    void Export(string filename, Spiroplot2D spiroplot, string dir = "plots/")
    {
        int width = spiroplot.gridWidth, height = spiroplot.gridHeight;

        // allocate memory for the image buffer
        uchar* image_buffer = new uchar[width * height * 4];
        memset(image_buffer, backgroundColor, width * height * 4 * sizeof(uchar));
        if (spiroplot.plotState == PlotState::ORDER) {
            for (int i = spiroplot.pointsOrder.size() - 1; i >= 0; i--)
            {
                int currentOrderID = spiroplot.pointsOrder[i];

                Point* currentPoint = nullptr;

                for (int j = 0; j < spiroplot.points.size(); j++)
                {
                    if (spiroplot.points[j].GetID() == currentOrderID) {
                        currentPoint = &spiroplot.points[j];
                    }
                }
                if (currentPoint == nullptr) {
                    std::cout << "Export failed" << std::endl;
                    return;
                }

                if (!spiroplot.hidePoint[currentPoint->GetID()])
                {
                    for (int j = 0; j < width * height; j++) {
                        if (currentPoint->grid[j] == backgroundColor)
                            continue;

                        int index = j * 4;
                        uchar r = (uchar)((currentPoint->grid[j] & 0x00FF0000) >> 16);
                        uchar g = (uchar)((currentPoint->grid[j] & 0x0000FF00) >> 8);
                        uchar b = (uchar)(currentPoint->grid[j] & 0x000000FF);

                        image_buffer[index + 0] = (uchar)r;
                        image_buffer[index + 1] = (uchar)g;
                        image_buffer[index + 2] = (uchar)b;
                        image_buffer[index + 3] = 255;
                    }
                }
                
            }
        }
        else {

            // copy the pixel data into the image buffer
            for (int i = 0; i < width * height; i++) {

                int index = i * 4;
                if (spiroplot2D.HiddenPoint(spiroplot.grid[i] >> 24))
                {
                    uchar r = (uchar)((backgroundColor & 0x00FF0000) >> 16);
                    uchar g = (uchar)((backgroundColor & 0x0000FF00) >> 8);
                    uchar b = (uchar)(backgroundColor & 0x000000FF);

                    image_buffer[index + 0] = (uchar)r;
                    image_buffer[index + 1] = (uchar)g;
                    image_buffer[index + 2] = (uchar)b;
                    image_buffer[index + 3] = 255;
                }
                else {

                    uchar r = (uchar)((spiroplot.grid[i] & 0x00FF0000) >> 16);
                    uchar g = (uchar)((spiroplot.grid[i] & 0x0000FF00) >> 8);
                    uchar b = (uchar)(spiroplot.grid[i] & 0x000000FF);

                    image_buffer[index + 0] = (uchar)r;
                    image_buffer[index + 1] = (uchar)g;
                    image_buffer[index + 2] = (uchar)b;
                    image_buffer[index + 3] = 255;
                }
            }
        }

        if (dir != "plots/")
            dir = "plots/" + dir + "/";

        if (!std::filesystem::exists(dir))
            std::filesystem::create_directory(dir);

        // write the image buffer to a PNG file
        stbi_write_png((dir + filename + string(".png")).c_str(), width, height, 4, image_buffer, width * 4);

        // free the memory allocated for the image buffer
        delete[] image_buffer;
    }


    void ExportConfigTest(std::tuple<std::vector<Spiroplot2D>, std::vector<string>> config, string dir)
    {
        for (int i = 0; i < std::get<0>(config).size(); ++i) {

            Export(std::get<1>(config)[i], std::get<0>(config)[i], dir);
        }
    }

    void Change2DSpiroplotResolution(int res, PlotState plotState)
    {
        spiroplot2D = Spiroplot2D(res, res, pointSize, plotState);
    }

    void Load3DSpiroplotTemplate(int temp, bool useGPU)
    {
        spiroplot3D.LoadTemplate(temp, useGPU);
    }

    void Clear()
    {
        spiroplot2D.Clear();
    }

    void Clear(bool useGPU)
    {
        spiroplot3D.Clear(useGPU);
    }

    void Reset()
    {
        spiroplot2D.Reset();
    }

    void Reset(bool useGPU)
    {
        spiroplot3D.Reset(useGPU);
    }

    Spiroplot2D spiroplot2D;
    Spiroplot3D spiroplot3D;

    bool isInitialized = false;
};

}
