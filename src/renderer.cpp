﻿#define STB_IMAGE_WRITE_IMPLEMENTATION

#include "precomp.h"
// -----------------------------------------------------------
// Initialize the renderer
// -----------------------------------------------------------
void Renderer::Init()
{
	
	/*
	// HIDE CONSOLE WINDOW
	// Find the console window by its class name
	HWND hwnd = FindWindowA("ConsoleWindowClass", NULL);
	if (hwnd != NULL)
		ShowWindow(hwnd, 0);
	*/

	camera2D = new Camera2D(scene.spiroplot2D.gridWidth, scene.spiroplot2D.gridHeight);

	camera3D = new Camera3D(scene.spiroplot2D.gridWidth, scene.spiroplot2D.gridHeight);

	mousePos.x -= SCRWIDTHOFFSET;
	mouseGridPos = ToGridPos(mousePos + int2(SCRWIDTHOFFSET, 0));
	
#ifdef GPU
	// Create Kernels
	generatePrimaryRaysKernel = new Kernel("Kernels/generatePrimaryRays.cl", "GeneratePrimaryRays");
	initialExtendKernel = new Kernel("Kernels/extend.cl", "InitialExtend");
	extendKernel = new Kernel("Kernels/extend.cl", "Extend");
	finalizeKernel = new Kernel("Kernels/finalize.cl", "Finalize");

	initialExtendKernelFast = new Kernel("Kernels/extend.cl", "InitialExtendFast");
	extendKernelFast = new Kernel("Kernels/extend.cl", "ExtendFast");

	updateLIFOKernel = new Kernel("Kernels/update.cl", "FillWithCircleLIFO");
	updateFIFOKernel = new Kernel("Kernels/update.cl", "FillWithCircleFIFO");

	// Create Buffers
	backgroundColorBuffer = new Buffer(sizeof(uint), &backgroundColor, 0);
	deviceBuffer = new Buffer(SCRWIDTH * SCRHEIGHT * sizeof(uint), screen->pixels, 0);
	pixelColorBuffer = new Buffer((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT * sizeof(uint));
	cameraPropBuffer = new Buffer(4 * sizeof(float4), camera3D->gpuCamProp, 0);

	// Primary Ray Buffers
	pixelIdxBuffer = new Buffer((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT * sizeof(int));
	originBuffer = new Buffer((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT * sizeof(float4));
	directionBuffer = new Buffer((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT * sizeof(float4));

	// Set Kernel Arguments

	generatePrimaryRaysKernel->SetArguments(pixelIdxBuffer, originBuffer, directionBuffer,  // Primary Rays
		cameraPropBuffer, pixelColorBuffer,
		SCRWIDTH, SCRWIDTHOFFSET, SCRHEIGHT, backgroundColorBuffer);

	cameraPropBuffer->CopyToDevice(false);
	backgroundColorBuffer->CopyToDevice(false);

	finalizeKernel->SetArguments(deviceBuffer, pixelColorBuffer,
		SCRWIDTH, SCRWIDTHOFFSET);

	deviceBuffer->CopyToDevice(true);

#endif




	/*
	// create fp32 rgb pixel buffer to render to
	accumulator = (float4*)MALLOC64( SCRWIDTH * SCRHEIGHT * 16 );
	memset( accumulator, 0, SCRWIDTH * SCRHEIGHT * 16 );

	grid = (float4*)MALLOC64(GRIDWIDTH * GRIDHEIGHT * 16);
	memset(grid, 0, GRIDWIDTH * GRIDHEIGHT * 16);
	*/
}

void Renderer::KeyDown(int key) {
	switch (key) {
		case GLFW_KEY_SPACE:
				ChangeGameState();
			break;
		case GLFW_KEY_PERIOD:
				ChangeAnimationState();
			break;
		case GLFW_KEY_C:
				CenterPoints();
			break;
		case GLFW_KEY_BACKSPACE:
				Clear();
			break;
		case GLFW_KEY_DELETE:
				Reset();
			break;
		//case GLFW_KEY_S:
				//Export(filename);
			break;
		case GLFW_KEY_H:
				HideInput();
			break;
		case GLFW_KEY_EQUAL:
			if (is3D)
				camera3D->Zoom(Camera3D::Direction::In, false, delta_prev_time);
			else
				Zoom(zoomSpeed);
			break;
		case GLFW_KEY_MINUS:
			if (is3D)
				camera3D->Zoom(Camera3D::Direction::Out, false, delta_prev_time);
			else
				Zoom(-zoomSpeed);
			break;
		case GLFW_KEY_W:
			if (is3D)
				camera3D->Move(Camera3D::Direction::Up, false, delta_prev_time);
			else
				Move(-moveSpeed, true);
			break;
		case GLFW_KEY_S:
			if (is3D)
				camera3D->Move(Camera3D::Direction::Down, false, delta_prev_time);
			else
				Move(moveSpeed, true);
			break;
		case GLFW_KEY_A:
			if (is3D)
				camera3D->Move(Camera3D::Direction::Left, false, delta_prev_time);
			else
				Move(-moveSpeed, false);
			break;
		case GLFW_KEY_D:
			if (is3D)
				camera3D->Move(Camera3D::Direction::Right, false, delta_prev_time);
			else
				Move(moveSpeed, false);
			break;
		case GLFW_KEY_UP:
			if (is3D)
				camera3D->Rotate(Camera3D::Direction::Up, false, delta_prev_time);
			break;
		case GLFW_KEY_DOWN:
			if (is3D)
				camera3D->Rotate(Camera3D::Direction::Down, false, delta_prev_time);
			break;
		case GLFW_KEY_LEFT:
			if (is3D)
			{
				if (!UI_item_selected)
					camera3D->Rotate(Camera3D::Direction::Left, false, delta_prev_time);
			}
			break;
		case GLFW_KEY_RIGHT:
			if (is3D) {
				if (!UI_item_selected)
					camera3D->Rotate(Camera3D::Direction::Right, false, delta_prev_time);
			}
			break;
		case GLFW_KEY_G:
				ChangeDevice();
			break;
		case GLFW_KEY_F:
				ChangeRenderMethod();
			break;
		case GLFW_KEY_R:
			if (is3D) {
				camera3D->ResetToInitial();
			}
			break;
		default:
			break;
	}
}

void Renderer::KeyUp(int key) {
	switch (key) {
		case GLFW_KEY_EQUAL:
			if (is3D)
				camera3D->Zoom(Camera3D::Direction::In, true, delta_prev_time);
			break;
		case GLFW_KEY_MINUS:
			if (is3D)
				camera3D->Zoom(Camera3D::Direction::Out, true, delta_prev_time);
			break;
		case GLFW_KEY_W:
			if (is3D)
				camera3D->Move(Camera3D::Direction::Up, true, delta_prev_time);
			break;
		case GLFW_KEY_S:
			if (is3D)
				camera3D->Move(Camera3D::Direction::Down, true, delta_prev_time);
			break;
		case GLFW_KEY_A:
			if (is3D)
				camera3D->Move(Camera3D::Direction::Left, true, delta_prev_time);
			break;
		case GLFW_KEY_D:
			if (is3D)
				camera3D->Move(Camera3D::Direction::Right, true, delta_prev_time);
			break;
		case GLFW_KEY_UP:
			if (is3D)
				camera3D->Rotate(Camera3D::Direction::Up, true, delta_prev_time);
			break;
		case GLFW_KEY_DOWN:
			if (is3D)
				camera3D->Rotate(Camera3D::Direction::Down, true, delta_prev_time);
			break;
		case GLFW_KEY_LEFT:
			if (is3D)
				camera3D->Rotate(Camera3D::Direction::Left, true, delta_prev_time);
			break;
		case GLFW_KEY_RIGHT:
			if (is3D)
				camera3D->Rotate(Camera3D::Direction::Right, true, delta_prev_time);
			break;
		default:
			break;
	}
}


void Renderer::MouseDown(int key) {

	// TODO CHANGE LATER
	if (is3D)
	{
		if (currentGameState == Edit)
		{
			// ADD or SELECT a point
			if (key == GLFW_MOUSE_BUTTON_1 || key == GLFW_MOUSE_BUTTON_3)
			{
				if (selectedPoint)
				{
					if (selectedPoint->state == Point::PointState::MOVE)
					{
						if (mousePos.x > SCRWIDTHOFFSET) // TODO TODO MAYBE MORE CHECKS
						{
							selectedPoint->state = Point::PointState::HOVERED;
							selectedPoint = 0;
						}
						return;
					}

					if (key == GLFW_MOUSE_BUTTON_3 || selectedPoint->state == Point::PointState::SELECTED)
					{
						return;
					}
				}


				Point* minPoint = 0;
				float minDist = std::numeric_limits<float>::max();
				for (int i = 0; i < scene.spiroplot3D.totalPoints; i++)
				{
					Point* point = &scene.spiroplot3D.points[i];
					float3 point_screen_pos = ToScreenPos(point->Position3D());
					float dist = magnitude(mousePos - float2(point_screen_pos.x, point_screen_pos.y));

					if (dist <= pointSize * point_screen_pos.z && dist < minDist)
					{
						minPoint = point;
						minDist = dist;
					}
				}

				Rotation* minRotation = 0;
				if (minPoint == 0)
				{
					for (int i = 0; i < scene.spiroplot3D.totalRotations; i++)
					{
						Rotation* rotation = &scene.spiroplot3D.rotations[i];
						float3 p1_screen_pos = ToScreenPos(rotation->p1->Position3D()), p2_screen_pos = ToScreenPos(rotation->p2->Position3D());
						float2 p1 = float2(p1_screen_pos.x, p1_screen_pos.y), p2 = float2(p2_screen_pos.x, p2_screen_pos.y);

						float2 dir = p2 - p1;
						float segmentLength = magnitude(dir);
						float2 normDir = dir / segmentLength;
						float2 toPoint = mousePos - p1;
						float projectionLength = dot(toPoint, normDir);
						float2 closestPoint;
						if (projectionLength < 0) {
							closestPoint = p1;
						}
						else if (projectionLength > segmentLength) {
							closestPoint = p2;
						}
						else {
							closestPoint = p1 + projectionLength * normDir;
						}

						float dist = magnitude(closestPoint - mousePos);

						if (dist <= lineSize && dist < minDist)
						{
							if (minPoint)
								minPoint = 0;

							minRotation = rotation;
							minDist = dist;
						}
					}
				}

				if (minPoint)
				{

					if (key == GLFW_MOUSE_BUTTON_1)
					{
						minPoint->ChangeState(Point::PointState::SELECTED);

						chrono::time_point currentTime = chrono::system_clock::now();
						std::chrono::milliseconds::rep elapsedTime = chrono::duration_cast<std::chrono::milliseconds>(currentTime - lastLeftClickTime).count();

						if (elapsedTime <= doubleClickThreshold)
							minPoint->ChangeMenuState(Point::MenuState::OPEN);

						lastLeftClickTime = currentTime;
					}
					else if (key == GLFW_MOUSE_BUTTON_3)
					{
						minPoint->ChangeState(Point::PointState::MOVE);
					}

					if (activePoint != minPoint)
					{
						if (activePoint)
						{
							activePoint->ChangeState(Point::PointState::INACTIVE);
						}
						activePoint = minPoint;
					}

					selectedPoint = minPoint;

					if (selectedRotation)
					{
						selectedRotation->ChangeState(Rotation::RotationState::INACTIVE);
						selectedRotation = 0;
					}
					if (activeRotation)
					{
						activeRotation->ChangeState(Rotation::RotationState::INACTIVE);
						activeRotation = 0;
					}
				}
				else if (minRotation)
				{
					minRotation->ChangeState(Rotation::RotationState::SELECTED);

					if (activeRotation != minRotation)
					{
						if (activeRotation)
						{
							activeRotation->ChangeState(Rotation::RotationState::INACTIVE);
						}
						activeRotation = minRotation;
					}

					// TODO MAYBE CHANGE
					if (selectedRotation != activeRotation)
						if (selectedRotation)
							selectedRotation->ChangeState(Rotation::RotationState::INACTIVE);

					selectedRotation = minRotation;

					if (selectedPoint)
					{
						selectedPoint->ChangeState(Point::PointState::INACTIVE);
						selectedPoint = 0;
					}
					if (activePoint)
					{
						activePoint->ChangeState(Point::PointState::INACTIVE);
						activePoint = 0;
					}
				}
				else if (mousePos.x > SCRWIDTHOFFSET && key == GLFW_MOUSE_BUTTON_1) // ADDPOINT 
				{
					// TODO TODO
					int3 gridPos = scene.spiroplot3D.GetPixelPos(camera3D->Get3DRay(mousePos.x - SCRWIDTHOFFSET, mousePos.y));
					if (gridPos.x != -1)
					{
						scene.spiroplot3D.AddPoint(gridPos.x, gridPos.y, gridPos.z, (uint)Rand(16777215), precisionLevel, true);
						activePoint = &scene.spiroplot3D.points[scene.spiroplot3D.totalPoints - 1]; // TODO check if this is a rigerous method
						activePoint->ChangeState(Point::PointState::HOVERED);
					}
				}
			}
			else if (key == GLFW_MOUSE_BUTTON_2)
			{
				if (selectedPoint)
				{
					if (selectedPoint->state == Point::PointState::MOVE)
						return;
				}

				if (activePoint)
				{

					Point* delPoint = &(*activePoint);

					activePoint->ChangeState(Point::PointState::INACTIVE);
					activePoint = 0;

					if (selectedPoint)
					{
						selectedPoint->ChangeState(Point::PointState::INACTIVE);
						selectedPoint = 0;
					}
					else if (selectedRotation)
					{
						selectedRotation->ChangeState(Rotation::RotationState::INACTIVE);
						selectedRotation = 0;
					}

					scene.spiroplot3D.DeletePoint(delPoint);

				}

				if (activeRotation)
				{

					Rotation* delRotation = &(*activeRotation);

					activeRotation->ChangeState(Rotation::RotationState::INACTIVE);
					activeRotation = 0;

					if (selectedPoint)
					{
						selectedPoint->ChangeState(Point::PointState::INACTIVE);
						selectedPoint = 0;
					}
					else if (selectedRotation)
					{
						selectedRotation->ChangeState(Rotation::RotationState::INACTIVE);
						selectedRotation = 0;
					}

					scene.spiroplot3D.DeleteRotation(delRotation);
				}
			}
		}
	}
	else {
		if (currentGameState == Edit)
		{
			// ADD or SELECT a point
			if (key == GLFW_MOUSE_BUTTON_1 || key == GLFW_MOUSE_BUTTON_3)
			{
				if (selectedPoint)
				{
					if (selectedPoint->state == Point::PointState::MOVE)
					{
						if (mouseGridPos.x >= 0 && mouseGridPos.x < scene.spiroplot2D.gridWidth && mouseGridPos.y >= 0 && mouseGridPos.y < scene.spiroplot2D.gridHeight)
						{
							selectedPoint->state = Point::PointState::HOVERED;
							selectedPoint = 0;
						}
						return;
					}

					if (key == GLFW_MOUSE_BUTTON_3 || selectedPoint->state == Point::PointState::SELECTED)
					{
						return;
					}
				}

				Point* minPoint = 0;
				float minDist = std::numeric_limits<float>::max();
				for (int i = 0; i < scene.spiroplot2D.totalPoints; i++)
				{
					Point* point = &scene.spiroplot2D.points[i];
					float dist = magnitude(mouseGridPos - point->Position());

					if (dist <= pointSize * (scene.spiroplot2D.gridWidth / GRIDWIDTH) && dist < minDist)
					{
						minPoint = point;
						minDist = dist;
					}
				}

				Rotation* minRotation = 0;
				if (minPoint == 0)
				{
					for (int i = 0; i < scene.spiroplot2D.totalRotations; i++)
					{
						Rotation* rotation = &scene.spiroplot2D.rotations[i];
						float2 p1 = rotation->p1->Position(), p2 = rotation->p2->Position();

						float2 dir = p2 - p1;
						float segmentLength = magnitude(dir);
						float2 normDir = dir / segmentLength;
						float2 toPoint = mouseGridPos - p1;
						float projectionLength = dot(toPoint, normDir);
						float2 closestPoint;
						if (projectionLength < 0) {
							closestPoint = p1;
						}
						else if (projectionLength > segmentLength) {
							closestPoint = p2;
						}
						else {
							closestPoint = p1 + projectionLength * normDir;
						}

						float dist = magnitude(closestPoint - mouseGridPos);

						if (dist <= lineSize * (scene.spiroplot2D.gridWidth / GRIDWIDTH) && dist < minDist)
						{
							if (minPoint)
								minPoint = 0;

							minRotation = rotation;
							minDist = dist;
						}
					}
				}

				if (minPoint)
				{

					if (key == GLFW_MOUSE_BUTTON_1)
					{
						minPoint->ChangeState(Point::PointState::SELECTED);

						chrono::time_point currentTime = chrono::system_clock::now();
						std::chrono::milliseconds::rep elapsedTime = chrono::duration_cast<std::chrono::milliseconds>(currentTime - lastLeftClickTime).count();

						if (elapsedTime <= doubleClickThreshold)
							minPoint->ChangeMenuState(Point::MenuState::OPEN);

						lastLeftClickTime = currentTime;
					}
					else if (key == GLFW_MOUSE_BUTTON_3)
						minPoint->ChangeState(Point::PointState::MOVE);

					if (activePoint != minPoint)
					{
						if (activePoint)
						{
							activePoint->ChangeState(Point::PointState::INACTIVE);
						}
						activePoint = minPoint;
					}

					selectedPoint = minPoint;

					if (selectedRotation)
					{
						selectedRotation->ChangeState(Rotation::RotationState::INACTIVE);
						selectedRotation = 0;
					}
					if (activeRotation)
					{
						activeRotation->ChangeState(Rotation::RotationState::INACTIVE);
						activeRotation = 0;
					}
				}
				else if (minRotation)
				{
					minRotation->ChangeState(Rotation::RotationState::SELECTED);

					if (activeRotation != minRotation)
					{
						if (activeRotation)
						{
							activeRotation->ChangeState(Rotation::RotationState::INACTIVE);
						}
						activeRotation = minRotation;
					}

					// TODO MAYBE CHANGE
					if (selectedRotation != activeRotation)
						if (selectedRotation)
							selectedRotation->ChangeState(Rotation::RotationState::INACTIVE);

					selectedRotation = minRotation;

					if (selectedPoint)
					{
						selectedPoint->ChangeState(Point::PointState::INACTIVE);
						selectedPoint = 0;
					}
					if (activePoint)
					{
						activePoint->ChangeState(Point::PointState::INACTIVE);
						activePoint = 0;
					}
				}
				else if (mousePos.x >= 0 && key == GLFW_MOUSE_BUTTON_1) // ADDPOINT   // Add x-screen offset (to not overlap with IMGUI)
				{
					if (mouseGridPos.x >= 0 && mouseGridPos.x < scene.spiroplot2D.gridWidth && mouseGridPos.y >= 0 && mouseGridPos.y < scene.spiroplot2D.gridHeight)
					{
						scene.spiroplot2D.AddPoint(mouseGridPos.x , mouseGridPos.y, (uint)Rand(16777215), precisionLevel, true);
						activePoint = &scene.spiroplot2D.points[scene.spiroplot2D.totalPoints - 1]; // TODO check if this is a rigerous method
						activePoint->ChangeState(Point::PointState::HOVERED);
					}
				}
			}
			else if (key == GLFW_MOUSE_BUTTON_2)
			{
				if (selectedPoint)
				{
					if (selectedPoint->state == Point::PointState::MOVE)
						return;
				}

				if (activePoint)
				{
					/*
					scene.spiroplot2D.DeletePoint(activePoint);
					if (activePoint == selectedPoint)
						selectedPoint = 0;

					activePoint = 0;
					*/

					Point* delPoint = &(*activePoint);
					// TODO MAYBE NOT DEFERENCE EVERYTHING WHEN DELETING
					// BUG: WHEN DELETING CERTEIN POINT/ROTATIONS, THE ROTATIONS/POINTS DONT HAVE THE SAME REFERENCES ANYMORE

					activePoint->ChangeState(Point::PointState::INACTIVE);
					activePoint = 0;

					if (selectedPoint)
					{
						selectedPoint->ChangeState(Point::PointState::INACTIVE);
						selectedPoint = 0;
					}
					else if (selectedRotation)
					{
						selectedRotation->ChangeState(Rotation::RotationState::INACTIVE);
						selectedRotation = 0;
					}

					scene.spiroplot2D.DeletePoint(delPoint);

				}

				if (activeRotation)
				{
					/*
					scene.spiroplot2D.DeleteRotation(activeRotation);

					if (activeRotation == selectedRotation)
						selectedRotation = 0;

					activeRotation = 0;
					*/

					Rotation* delRotation = &(*activeRotation);

					// TODO MAYBE NOT DEFERENCE EVERYTHING WHEN DELETING
					// BUG: WHEN DELETING CERTEIN POINT/ROTATIONS, THE ROTATIONS/POINTS DONT HAVE THE SAME REFERENCES ANYMORE

					activeRotation->ChangeState(Rotation::RotationState::INACTIVE);
					activeRotation = 0;

					if (selectedPoint)
					{
						selectedPoint->ChangeState(Point::PointState::INACTIVE);
						selectedPoint = 0;
					}
					else if (selectedRotation)
					{
						selectedRotation->ChangeState(Rotation::RotationState::INACTIVE);
						selectedRotation = 0;
					}

					scene.spiroplot2D.DeleteRotation(delRotation);
				}
			}
		}
	}
}


void Renderer::MouseUp(int key) {

	if (is3D)
	{
		if (currentGameState == Edit)
		{
			if (key == GLFW_MOUSE_BUTTON_1)
			{

				if (selectedPoint)
				{
					if (activePoint)
					{
						if (activePoint != selectedPoint)
						{
							scene.spiroplot3D.AddRotation(selectedPoint, activePoint, precisionLevel);
							selectedPoint->ChangeState(Point::PointState::INACTIVE);
						}
						else
						{
							selectedPoint->ChangeState(Point::PointState::HOVERED);
						}
					}
					else
					{
						selectedPoint->ChangeState(Point::PointState::INACTIVE);
					}

					selectedPoint = 0;
				}
			}
		}
	}
	else
	{
		if (currentGameState == Edit)
		{
			if (key == GLFW_MOUSE_BUTTON_1)
			{
				if (mouseGridPos.x < 0 || mouseGridPos.x >= scene.spiroplot2D.gridWidth || mouseGridPos.y < 0 || mouseGridPos.y >= scene.spiroplot2D.gridHeight)
					return;

				if (selectedPoint)
				{
					if (activePoint)
					{
						if (activePoint != selectedPoint)
						{
							scene.spiroplot2D.AddRotation(selectedPoint, activePoint, precisionLevel);
							selectedPoint->ChangeState(Point::PointState::INACTIVE);
						}
						else
						{
							selectedPoint->ChangeState(Point::PointState::HOVERED);
						}
					}
					else
					{
						selectedPoint->ChangeState(Point::PointState::INACTIVE);
					}

					selectedPoint = 0;
				}
			}
		}
	}
}


void Renderer::MouseMove(int x, int y) {

	// TODO CHANGE LATER
	if (is3D)
	{
		mousePos.y = y;
		mousePos.x = x;
		if (currentGameState == Edit)
		{


			// Move point
			if (selectedPoint)
			{
				if (selectedPoint->state == Point::PointState::MOVE)
				{
					if (mousePos.x < SCRWIDTHOFFSET)
						return;

					int3 gridPos = scene.spiroplot3D.GetPixelPos(camera3D->Get3DRay(mousePos.x - SCRWIDTHOFFSET, mousePos.y));
					if (gridPos.x != -1)
					{
						selectedPoint->Position(gridPos.x, gridPos.y, gridPos.z, precisionLevel);
					}

					return;
					/*
					if (mousePos.x > SCRWIDTHOFFSET)
						selectedPoint->Position(mouseGridPos.x, mouseGridPos.y, precisionLevel);
					else
						selectedPoint->Position(0, mouseGridPos.y, precisionLevel);
					*/
					return;
				}
			}

			Point* minPoint = 0;
			float minDist = std::numeric_limits<float>::max();
			for (int i = 0; i < scene.spiroplot3D.totalPoints; i++)
			{
				Point* point = &scene.spiroplot3D.points[i];
				float3 point_screen_pos = ToScreenPos(point->Position3D());
				float dist = magnitude(mousePos - float2(point_screen_pos.x, point_screen_pos.y));

				if (dist <= pointSize * point_screen_pos.z && dist < minDist)
				{
					minPoint = point;
					minDist = dist;
				}
			}


			Rotation* minRotation = 0;
			if (minPoint == 0)
			{
				for (int i = 0; i < scene.spiroplot3D.totalRotations; i++)
				{
					Rotation* rotation = &scene.spiroplot3D.rotations[i];
					float3 p1_screen_pos = ToScreenPos(rotation->p1->Position3D()), p2_screen_pos = ToScreenPos(rotation->p2->Position3D());
					float2 p1 = float2(p1_screen_pos.x, p1_screen_pos.y), p2 = float2(p2_screen_pos.x, p2_screen_pos.y);

					float2 dir = p2 - p1;
					float segmentLength = magnitude(dir);
					float2 normDir = dir / segmentLength;
					float2 toPoint = mousePos - p1;
					float projectionLength = dot(toPoint, normDir);
					float2 closestPoint;
					if (projectionLength < 0) {
						closestPoint = p1;
					}
					else if (projectionLength > segmentLength) {
						closestPoint = p2;
					}
					else {
						closestPoint = p1 + projectionLength * normDir;
					}

					float dist = magnitude(closestPoint - mousePos);

					if (dist <= lineSize && dist < minDist)
					{
						if (minPoint)
							minPoint = 0;

						minRotation = rotation;
						minDist = dist;
					}
				}
			}

			if (minPoint)
			{
				if (activePoint != minPoint)
				{
					if (activePoint)
						if (activePoint->state != Point::PointState::SELECTED) // TODO MAYBE BETTER METHOD
							activePoint->ChangeState(Point::PointState::INACTIVE);

					if (minPoint)
						if (minPoint->state != Point::PointState::SELECTED)  // TODO MAYBE BETTER METHOD
							minPoint->ChangeState(Point::PointState::HOVERED);

					activePoint = minPoint;
				}

				if (activeRotation)
				{
					if (activeRotation->state != Rotation::RotationState::SELECTED) // TODO MAYBE BETTER METHOD
						activeRotation->ChangeState(Rotation::RotationState::INACTIVE);

					activeRotation = 0;
				}
			}
			else if (minRotation)
			{
				if (activeRotation != minRotation)
				{
					if (activeRotation)
						if (activeRotation->state != Rotation::RotationState::SELECTED) // TODO MAYBE BETTER METHOD
							activeRotation->ChangeState(Rotation::RotationState::INACTIVE);

					if (minRotation)
						if (minRotation->state != Rotation::RotationState::SELECTED)  // TODO MAYBE BETTER METHOD
							minRotation->ChangeState(Rotation::RotationState::HOVERED);

					activeRotation = minRotation;
				}

				if (activePoint)
				{
					if (activePoint->state != Point::PointState::SELECTED) // TODO MAYBE BETTER METHOD
						activePoint->ChangeState(Point::PointState::INACTIVE);

					activePoint = 0;
				}
			}
			else
			{
				if (activeRotation)
				{
					if (activeRotation->state != Rotation::RotationState::SELECTED) // TODO MAYBE BETTER METHOD
						activeRotation->ChangeState(Rotation::RotationState::INACTIVE);

					activeRotation = 0;
				}
				else if (activePoint)
				{
					if (activePoint->state != Point::PointState::SELECTED) // TODO MAYBE BETTER METHOD
						activePoint->ChangeState(Point::PointState::INACTIVE);

					activePoint = 0;
				}
			}

		}
	}
	else {

		mousePos.y = SCRHEIGHT - y;
		mousePos.x = x - SCRWIDTHOFFSET;	   // Add x-screen offset (to not overlap with IMGUI)

		mouseGridPos = ToGridPos(mousePos + int2(SCRWIDTHOFFSET, 0)) ;

		if (currentGameState == Edit)
		{
			// Move point
			if (selectedPoint)
			{
				if (selectedPoint->state == Point::PointState::MOVE)
				{
					if (mousePos.x >= 0)
						selectedPoint->Position(mouseGridPos.x, mouseGridPos.y, precisionLevel);
					else
						selectedPoint->Position(0, mouseGridPos.y, precisionLevel);
					return;
				}
			}

			Point* minPoint = 0;
			float minDist = std::numeric_limits<float>::max();
			for (int i = 0; i < scene.spiroplot2D.totalPoints; i++)
			{
				Point* point = &scene.spiroplot2D.points[i];
				float dist = magnitude(mouseGridPos - point->Position());

				if (dist <= pointSize && dist < minDist)
				{
					minPoint = point;
					minDist = dist;
				}
			}

			Rotation* minRotation = 0;
			if (minPoint == 0)
			{
				for (int i = 0; i < scene.spiroplot2D.totalRotations; i++)
				{
					Rotation* rotation = &scene.spiroplot2D.rotations[i];
					float2 p1 = rotation->p1->Position(), p2 = rotation->p2->Position();

					float2 dir = p2 - p1;
					float segmentLength = magnitude(dir);
					float2 normDir = dir / segmentLength;
					float2 toPoint = mouseGridPos - p1;
					float projectionLength = dot(toPoint, normDir);
					float2 closestPoint;
					if (projectionLength < 0) {
						closestPoint = p1;
					}
					else if (projectionLength > segmentLength) {
						closestPoint = p2;
					}
					else {
						closestPoint = p1 + projectionLength * normDir;
					}

					float dist = magnitude(closestPoint - mouseGridPos);

					if (dist <= lineSize && dist < minDist)
					{
						if (minPoint)
							minPoint = 0;

						minRotation = rotation;
						minDist = dist;
					}
				}
			}

			if (minPoint)
			{
				if (activePoint != minPoint)
				{
					if (activePoint)
						if (activePoint->state != Point::PointState::SELECTED) // TODO MAYBE BETTER METHOD
							activePoint->ChangeState(Point::PointState::INACTIVE);

					if (minPoint)
						if (minPoint->state != Point::PointState::SELECTED)  // TODO MAYBE BETTER METHOD
							minPoint->ChangeState(Point::PointState::HOVERED);

					activePoint = minPoint;
				}

				if (activeRotation)
				{
					if (activeRotation->state != Rotation::RotationState::SELECTED) // TODO MAYBE BETTER METHOD
						activeRotation->ChangeState(Rotation::RotationState::INACTIVE);

					activeRotation = 0;
				}
			}
			else if (minRotation)
			{
				if (activeRotation != minRotation)
				{
					if (activeRotation)
						if (activeRotation->state != Rotation::RotationState::SELECTED) // TODO MAYBE BETTER METHOD
							activeRotation->ChangeState(Rotation::RotationState::INACTIVE);

					if (minRotation)
						if (minRotation->state != Rotation::RotationState::SELECTED)  // TODO MAYBE BETTER METHOD
							minRotation->ChangeState(Rotation::RotationState::HOVERED);

					activeRotation = minRotation;
				}

				if (activePoint)
				{
					if (activePoint->state != Point::PointState::SELECTED) // TODO MAYBE BETTER METHOD
						activePoint->ChangeState(Point::PointState::INACTIVE);

					activePoint = 0;
				}
			}
			else
			{
				if (activeRotation)
				{
					if (activeRotation->state != Rotation::RotationState::SELECTED) // TODO MAYBE BETTER METHOD
						activeRotation->ChangeState(Rotation::RotationState::INACTIVE);

					activeRotation = 0;
				}
				else if (activePoint)
				{
					if (activePoint->state != Point::PointState::SELECTED) // TODO MAYBE BETTER METHOD
						activePoint->ChangeState(Point::PointState::INACTIVE);

					activePoint = 0;
				}
			}
		}
	}
}

void Renderer::Zoom(double zoom)
{
	camera2D->Zoom(zoom);
	// TODO KEEP BLACK BAR AT THE LEFT
	memset(screen->pixels, 0, SCRWIDTH * SCRHEIGHT * 4);
}

void Renderer::Move(double value, bool isVertical)
{
	camera2D->Move(value, isVertical, scene.spiroplot2D.gridWidth, scene.spiroplot2D.gridHeight);
	// TODO KEEP BLACK BAR AT THE LEFT
	memset(screen->pixels, 0, SCRWIDTH * SCRHEIGHT * 4);
}

void Renderer::ChangeGameState() {
	if (currentGameState == Edit || currentGameState == Play)
	{
		if (is3D)
		{
			if (currentGameState == Edit && scene.spiroplot3D.totalRotations > 0)
			{
				if (activePoint)
				{
					activePoint->ChangeState(Point::PointState::INACTIVE);
					activePoint = 0;
				}

				if (selectedPoint)
				{
					selectedPoint->ChangeState(Point::PointState::INACTIVE);
					selectedPoint = 0;
				}

				if (activeRotation)
				{
					activeRotation->ChangeState(Rotation::RotationState::INACTIVE);
					activeRotation = 0;
				}

				if (selectedRotation)
				{
					selectedRotation->ChangeState(Rotation::RotationState::INACTIVE);
					selectedRotation = 0;
				}
				currentGameState = Play;
			}
			else
				currentGameState = Edit;
		}
		else
		{
			if (currentGameState == Edit && scene.spiroplot2D.totalRotations > 0)
			{
				if (activePoint)
				{
					activePoint->ChangeState(Point::PointState::INACTIVE);
					activePoint = 0;
				}

				if (selectedPoint)
				{
					selectedPoint->ChangeState(Point::PointState::INACTIVE);
					selectedPoint = 0;
				}

				if (activeRotation)
				{
					activeRotation->ChangeState(Rotation::RotationState::INACTIVE);
					activeRotation = 0;
				}

				if (selectedRotation)
				{
					selectedRotation->ChangeState(Rotation::RotationState::INACTIVE);
					selectedRotation = 0;
				}
				currentGameState = Play;
			}
			else
				currentGameState = Edit;
		}
	}
}

void Renderer::ChangeAnimationState() {
	if (currentGameState == Edit)
	{
		if (is3D)
			return;

		if (scene.spiroplot2D.totalRotations > 0)
		{
			animationOriginalP1PosX = scene.spiroplot2D.CurrentRotation().p1->GetX();
			animationOriginalP1PosY = scene.spiroplot2D.CurrentRotation().p1->GetY();

			animationOriginalP2PosX = scene.spiroplot2D.CurrentRotation().p2->GetX();
			animationOriginalP2PosY = scene.spiroplot2D.CurrentRotation().p2->GetY();

			animationTimer.reset();
			if (activePoint)
			{
				activePoint->ChangeState(Point::PointState::INACTIVE);
				activePoint = 0;
			}

			if (selectedPoint)
			{
				selectedPoint->ChangeState(Point::PointState::INACTIVE);
				selectedPoint = 0;
			}

			if (activeRotation)
			{
				activeRotation->ChangeState(Rotation::RotationState::INACTIVE);
				activeRotation = 0;
			}

			if (selectedRotation)
			{
				selectedRotation->ChangeState(Rotation::RotationState::INACTIVE);
				selectedRotation = 0;
			}

			currentGameState = Animation;
		}
	}
}

void Renderer::ChangePrecision(PrecisionLevel newPrecisionLevel) {
	precisionLevel = newPrecisionLevel;
	scene.UpdatePrecision(precisionLevel);
}

void Renderer::ChangePlotState(PlotState newPlotState) {
	plotState = newPlotState;
	scene.UpdatePlotState(plotState);
}

void Renderer::ChangeRotationMethod(Rotation::RotationMethod newRotationMethod) {
	currentRotationMethod = newRotationMethod;
}
void Renderer::ChangeDevice() {
#ifndef GPU
	return;
#endif

	if (!is3D || currentGameState == Animation)
		return;

	useGPU = !useGPU;
	
	if (useGPU)
	{
		scene.spiroplot3D.gridBuffer->CopyToDevice(false);
		scene.spiroplot3D.downSampledGridBuffer->CopyToDevice(false);
		scene.spiroplot3D.bboxBuffer->CopyToDevice(false);
		cameraPropBuffer->CopyToDevice(true);

		generatePrimaryRaysKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);

		if (fast)
		{
			initialExtendKernelFast->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
			extendKernelFast->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
		}
		else
		{
			initialExtendKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
			extendKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
		}
		finalizeKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);

		deviceBuffer->CopyFromDevice(true);
	}
}

void Renderer::ChangeRenderMethod() {
	if (!is3D)
		return;

	fast = !fast;

	if (useGPU)
	{
		generatePrimaryRaysKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);

		if (fast)
		{
			initialExtendKernelFast->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
			extendKernelFast->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
		}
		else
		{
			initialExtendKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
			extendKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
		}
		finalizeKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);

		deviceBuffer->CopyFromDevice(true);
	}
}
void Renderer::DrawInputRotations()
{
	uint lineColor = 0x00000000;
	for (int i = 0; i < scene.spiroplot2D.totalRotations; i++)
	{
		Rotation rotation = scene.spiroplot2D.rotations[i];
		Point p1 = *rotation.p1, p2 = *rotation.p2;
		DrawLine(p1, p2, rotation.state);
	 }
}

void Renderer::Draw3DInputRotations()
{
	uint lineColor = 0x00000000;
	for (int i = 0; i < scene.spiroplot3D.totalRotations; i++)
	{
		Rotation rotation = scene.spiroplot3D.rotations[i];
		Point p1 = *rotation.p1, p2 = *rotation.p2;
		Draw3DLine(p1, p2, rotation.state);
	}
}

void Renderer::DrawLine(Point p1, Point p2, Rotation::RotationState lineState = Rotation::RotationState::INACTIVE)
{
	// PCHECK
	float2 p1_pos = p1.Position();
	float2 p2_pos = p2.Position();

	p1_pos = ToScreenPos(p1_pos);
	p2_pos = ToScreenPos(p2_pos);

	int iterations = 0;

	int dx = fabs(p2_pos.x - p1_pos.x);
	int dy = fabs(p2_pos.y - p1_pos.y);
	int sx = p1_pos.x < p2_pos.x ? 1 : -1;
	int sy = p1_pos.y < p2_pos.y ? 1 : -1;
	int err = dx - dy;

	while (magnitude(p1_pos - p2_pos) > pointSize/2 && iterations < 1000) {

		// TODO FIND BETTER METHOD
		int2 p1_grid_pos = ToGridPosInvertY(int2(p1_pos.x, p1_pos.y));
		if (p1_grid_pos.x >= 0 && p1_grid_pos.x < scene.spiroplot2D.gridWidth && p1_grid_pos.y >= 0 && p1_grid_pos.y < scene.spiroplot2D.gridHeight)
			DrawLinePoint(p1_pos, lineState);

		int e2 = 2 * err;
		if (e2 > -dy) {
			err -= dy;
			p1_pos.x += sx;
		}
		if (e2 < dx) {
			err += dx;
			p1_pos.y += sy;
		}
		iterations++;
	}

	int2 p1_grid_pos = ToGridPosInvertY(int2(p1_pos.x, p1_pos.y));
	if (p1_grid_pos.x >= 0 && p1_grid_pos.x < scene.spiroplot2D.gridWidth && p1_grid_pos.y >= 0 && p1_grid_pos.y < scene.spiroplot2D.gridHeight)
		DrawLinePoint(p1_pos, lineState);
}

void Renderer::Draw3DLine(Point p1, Point p2, Rotation::RotationState lineState = Rotation::RotationState::INACTIVE)
{
	// PCHECK
	float2 p1_pos = p1.Position();
	float2 p2_pos = p2.Position();

	float3 p1_screen_pos = ToScreenPos(p1.Position3D());
	float3 p2_screen_pos = ToScreenPos(p2.Position3D());

	p1_pos.x = p1_screen_pos.x, p1_pos.y = p1_screen_pos.y;
	p2_pos.x = p2_screen_pos.x, p2_pos.y = p2_screen_pos.y;

	int iterations = 0;

	int dx = fabs(p2_pos.x - p1_pos.x);
	int dy = fabs(p2_pos.y - p1_pos.y);
	int sx = p1_pos.x < p2_pos.x ? 1 : -1;
	int sy = p1_pos.y < p2_pos.y ? 1 : -1;
	int err = dx - dy;

	while (magnitude(p1_pos - p2_pos) > pointSize / 2 && iterations < 1000) {

		// TODO FIND BETTER METHOD
		int2 p1_grid_pos = ToGridPosInvertY(int2(p1_pos.x, p1_pos.y));
		if (p1_grid_pos.x >= 0 && p1_grid_pos.x < scene.spiroplot2D.gridWidth && p1_grid_pos.y >= 0 && p1_grid_pos.y < scene.spiroplot2D.gridHeight)
			DrawLinePoint(p1_pos, lineState);

		int e2 = 2 * err;
		if (e2 > -dy) {
			err -= dy;
			p1_pos.x += sx;
		}
		if (e2 < dx) {
			err += dx;
			p1_pos.y += sy;
		}
		iterations++;
	}

	int2 p1_grid_pos = ToGridPosInvertY(int2(p1_pos.x, p1_pos.y));
	if (p1_grid_pos.x >= 0 && p1_grid_pos.x < scene.spiroplot2D.gridWidth && p1_grid_pos.y >= 0 && p1_grid_pos.y < scene.spiroplot2D.gridHeight)
		DrawLinePoint(p1_pos, lineState);
}

void Renderer::DrawLinePoint(float2 point_pos, Rotation::RotationState lineState) {

	if (lineState == Rotation::RotationState::HOVERED || lineState == Rotation::RotationState::SELECTED)
	{
		for (int x = point_pos.x - lineSize - lineGlowRadius; x <= point_pos.x + lineSize + lineGlowRadius; x++) {
			for (int y = point_pos.y - lineSize - lineGlowRadius; y <= point_pos.y + lineSize + lineGlowRadius; y++) {
				// Check if the coords are within the bounds of the grid 
				if (x >= SCRWIDTHOFFSET && x < SCRWIDTH && y >= 0 && y < SCRHEIGHT) {

					// Calculate the distance between the current point and the center of the circle
					double distance = sqrt(pow(x - point_pos.x, 2) + pow(y - point_pos.y, 2));

					// If the distance is less than or equal to the radius, mark the point as part of the glow
					if (distance > lineSize && distance <= lineSize + lineGlowRadius) {
						if (screen->pixels[x + y * SCRWIDTH] != lineColor + 1) // TODO MAYBE BETTER METHOD
						{
							if (lineState == Rotation::RotationState::HOVERED)
								screen->pixels[x + y * SCRWIDTH] = 0x69b2ce;
							else
								screen->pixels[x + y * SCRWIDTH] = 0xfa9a50;
						}
					}
					else if (distance <= lineSize)
					{
						screen->pixels[x + y * SCRWIDTH] = lineColor + 1; // TODO MAYBE BETTER METHOD
					}
				}
			}
		}
	}
	else
	{
		for (int x = point_pos.x - pointSize; x <= point_pos.x + pointSize; x++) {
			for (int y = point_pos.y - pointSize; y <= point_pos.y + pointSize; y++) {

				// Check if the coords are within the bounds of the grid 
				if (x >= SCRWIDTHOFFSET && x < SCRWIDTH && y >= 0 && y < SCRHEIGHT) {
					// Calculate the distance between the current point and the center of the circle
					double distance = sqrt(pow(x - point_pos.x, 2) + pow(y - point_pos.y, 2));

					if (distance <= lineSize)
					{
						screen->pixels[x + y * SCRWIDTH] = lineColor;
					}
				}
			}
		}
	}
}

void Renderer::DrawInputPoints()
{
	for (int i = 0; i < scene.spiroplot2D.totalPoints; i++)
	{
		Point point = scene.spiroplot2D.points[i];

		float2 point_pos = point.Position();

		if (point_pos.x < 0 || point_pos.x >= scene.spiroplot2D.gridWidth || point_pos.y < 0 || point_pos.y >= scene.spiroplot2D.gridHeight)
			continue;

		// Convert to Screen Pos
		point_pos = ToScreenPos(point_pos);

		// Add Glow To hovered / selected point
		if (point.state == Point::PointState::HOVERED || point.state == Point::PointState::SELECTED)
		{
			for (int x = point_pos.x - pointSize - pointGlowRadius; x <= point_pos.x + pointSize + pointGlowRadius; x++) {
				for (int y = point_pos.y - pointSize - pointGlowRadius; y <= point_pos.y + pointSize + pointGlowRadius; y++) {
					// Check if the coords are within the bounds of the grid 
					if (x >= SCRWIDTHOFFSET && x < SCRWIDTH && y >= 0 && y < SCRHEIGHT) {
						// Calculate the distance between the current point and the center of the circle
						double distance = sqrt(pow(x - point_pos.x, 2) + pow(y - point_pos.y, 2));

						// If the distance is less than or equal to the radius, mark the point as part of the glow
						if (distance > pointSize && distance <= pointSize + pointGlowRadius) {
							if (point.state == Point::PointState::HOVERED)
								screen->pixels[x + y * SCRWIDTH] = 0x69b2ce;
							else
								screen->pixels[x + y * SCRWIDTH] = 0xfa9a50;
						}
						else if (distance <= pointSize)
						{
							if (distance >= pointSize * 0.8)
								screen->pixels[x + y * SCRWIDTH] = 0; // Add black border
							else
								screen->pixels[x + y * SCRWIDTH] = point.color;
						}
					}
				}
			}
		}
		else
		{
			for (int x = point_pos.x - pointSize; x <= point_pos.x + pointSize; x++) {
				for (int y = point_pos.y - pointSize; y <= point_pos.y + pointSize; y++) {

					// Check if the coords are within the bounds of the grid 
					if (x >= SCRWIDTHOFFSET && x < SCRWIDTH && y >= 0 && y < SCRHEIGHT) {
						// Calculate the distance between the current point and the center of the circle
						double distance = sqrt(pow(x - point_pos.x, 2) + pow(y - point_pos.y, 2));

						// If the distance is less than or equal to the radius, mark the point as part of the circle
						if (distance <= pointSize) {
							if (distance >= pointSize * 0.8)
								screen->pixels[x + y * SCRWIDTH] = 0; // Add black border
							else
								screen->pixels[x + y * SCRWIDTH] = point.color;
						}
					}
				}
			}
		}
	}
}

void Renderer::Draw3DInputPoints()
{
	for (int i = 0; i < scene.spiroplot3D.totalPoints; i++)
	{
		Point point = scene.spiroplot3D.points[i];

		float3 point_pos = point.Position3D();

		if (point_pos.x < 0 || point_pos.x >= scene.spiroplot3D.gridWidth || point_pos.y < 0 || point_pos.y >= scene.spiroplot3D.gridHeight || point_pos.z < 0 || point_pos.z >= scene.spiroplot3D.gridDepth)
			continue;

		// Convert to Screen Pos
		float3 point_screen_pos = ToScreenPos(point_pos);

		// Add Glow To hovered / selected point
		if (point.state == Point::PointState::HOVERED || point.state == Point::PointState::SELECTED)
		{
			for (int x = point_screen_pos.x - pointSize * point_screen_pos.z - pointGlowRadius * point_screen_pos.z; x <= point_screen_pos.x + pointSize * point_screen_pos.z + pointGlowRadius * point_screen_pos.z; x++) {
				for (int y = point_screen_pos.y - pointSize * point_screen_pos.z - pointGlowRadius * point_screen_pos.z; y <= point_screen_pos.y + pointSize * point_screen_pos.z + pointGlowRadius * point_screen_pos.z; y++) {
					// Check if the coords are within the bounds of the grid 
					if (x >= SCRWIDTHOFFSET && x < SCRWIDTH && y >= 0 && y < SCRHEIGHT) {
						// Calculate the distance between the current point and the center of the circle
						double distance = sqrt(pow(x - point_screen_pos.x, 2) + pow(y - point_screen_pos.y, 2));

						// If the distance is less than or equal to the radius, mark the point as part of the glow
						if (distance > pointSize * point_screen_pos.z && distance <= pointSize * point_screen_pos.z + pointGlowRadius * point_screen_pos.z) {
							if (point.state == Point::PointState::HOVERED)
								screen->pixels[x + y * SCRWIDTH] = 0x69b2ce;
							else
								screen->pixels[x + y * SCRWIDTH] = 0xfa9a50;
						}
						else if (distance <= pointSize * point_screen_pos.z)
						{
							if (distance >= pointSize * 0.8 * point_screen_pos.z)
								screen->pixels[x + y * SCRWIDTH] = 0; // Add black border
							else
								screen->pixels[x + y * SCRWIDTH] = point.color;
						}
					}
				}
			}
		}
		else
		{
			for (int x = point_screen_pos.x - pointSize * point_screen_pos.z; x <= point_screen_pos.x + pointSize * point_screen_pos.z; x++) {
				for (int y = point_screen_pos.y - pointSize * point_screen_pos.z; y <= point_screen_pos.y + pointSize * point_screen_pos.z; y++) {

					// Check if the coords are within the bounds of the Screen 
					if (x >= SCRWIDTHOFFSET && x < SCRWIDTH && y >= 0 && y < SCRHEIGHT) {
						// Calculate the distance between the current point and the center of the circle
						double distance = sqrt(pow(x - point_screen_pos.x, 2) + pow(y - point_screen_pos.y, 2));

						// If the distance is less than or equal to the radius, mark the point as part of the circle
						if (distance <= pointSize * point_screen_pos.z) {
							if (distance >= pointSize * 0.8 * point_screen_pos.z)
								screen->pixels[x + y * SCRWIDTH] = 0; // Add black border
							else
								screen->pixels[x + y * SCRWIDTH] = point.color;
						}
					}
				}
			}
		}
	}
}


float2 Renderer::ToScreenPos(float2 gridPos)
{
	float2 screenPos = camera2D->ToScreenPos(gridPos, scene.spiroplot2D.gridWidth, scene.spiroplot2D.gridHeight);
	return screenPos;
}

float3 Renderer::ToScreenPos(float3 gridPos)
{
	float3 screenPos = camera3D->ToScreenPos(gridPos);
	return screenPos;
}

int2 Renderer::ToGridPos(int2 screenPos)
{
	int2 gridPos = (camera2D->ToGridPos(screenPos, scene.spiroplot2D.gridWidth));
	return gridPos;
}

int2 Renderer::ToGridPosInvertY(int2 screenPos)
{
	int2 gridPos = camera2D->ToGridPosInvertY(screenPos);
	return gridPos;
}


void Renderer::CenterPoints()
{
	if (currentGameState == Edit)
		scene.spiroplot2D.CenterAndScalePoints(precisionLevel);
}

void Renderer::Clear() {
	//if (currentGameState == Edit)
	if (is3D)
	{
		scene.Clear(useGPU);

		if (useGPU)
		{
			generatePrimaryRaysKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);

			if (fast)
			{
				initialExtendKernelFast->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
				extendKernelFast->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
			}
			else
			{
				initialExtendKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
				extendKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
			}
			finalizeKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);

			deviceBuffer->CopyFromDevice(true);
		}
	}
	else
		scene.Clear();
}

void Renderer::Reset() {
	if (currentGameState == Edit)
	{
		activePoint = 0;
		activeRotation = 0;
		selectedPoint = 0;
		selectedRotation = 0;

		if (is3D)
		{
			scene.Reset(useGPU);
			camera3D->ResetToInitial();
			if (useGPU)
			{
				generatePrimaryRaysKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);

				if (fast)
				{
					initialExtendKernelFast->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
					extendKernelFast->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
				}
				else
				{
					initialExtendKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
					extendKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
				}
				finalizeKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);

				deviceBuffer->CopyFromDevice(true);
			}
		}
		else
			scene.Reset();
	}
}

void Renderer::Change2DSpiroplotResolution(int res)
{
	if (is3D) {
		ChangeDimension();
	}

	Reset();
	scene.Change2DSpiroplotResolution(res, plotState);
	camera2D->ResetWithZoom(GRIDWIDTH, GRIDHEIGHT, (double) GRIDWIDTH/(double) res);
	Move(moveSpeed, true);
	Move(-moveSpeed, true);
}

void Renderer::Load3DSpiroplotTemplate(int temp)
{
	if (!is3D) {
		ChangeDimension();
	}

	Reset();
	scene.Load3DSpiroplotTemplate(temp, useGPU);
	camera3D->ResetToInitial();
}
void Renderer::Export(string filename) {
	scene.Export(filename);
}

void Renderer::Export3D(string filename) {
	scene.Export3D(filename);
}

void Renderer::HideInput() {
	hideInput = !hideInput;
}

void Renderer::ChangeDimension()
{
	if (currentGameState == Animation)
		return;

	currentGameState = Edit;

	is3D = !is3D;

	// TODO CHANGE TO A ANIMATION TRANSITION
	memset(screen->pixels, 0, SCRWIDTH * SCRHEIGHT * 4);

	if (!is3D)
	{
		disPlayRotation = true;
		camera3D->Reset();
	} // TODO ALSO DO THIS FOR 2D CAMERA

	if (activePoint)
	{
		activePoint->ChangeState(Point::PointState::INACTIVE);
		activePoint = 0;
	}

	if (selectedPoint)
	{
		selectedPoint->ChangeState(Point::PointState::INACTIVE);
		selectedPoint = 0;
	}

	if (activeRotation)
	{
		activeRotation->ChangeState(Rotation::RotationState::INACTIVE);
		activeRotation = 0;
	}

	if (selectedRotation)
	{
		selectedRotation->ChangeState(Rotation::RotationState::INACTIVE);
		selectedRotation = 0;
	}
}
// -----------------------------------------------------------
// Main application tick function - Executed once per frame
// -----------------------------------------------------------

void Renderer::Tick(float deltaTime)
{
	delta_prev_time = deltaTime;
	//memset(screen->pixels, 0, SCRWIDTH * SCRHEIGHT * 4);

	if (!scene.isInitialized)
		return;

	static bool firstTick = true;
	if (firstTick)
	{
#ifdef GPU
		initialExtendKernel->SetArguments(pixelIdxBuffer, originBuffer, directionBuffer,
			scene.spiroplot3D.gridWidth, scene.spiroplot3D.gridHeight, scene.spiroplot3D.gridDepth);

		extendKernel->SetArguments(pixelIdxBuffer, originBuffer, directionBuffer,
			pixelColorBuffer, scene.spiroplot3D.gridBuffer,
			scene.spiroplot3D.gridWidth, scene.spiroplot3D.gridHeight, scene.spiroplot3D.gridDepth, backgroundColorBuffer);

		scene.spiroplot3D.gridBuffer->CopyToDevice(false);

		initialExtendKernelFast->SetArguments(pixelIdxBuffer, originBuffer, directionBuffer,
			scene.spiroplot3D.gridWidth, scene.spiroplot3D.gridHeight, scene.spiroplot3D.gridDepth,
			scene.spiroplot3D.bboxBuffer);

		scene.spiroplot3D.bboxBuffer->CopyToDevice(false);

		extendKernelFast->SetArguments(pixelIdxBuffer, originBuffer, directionBuffer,
			pixelColorBuffer, scene.spiroplot3D.gridBuffer, scene.spiroplot3D.downSampledGridBuffer,
			scene.spiroplot3D.gridWidth, scene.spiroplot3D.gridHeight, scene.spiroplot3D.gridDepth, backgroundColorBuffer,
			scene.spiroplot3D.gridWidthBlock, scene.spiroplot3D.gridHeightBlock, scene.spiroplot3D.gridDepthBlock,
			scene.spiroplot3D.bboxBuffer);

		scene.spiroplot3D.downSampledGridBuffer->CopyToDevice(false);

		updateLIFOKernel->SetArguments(scene.spiroplot3D.updatedCellsBuffer,
			scene.spiroplot3D.gridBuffer, scene.spiroplot3D.downSampledGridBuffer,
			scene.spiroplot3D.gridWidth, scene.spiroplot3D.gridHeight, scene.spiroplot3D.gridDepth, scene.spiroplot3D.pointSize,
			scene.spiroplot3D.gridWidthBlock, scene.spiroplot3D.gridHeightBlock, scene.spiroplot3D.gridDepthBlock);

		updateFIFOKernel->SetArguments(scene.spiroplot3D.updatedCellsBuffer,
			scene.spiroplot3D.gridBuffer, scene.spiroplot3D.downSampledGridBuffer,
			scene.spiroplot3D.gridWidth, scene.spiroplot3D.gridHeight, scene.spiroplot3D.gridDepth, scene.spiroplot3D.pointSize,
			scene.spiroplot3D.gridWidthBlock, scene.spiroplot3D.gridHeightBlock, scene.spiroplot3D.gridDepthBlock,
			backgroundColorBuffer);

		scene.spiroplot3D.updatedCellsBuffer->CopyToDevice(true);
#endif
		firstTick = false;
	}

	Timer t;

	// TODO CHANGE LATER
	if (!is3D)
	{
		if (currentGameState == Play)
		{
			int i = 0;
			int max = 1;

			if (speed == Fast)
				max = 100;
			else if (speed == UltraFast)
				max = 1000;

			do
			{
				scene.spiroplot2D.Iterate();
				i++;
			} while (i < max);
		}
		else if (currentGameState == Animation)
		{
			float elapsedAniTimeS = animationTimer.elapsed(); // TODO CHECK WHY THIS ISNT IN SECONDS
			if (elapsedAniTimeS < animationTime)
			{
				scene.spiroplot2D.AnimatedRotation(elapsedAniTimeS / animationTime, animationOriginalP1PosX, animationOriginalP1PosY, animationOriginalP2PosX, animationOriginalP2PosY, precisionLevel, false);
			}
			else
			{
				scene.spiroplot2D.AnimatedRotation(elapsedAniTimeS / animationTime, animationOriginalP1PosX, animationOriginalP1PosY, animationOriginalP2PosX, animationOriginalP2PosY, precisionLevel, true);
				currentGameState = Edit;
			}
		}

		if (plotState != ORDER)
		{
#pragma omp parallel for schedule(dynamic)
			for (int y = 0; y < SCRHEIGHT; y++)
			{
				for (int dest = y * SCRWIDTH, x = SCRWIDTHOFFSET; x < SCRWIDTH; x++)
				{
					int2 pixel = ToGridPosInvertY(int2(x, y));

					// Ensure indices are within grid boundaries
					if (pixel.x < 0 || pixel.x >= scene.spiroplot2D.gridWidth || pixel.y < 0 || pixel.y >= scene.spiroplot2D.gridHeight) {
						continue;
					}

					uint pixelColor = scene.spiroplot2D.grid[pixel.x + pixel.y * scene.spiroplot2D.gridWidth];

					if (scene.spiroplot2D.HiddenPoint(pixelColor >> 24))
					{
						screen->pixels[dest + x] = backgroundColor;
					}
					else
						screen->pixels[dest + x] = pixelColor & 0xFFFFFF;
				}
			}
		}
		else
		{
			std::vector<Point> plotPoints = scene.spiroplot2D.points;

			plotPoints.erase(std::remove_if(plotPoints.begin(), plotPoints.end(),
				[this](const Point& point) {
					return scene.spiroplot2D.HiddenPoint(point.GetID());
				}), plotPoints.end());

			std::sort(plotPoints.begin(), plotPoints.end(),
				[this](const Point& a, const Point& b) {
					auto it_a = std::find(scene.spiroplot2D.pointsOrder.begin(), scene.spiroplot2D.pointsOrder.end(), a.GetID());
					auto it_b = std::find(scene.spiroplot2D.pointsOrder.begin(), scene.spiroplot2D.pointsOrder.end(), b.GetID());
					return it_a < it_b;
				});

#pragma omp parallel for schedule(dynamic)
			for (int y = 0; y < SCRHEIGHT; y++)
			{
				for (int dest = y * SCRWIDTH, x = SCRWIDTHOFFSET; x < SCRWIDTH; x++)
				{
					int2 pixel = ToGridPosInvertY(int2(x, y));

					// Ensure indices are within grid boundaries
					if (pixel.x < 0 || pixel.x >= scene.spiroplot2D.gridWidth || pixel.y < 0 || pixel.y >= scene.spiroplot2D.gridHeight) {
						continue;
					}

					uint pixelColor = backgroundColor;
					for (int i = 0; i < plotPoints.size(); i++)
					{
						pixelColor = plotPoints[i].grid[pixel.x + pixel.y * scene.spiroplot2D.gridWidth];
						if (pixelColor != backgroundColor)
						{
							screen->pixels[dest + x] = pixelColor & 0xFFFFFF;
							break;
						}
					}
					screen->pixels[dest + x] = pixelColor & 0xFFFFFF;
				}
			}
		}

		if (currentGameState == Edit)
		{
			if (selectedPoint)
			{
				Point mousePoint;

				if (activePoint && activePoint != selectedPoint)
					mousePoint = *activePoint;
				else
					mousePoint = Point(mouseGridPos.x, mouseGridPos.y, precisionLevel, 0, 0, 0xFF0000, true);

				DrawLine(*selectedPoint, mousePoint);
			}
		}

		if (!hideInput)
		{
			DrawInputRotations();
			DrawInputPoints();
		}
	}
	else {

		bool hasMoved = camera3D->Update();
		bool newPoint = false;
		int maxI = 1;
		if (currentGameState == Play)
		{
			int i = 0;
			if (speed == Fast)
				maxI = 100;
			else if (speed == UltraFast)
				maxI = 1000;

			if (useGPU)
			{
				do
				{
					scene.spiroplot3D.Iterate(currentRotationMethod, i);
					i++;
				} while (i < maxI);
				scene.spiroplot3D.bboxBuffer->CopyToDevice(false);
				scene.spiroplot3D.updatedCellsBuffer->CopyToDevice(true);
				
				if(scene.spiroplot3D.plotState == LIFO)
					updateLIFOKernel->Run(maxI * 2);
				else if (scene.spiroplot3D.plotState == FIFO)
					updateFIFOKernel->Run(maxI * 2);

				//scene.spiroplot3D.gridBuffer->CopyToDevice(false);
				//scene.spiroplot3D.downSampledGridBuffer->CopyToDevice(false);

			}
			else
			{
				do
				{
					scene.spiroplot3D.Iterate(currentRotationMethod);
					i++;
				} while (i < maxI);
			}
			newPoint = true;
		}

		if (useGPU)
		{
			if (hasMoved || newPoint)
			{
				if (hasMoved)
					cameraPropBuffer->CopyToDevice(true);

				generatePrimaryRaysKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);

				if (fast)
				{
					initialExtendKernelFast->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
					extendKernelFast->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
				}
				else
				{
					initialExtendKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
					extendKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);
				}
				finalizeKernel->Run((SCRWIDTH - SCRWIDTHOFFSET) * SCRHEIGHT);

				deviceBuffer->CopyFromDevice(true);
			}
		}
		else {
			if (fast)
			{
#pragma omp parallel for schedule(dynamic)
				for (int y = 0; y < SCRHEIGHT; y++)
				{
					for (int dest = y * SCRWIDTH, x = 0; x < SCRWIDTH - SCRWIDTHOFFSET; x++)
					{
						uint pixelColor = scene.spiroplot3D.GetPixelFast(camera3D->GetRay(x, y));
						screen->pixels[dest + x + SCRWIDTHOFFSET] = pixelColor & 0xFFFFFF;
					}
				}
			} else {
#pragma omp parallel for schedule(dynamic)
				for (int y = 0; y < SCRHEIGHT; y++)
				{
					for (int dest = y * SCRWIDTH, x = 0; x < SCRWIDTH - SCRWIDTHOFFSET; x++)
					{
						uint pixelColor = scene.spiroplot3D.GetPixel(camera3D->GetRay(x, y));
						screen->pixels[dest + x + SCRWIDTHOFFSET] = pixelColor & 0xFFFFFF;
					}
				}
			}
		}

		if (currentGameState == Edit)
		{
			if (selectedPoint && selectedPoint->state != Point::PointState::MOVE)
			{
				Point mousePoint;

				if (activePoint && activePoint != selectedPoint)
				{
					mousePoint = *activePoint;
					float3 mouse_screen_pos = ToScreenPos(mousePoint.Position3D());
					mousePoint.SetX(mouse_screen_pos.x - SCRWIDTHOFFSET), mousePoint.SetY(SCRHEIGHT - mouse_screen_pos.y);
				}
				else
					mousePoint = Point(mousePos.x - SCRWIDTHOFFSET, SCRHEIGHT - mousePos.y, precisionLevel, 0, 0, 0xFF0000, true);
				
				Point origin = *selectedPoint;
				float3 screen_origin_pos = ToScreenPos(origin.Position3D());
				origin.SetX(screen_origin_pos.x - SCRWIDTHOFFSET), origin.SetY( SCRHEIGHT - screen_origin_pos.y);

				DrawLine(origin, mousePoint);
			}
		}

		if (!hideInput)
		{
			Draw3DInputRotations();
			Draw3DInputPoints();
		}
	}

	// performance report - running average - ms,
	static float avg = 10, alpha = 1;
	avg = (1 - alpha) * avg + alpha * t.elapsed() * 1000;
	if (alpha > 0.05f) alpha *= 0.5f;
	float fps = 1000 / avg, rps = (SCRWIDTH * SCRHEIGHT) * fps;
	//printf("%5.2fms (%.1fps)\n", avg, fps);

}

