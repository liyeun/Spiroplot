#pragma once

namespace Tmpl8
{

#define GPU

class Renderer : public TheApp
{
public:
	bool UI_item_selected = false;
	float delta_prev_time = 0;

	enum GameState { Edit, Play, Animation };
	enum SpeedState { Normal, Fast, UltraFast };

	// game flow methods
	void Init();
	float3 Trace( Ray& ray, int recursion_depth);
	float3 Sample(Ray& ray);
	void Tick( float deltaTime );
	void Shutdown() { /* implement if you want to do something on exit */ }
	// input handling
	void MouseUp(int button);
	void MouseDown(int button);
	void MouseMove(int x, int y);
	void MouseWheel( float y ) { /* implement if you want to handle the mouse wheel */ }
	void KeyUp(int key); //{ /* implement if you want to handle keys */ }
	void KeyDown(int key);// { /* implement if you want to handle keys */ }

	void ChangeGameState();
	void ChangeAnimationState();
	void ChangeDimension();
	void ChangePrecision(PrecisionLevel precisionlevel);
	void ChangePlotState(PlotState plotState);
	void ChangeRotationMethod(Rotation::RotationMethod rotateMethod);
	void ChangeDevice();
	void ChangeRenderMethod();

	void Change2DSpiroplotResolution(int res);
	void Load3DSpiroplotTemplate(int temp);

	float2 ToScreenPos(float2 pos);
	float3 ToScreenPos(float3 pos);

	int2 ToGridPos(int2 pos);
	int2 ToGridPosInvertY(int2 pos);

	void DrawInputRotations();
	void Draw3DInputRotations();
	void DrawLine(Point p1, Point p2, Rotation::RotationState lineState);
	void Draw3DLine(Point p1, Point p2, Rotation::RotationState lineState);
	void DrawLinePoint(float2 point_pos, Rotation::RotationState lineState);
	void DrawInputPoints();
	void Draw3DInputPoints();

	void Zoom(double zoom);
	void Move(double value, bool isVertical);

	void CenterPoints();
	void Export(string filename);
	void Export3D(string filename);
	void HideInput();
	void Clear();
	void Reset();
	// data members
	int2 mousePos;
	int2 mouseGridPos;

	Scene scene;
	Camera2D* camera2D = nullptr;
	Camera3D* camera3D = nullptr;

	GameState currentGameState = Edit;
	Point* activePoint = 0;
	Rotation* activeRotation = 0;
	Point* selectedPoint = 0;
	Rotation* selectedRotation = 0;

	int pointSize = 20;
	int pointGlowRadius = 5;


	int lineSize = 8;
	int lineGlowRadius = 5;
	uint lineColor = 0x9A9A9A;

	float animationTime = .2f;
	Number animationOriginalP1PosX, animationOriginalP1PosY;
	Number animationOriginalP2PosX, animationOriginalP2PosY;

	AnimationTimer animationTimer;

	bool hideInput = false;

	SpeedState speed = Normal;
	PrecisionLevel precisionLevel = PrecisionLevel::MPFR_256;
	bool is3D = false;
	PlotState plotState = LIFO;

	Rotation::RotationMethod currentRotationMethod = Rotation::RotationMethod::CenterMidPoint;
	bool disPlayRotation = true;

	// Define a threshold for double clicking (in milliseconds)
	const double doubleClickThreshold = 300;

	// Keep track of the time of the last left mouse button click
	chrono::time_point<std::chrono::system_clock> lastLeftClickTime = chrono::system_clock::now();


	double moveSpeed = 30;
	double zoomSpeed = 0.1;


	// GPGPU
	bool useGPU = false;
	bool fast = true;

	//Kernels
	static inline Kernel* generatePrimaryRaysKernel;
	static inline Kernel* initialExtendKernel;
	static inline Kernel* extendKernel;
	static inline Kernel* finalizeKernel;

	static inline Kernel* initialExtendKernelFast;
	static inline Kernel* extendKernelFast;

	static inline Kernel* updateLIFOKernel;
	static inline Kernel* updateFIFOKernel;
	//Buffers
	static inline Buffer* backgroundColorBuffer;
	// Screen Buffers
	static inline Buffer* deviceBuffer; // Buffer that stores and display the final pixel values
	static inline Buffer* pixelColorBuffer; 
	static inline Buffer* cameraPropBuffer;


	// Ray Buffers
	static inline Buffer* pixelIdxBuffer;
	static inline Buffer* originBuffer;
	static inline Buffer* directionBuffer;

	int* shadowBounceCounter;

	static inline Buffer* shadowBounceCounterBuffer;

	// Bounce Ray Buffers
	static inline Buffer* bounceCounterBuffer;
	static inline Buffer* bouncePixelIdxBuffer;


};

} // namespace Tmpl8