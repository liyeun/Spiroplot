__kernel void GeneratePrimaryRays(__global int* pixelIdxs, __global float4* origins, __global float4* directions,   // Primary Rays
__global float4* camProp, __global uint* pixelColors,                                                               // Camera Properties & Pixel color buffer
int scrWidth, int scrWidthOffset, int scrHeight, __global uint* backgroundColor)                                              // Static values
{
    int threadId = get_global_id(0);
    int xWidth = scrWidth - scrWidthOffset;
    
    int x = threadId % xWidth;
    int y = threadId / xWidth;

    float u = (float)x * (1.0f / xWidth);
	float v = (float)y * (1.0f / scrHeight);

    float4 P = camProp[1] + u * (camProp[2] - camProp[1]) + v * (camProp[3] - camProp[1]);

    origins[threadId] = camProp[0];

    directions[threadId] = normalize(P - camProp[0]);

    // Reset Buffers to initial values
    pixelIdxs[threadId] = threadId;
    pixelColors[threadId] = backgroundColor[0];
}