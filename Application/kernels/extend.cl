/*
__global int counter = 0;
__kernel void InitialExtend(__global int* pixelIdxs, __global float4* origins, __global float4* directions,   // Primary Rays
__global int* bouncePixelIdxs,
int gridWidth, int gridHeight, int gridDepth, uint backgroundColor)
{
    int threadId = get_global_id(0);
    
    int rayPixelIdx = pixelIdxs[threadId];

    float3 rayOrigin = origins[rayPixelIdx].xyz;
    float3 rayDirection = directions[rayPixelIdx].xyz;
    
    float3 rD = (float3)(1 / rayDirection.x, 1 / rayDirection.y, 1 / rayDirection.z);

    float tx1 = -rayOrigin.x * rD.x, tx2 = (gridWidth - rayOrigin.x) * rD.x;
    float tmin = min(tx1, tx2), tmax = max(tx1, tx2);
    float ty1 = -rayOrigin.y * rD.y, ty2 = (gridHeight - rayOrigin.y) * rD.y;
    tmin = max(tmin, min(ty1, ty2)), tmax = min(tmax, max(ty1, ty2));
    float tz1 = -rayOrigin.z * rD.z, tz2 = (gridDepth - rayOrigin.z) * rD.z;
    tmin = max(tmin, min(tz1, tz2)), tmax = min(tmax, max(tz1, tz2));

    origins[rayPixelIdx] += directions[rayPixelIdx] * tmin;

    __global int* bounceCounter = &rayCounters[1];
    int ri = atomic_inc(&counter);
    if(tmax >= tmin && tmax > 0)
    {
        bouncePixelIdxs[ri - 1] = rayPixelIdx;
    }

    if(ri == *bounceCounter - 1) 
    {
        counter = 0;
        rayCounters[0] = 0;
        rayCounters[1] = 0;
    }
}
*/
__kernel void InitialExtend(__global int* pixelIdxs, __global float4* origins, __global float4* directions,   // Primary Rays
int gridWidth, int gridHeight, int gridDepth)
{
    int threadId = get_global_id(0);
    //int rayPixelIdx = pixelIdxs[threadId];

    float3 rayOrigin = origins[threadId].xyz;
    float3 rayDirection = directions[threadId].xyz;
    
    float3 rD = (float3)(1.0f / rayDirection.x, 1.0f / rayDirection.y, 1.0f / rayDirection.z);

    float tx1 = -rayOrigin.x * rD.x, tx2 = (gridWidth - rayOrigin.x) * rD.x;
    float tmin = min(tx1, tx2), tmax = max(tx1, tx2);
    float ty1 = -rayOrigin.y * rD.y, ty2 = (gridHeight - rayOrigin.y) * rD.y;
    tmin = max(tmin, min(ty1, ty2)), tmax = min(tmax, max(ty1, ty2));
    float tz1 = -rayOrigin.z * rD.z, tz2 = (gridDepth - rayOrigin.z) * rD.z;
    tmin = max(tmin, min(tz1, tz2)), tmax = min(tmax, max(tz1, tz2));

    if(tmax >= tmin && tmax > 0){
        origins[threadId] += directions[threadId] * tmin;
    } else{
        origins[threadId] = (float4) (-2, -2, -2, -2);
    }
}

__kernel void Extend(__global int* pixelIdxs, __global float4* origins, __global float4* directions,   // Primary Rays
 __global uint* pixelColors, __global uint* grid,
int gridWidth, int gridHeight, int gridDepth,  __global uint* backgroundColor)
{
    int threadId = get_global_id(0);
    //int rayPixelIdx = pixelIdxs[threadId];
    float3 rayOrigin = origins[threadId].xyz;

    int3 currentPos;
    currentPos.x = floor(rayOrigin.x);
    currentPos.y = floor(rayOrigin.y);
    currentPos.z = floor(rayOrigin.z);

    if (currentPos.x < -1 || currentPos.x > gridWidth || currentPos.y < -1 || currentPos.y > gridHeight || currentPos.z < -1 || currentPos.z > gridDepth)
    {
        return;
    }

    float3 rayDirection = directions[threadId].xyz;

    float tDeltaX = (rayDirection.x == 0.0f) ? FLT_MAX : fabs(1.0 / rayDirection.x);
    float tDeltaY = (rayDirection.y == 0.0f) ? FLT_MAX  : fabs(1.0 / rayDirection.y);
    float tDeltaZ = (rayDirection.z == 0.0f) ? FLT_MAX  : fabs(1.0 / rayDirection.z);

    int stepX = (rayDirection.x > 0) ? 1 : -1;
    int stepY = (rayDirection.y > 0) ? 1 : -1;
    int stepZ = (rayDirection.z > 0) ? 1 : -1;

    float tMaxX = tDeltaX * ((stepX > 0) ? (currentPos.x + 1 - rayOrigin.x) : (currentPos.x - rayOrigin.x));
    float tMaxY = tDeltaY * ((stepY > 0) ? (currentPos.y + 1 - rayOrigin.y) : (currentPos.y - rayOrigin.y));
    float tMaxZ = tDeltaZ * ((stepZ > 0) ? (currentPos.z + 1 - rayOrigin.z) : (currentPos.z - rayOrigin.z));

    int justOutX = (stepX > 0) ? gridWidth : -1;
    int justOutY = (stepY > 0) ? gridHeight : -1;
    int justOutZ = (stepZ > 0) ? gridDepth : -1;
    
    while (true) {
        int cell = currentPos.x + gridWidth * (currentPos.y + gridHeight * currentPos.z);
        if (currentPos.x >= 0 && currentPos.x < gridWidth && currentPos.y >= 0 && currentPos.y < gridHeight && currentPos.z >= 0 && currentPos.z < gridDepth) {
            if (grid[cell] != backgroundColor[0])
            {
                pixelColors[threadId] = grid[cell];
                return;
            }
        }

        if (tMaxX < tMaxY) {
            if (tMaxX < tMaxZ) {
                currentPos.x += stepX;
                if (currentPos.x == justOutX)
                {
                    return;
                }
                tMaxX += tDeltaX;
            }
            else {
                currentPos.z += stepZ;
                if (currentPos.z == justOutZ)
                {
                    return;
                }
                tMaxZ += tDeltaZ;
            }
        }
        else {
            if (tMaxY < tMaxZ) {
                currentPos.y += stepY;
                if (currentPos.y == justOutY)
                {
                    return;
                }
                tMaxY += tDeltaY;
            }
            else {
                currentPos.z += stepZ;
                if (currentPos.z == justOutZ)
                {
                    return;
                }
                tMaxZ += tDeltaZ;
            }
        }
    }
}

__kernel void InitialExtendFast(__global int* pixelIdxs, __global float4* origins, __global float4* directions,   // Primary Rays
int gridWidth, int gridHeight, int gridDepth,
__global int* bbox)
{
    int threadId = get_global_id(0);
    //int rayPixelIdx = pixelIdxs[threadId];

    float3 rayOrigin = origins[threadId].xyz;
    float3 rayDirection = directions[threadId].xyz;
    
    float3 rD = (float3)(1.0f / rayDirection.x, 1.0f / rayDirection.y, 1.0f / rayDirection.z);

    float tx1 = (bbox[0] - rayOrigin.x) * rD.x, tx2 = (bbox[3] - rayOrigin.x) * rD.x;
    float tmin = min(tx1, tx2), tmax = max(tx1, tx2);
    float ty1 = (bbox[1] - rayOrigin.y) * rD.y, ty2 = (bbox[4] - rayOrigin.y) * rD.y;
    tmin = max(tmin, min(ty1, ty2)), tmax = min(tmax, max(ty1, ty2));
    float tz1 = (bbox[2] - rayOrigin.z) * rD.z, tz2 = (bbox[5] - rayOrigin.z) * rD.z;
    tmin = max(tmin, min(tz1, tz2)), tmax = min(tmax, max(tz1, tz2));

    if(tmax >= tmin && tmax > 0){
        origins[threadId] += directions[threadId] * tmin;
    } else{
        origins[threadId] = (float4) (-2, -2, -2, -2);
    }
}

__kernel void ExtendFast(__global int* pixelIdxs, __global float4* origins, __global float4* directions,   // Primary Rays
 __global uint* pixelColors, __global uint* grid, __global uint* downSampledGrid,
int gridWidth, int gridHeight, int gridDepth,  __global uint* backgroundColor,
int gridWidthBlock, int gridHeightBlock, int gridDepthBlock,
__global int* bbox)
{
    int threadId = get_global_id(0);
    //int rayPixelIdx = pixelIdxs[threadId];
    float3 rayOrigin = origins[threadId].xyz;
    float3 originalO = rayOrigin;

    int3 currentPos;
    currentPos.x = floor(rayOrigin.x);
    currentPos.y = floor(rayOrigin.y);
    currentPos.z = floor(rayOrigin.z);

    if (currentPos.x < bbox[0] - 1 || currentPos.x > bbox[3] + 1 || currentPos.y < bbox[1] - 1 || currentPos.y > bbox[4] + 1 || currentPos.z < bbox[2] - 1 || currentPos.z > bbox[5] + 1)
    {
        return;
    }

    int3 downSampledPos;
    downSampledPos.x = currentPos.x / gridWidthBlock;
    downSampledPos.y = currentPos.y / gridHeightBlock;
    downSampledPos.z = currentPos.z / gridDepthBlock;

    float3 rayDirection = directions[threadId].xyz;

    float downSampledtDeltaX = (rayDirection.x == 0.0f) ? FLT_MAX : fabs(gridWidthBlock / rayDirection.x);
    float downSampledtDeltaY = (rayDirection.y == 0.0f) ? FLT_MAX : fabs(gridHeightBlock / rayDirection.y);
    float downSampledtDeltaZ = (rayDirection.z == 0.0f) ? FLT_MAX : fabs(gridDepthBlock / rayDirection.z);

    int stepX = (rayDirection.x > 0) ? 1 : -1;
    int stepY = (rayDirection.y > 0) ? 1 : -1;
    int stepZ = (rayDirection.z > 0) ? 1 : -1;

    float downSampledtMaxX = downSampledtDeltaX * ((stepX > 0) ? (downSampledPos.x + 1 - rayOrigin.x / gridWidthBlock) : (rayOrigin.x / gridWidthBlock - downSampledPos.x));
    float downSampledtMaxY = downSampledtDeltaY * ((stepY > 0) ? (downSampledPos.y + 1 - rayOrigin.y / gridHeightBlock) : (rayOrigin.y / gridHeightBlock - downSampledPos.y));
    float downSampledtMaxZ = downSampledtDeltaZ * ((stepZ > 0) ? (downSampledPos.z + 1 - rayOrigin.z / gridDepthBlock) : (rayOrigin.z / gridDepthBlock - downSampledPos.z));

    int downSampledtjustOutX = (stepX > 0) ? ceil((float) bbox[3] / (float) gridWidthBlock) + 1 : floor((float) bbox[0] / (float) gridWidthBlock) - 1;
    int downSampledtjustOutY = (stepY > 0) ? ceil((float) bbox[4] / (float) gridHeightBlock) + 1: floor((float) bbox[1] / (float) gridHeightBlock) -1;
    int downSampledtjustOutZ = (stepZ > 0) ? ceil((float) bbox[5] / (float) gridDepthBlock) + 1: floor((float) bbox[2] / (float) gridDepthBlock) -1;

    float tDeltaX = (rayDirection.x == 0.0f) ? FLT_MAX : fabs(1.0 / rayDirection.x);
    float tDeltaY = (rayDirection.y == 0.0f) ? FLT_MAX  : fabs(1.0 / rayDirection.y);
    float tDeltaZ = (rayDirection.z == 0.0f) ? FLT_MAX  : fabs(1.0 / rayDirection.z);
    
    float tMaxX, tMaxY, tMaxZ;
    int justOutX, justOutY, justOutZ;
    while (true) {
        
        if (downSampledPos.x >= 0 && downSampledPos.x < gridWidth / gridWidthBlock && downSampledPos.y >= 0 && downSampledPos.y < gridHeight / gridHeightBlock && downSampledPos.z >= 0 && downSampledPos.z < gridDepth / gridDepthBlock) {
            if (downSampledGrid[downSampledPos.x + gridWidth / gridWidthBlock * (downSampledPos.y + gridHeight / gridHeightBlock * downSampledPos.z)] == 1)
            {
                rayOrigin = originalO;

                float tx1 = (downSampledPos.x * gridWidthBlock - rayOrigin.x) * rayDirection.x, tx2 = (((downSampledPos.x + 1) * gridWidthBlock - 1) - rayOrigin.x) * rayDirection.x;
                float tmin = min(tx1, tx2), tmax = max(tx1, tx2);
                float ty1 = (downSampledPos.y * gridHeightBlock - rayOrigin.y) * rayDirection.y, ty2 = (((downSampledPos.y + 1) * gridHeightBlock - 1) - rayOrigin.y) * rayDirection.y;
                tmin = max(tmin, min(ty1, ty2)), tmax = min(tmax, max(ty1, ty2));
                float tz1 = (downSampledPos.z * gridDepthBlock - rayOrigin.z) * rayDirection.z, tz2 = (((downSampledPos.z + 1) * gridDepthBlock - 1) - rayOrigin.z) * rayDirection.z;
                tmin = max(tmin, min(tz1, tz2)), tmax = min(tmax, max(tz1, tz2));

                rayOrigin += rayDirection * tmin;

                currentPos.x = floor(rayOrigin.x);
                currentPos.y = floor(rayOrigin.y);
                currentPos.z = floor(rayOrigin.z);

                tMaxX = tDeltaX * ((stepX > 0) ? (currentPos.x + 1 - rayOrigin.x) : (currentPos.x - rayOrigin.x));
                tMaxY = tDeltaY * ((stepY > 0) ? (currentPos.y + 1 - rayOrigin.y) : (currentPos.y - rayOrigin.y));
                tMaxZ = tDeltaZ * ((stepZ > 0) ? (currentPos.z + 1 - rayOrigin.z) : (currentPos.z - rayOrigin.z));
        
                justOutX = (stepX > 0) ? (downSampledPos.x + 1) * gridWidthBlock : downSampledPos.x * gridWidthBlock - 1;
                justOutY = (stepY > 0) ? (downSampledPos.y + 1) * gridHeightBlock : downSampledPos.y * gridHeightBlock - 1;
                justOutZ = (stepZ > 0) ? (downSampledPos.z + 1) * gridDepthBlock : downSampledPos.z * gridDepthBlock - 1;

                while (true) {
                    int cell = currentPos.x + gridWidth * (currentPos.y + gridHeight * currentPos.z);
                    if (currentPos.x >= 0 && currentPos.x < gridWidth && currentPos.y >= 0 && currentPos.y < gridHeight && currentPos.z >= 0 && currentPos.z < gridDepth) {
                        if (grid[cell] != backgroundColor[0])
                        {
                            pixelColors[threadId] = grid[cell];
                            return;
                        }
                    }

                    if (tMaxX < tMaxY) {
                        if (tMaxX < tMaxZ) {
                            currentPos.x += stepX;
                            if (currentPos.x == justOutX)
                                break;
                            tMaxX += tDeltaX;
                        }
                        else {
                            currentPos.z += stepZ;
                            if (currentPos.z == justOutZ)
                                break;
                            tMaxZ += tDeltaZ;
                        }
                    }
                    else {
                        if (tMaxY < tMaxZ) {
                            currentPos.y += stepY;
                            if (currentPos.y == justOutY)
                                break;
                            tMaxY += tDeltaY;
                        }
                        else {
                            currentPos.z += stepZ;
                            if (currentPos.z == justOutZ)
                                break;
                            tMaxZ += tDeltaZ;
                        }
                    }
                }
            }
        }

        if (downSampledtMaxX < downSampledtMaxY) {
            if (downSampledtMaxX < downSampledtMaxZ) {
                downSampledPos.x += stepX;
                if (downSampledPos.x == downSampledtjustOutX)
                    return;
                downSampledtMaxX += downSampledtDeltaX;
            }
            else {
                downSampledPos.z += stepZ;
                if (downSampledPos.z == downSampledtjustOutZ)
                    return;
                downSampledtMaxZ += downSampledtDeltaZ;
            }
        }
        else {
            if (downSampledtMaxY < downSampledtMaxZ) {
                downSampledPos.y += stepY;
                if (downSampledPos.y == downSampledtjustOutY)
                    return;
                downSampledtMaxY += downSampledtDeltaY;
            }
            else {
                downSampledPos.z += stepZ;
                if (downSampledPos.z == downSampledtjustOutZ)
                    return;
                downSampledtMaxZ += downSampledtDeltaZ;
            }
        }
    }
}