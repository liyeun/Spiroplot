#pragma once

// default screen resolution
#define SCRWIDTHOFFSET	290
#define SCRWIDTH	1090
#define SCRHEIGHT	800


#define GRIDWIDTH	800
#define GRIDHEIGHT	800
// #define FULLSCREEN
// #define DOUBLESIZE

namespace Tmpl8 {
	__declspec(align(64)) class Ray
	{
	public:
		Ray() = default;
		Ray(float3 origin, float3 direction, float distance = 1e34f)
		{
			O = origin, D = direction;

			rD.x = 1.0f / D.x;
			rD.y = 1.0f / D.y;
			rD.z = 1.0f / D.z;
		}
		// ray data
		float3 O, D, rD;
	};

	class Camera2D
	{
	public:
		Camera2D(int gridWidth, int gridHeight)
		{
			panX = gridWidth / 2 + panXOffset; // the x-coordinate of the center of the view
			panY = gridHeight / 2 + panYOffset; // the y-coordinate of the center of the view

			xRatio = gridWidth / (SCRWIDTH - SCRWIDTHOFFSET);
			yRatio = gridHeight / SCRHEIGHT;

			viewWidth = (SCRWIDTH - SCRWIDTHOFFSET) / zoomFactor / 2 * xRatio;
			viewHeight = SCRHEIGHT / zoomFactor / 2 * yRatio;

		}

		void Move(double value, bool isVertical, int gridWidth, int gridHeight)
		{
			if (isVertical)
				panYOffset += value;
			else
				panXOffset += value;

			panX = gridWidth / 2 + panXOffset; // the x-coordinate of the center of the view
			panY = gridHeight / 2 + panYOffset; // the y-coordinate of the center of the view

		}

		void Zoom(double zoom)
		{
			zoomFactor += zoom;
			if (zoomFactor < 0)
				zoomFactor = 0;

			viewWidth = (SCRWIDTH - SCRWIDTHOFFSET) / zoomFactor / 2 * xRatio;
			viewHeight = SCRHEIGHT / zoomFactor / 2 * yRatio;

			std::cout << zoomFactor << std::endl;
		}

		float2 ToScreenPos(float2 gridPos, int gridWidth, int gridHeight)
		{
			// Adjust for pan
			gridPos.x = (gridPos.x - panX) / (gridWidth / (SCRWIDTH - SCRWIDTHOFFSET) / (zoomFactor * (gridWidth/GRIDWIDTH)));
			gridPos.y = (gridPos.y - (gridHeight / 2 - panYOffset)) / (gridHeight / SCRHEIGHT / (zoomFactor * (gridWidth / GRIDWIDTH)));

			// Now, point_pos.x and point_pos.y are in screen coordinates with origin at the center of the screen. Let's shift the origin to the top-left corner.
			gridPos.x += (SCRWIDTH - SCRWIDTHOFFSET) / 2 + SCRWIDTHOFFSET;
			gridPos.y = SCRHEIGHT / 2 - gridPos.y; // We subtract y-coordinate because screen y-coordinates increase downwards

			return gridPos;
		}


		int2 ToGridPos(int2 screenPos, int gridHeight)
		{
			// Convert screen coordinates to image coordinates
			screenPos.x = ((screenPos.x - SCRWIDTHOFFSET) * 2.0 / (SCRWIDTH - SCRWIDTHOFFSET) - 1.0) * viewWidth + panX;
			screenPos.y = ((screenPos.y * 2.0 / SCRHEIGHT) - 1.0) * viewHeight + gridHeight / 2 - panYOffset;
			return screenPos;
		}


		int2 ToGridPosInvertY(int2 screenPos)
		{
			// Convert screen coordinates to image coordinates
			screenPos.x = ((screenPos.x - SCRWIDTHOFFSET) * 2.0 / (SCRWIDTH - SCRWIDTHOFFSET) - 1.0) * viewWidth + panX;
			screenPos.y = ((screenPos.y * 2.0 / SCRHEIGHT) - 1.0) * viewHeight + panY;
			return screenPos;
		}

		void Test()
		{
			std::cout << "Camera 2D OK" << std::endl;
			std::cout << panX << std::endl;
			std::cout << panY << std::endl;
			std::cout << viewWidth << std::endl;
			std::cout << viewHeight << std::endl;
			std::cout << xRatio << std::endl;
			std::cout << yRatio << std::endl;
		}

		double zoomFactor = 1.0;
		double zoomSpeed = 0.1;

		double panXOffset = 0.0, panYOffset = 0.0;

		double panX; // the x-coordinate of the center of the view
		double panY; // the y-coordinate of the center of the view

		double viewWidth;
		double viewHeight;

		double xRatio;
		double yRatio;

		void ResetWithZoom(int gridWidth, int gridHeight, double zoom) {
			zoomFactor = zoom;

			panXOffset = 0.0, panYOffset = 0.0;

			panX = gridWidth / 2 + panXOffset; // the x-coordinate of the center of the view
			panY = gridHeight / 2 + panYOffset; // the y-coordinate of the center of the view

			xRatio = gridWidth / (SCRWIDTH - SCRWIDTHOFFSET);
			yRatio = gridHeight / SCRHEIGHT;

			viewWidth = (SCRWIDTH - SCRWIDTHOFFSET) / zoomFactor / 2 * xRatio;
			viewHeight = SCRHEIGHT / zoomFactor / 2 * yRatio;

		}
	};

	class Camera3D
	{
	public:
		float delta_current_time = 0;

		enum Direction { Up, Down, Left, Right, In, Out };
		Camera3D(int gridWidth, int gridHeight)
		{
			// setup a basic view frustum
			camPos = float3(0, 0, -2);
			topLeft = float3(-aspect, 1, 0);
			topRight = float3(aspect, 1, 0);
			bottomLeft = float3(-aspect, -1, 0);

			float3 translation = float3(400, 400, -800);
			//float3 translation = float3(0, 0, -800);
			camPos += translation;
			topLeft += translation;
			topRight += translation;
			bottomLeft += translation;

			// GPU
			gpuCamProp = new float4[4]{ float4(camPos, 0) , float4(topLeft, 0), float4(topRight, 0), float4(bottomLeft, 0) };

		}

		float aspect = (float)(SCRWIDTH - SCRWIDTHOFFSET) / (float)SCRHEIGHT;
		float3 camPos;
		float3 topLeft, topRight, bottomLeft;
		float4* gpuCamProp;

		Ray GetRay(const int x, const int y)
		{
			// calculate pixel position on virtual screen plane
			const float u = (float)x * (1.0f / (SCRWIDTH - SCRWIDTHOFFSET));
			const float v = (float)y * (1.0f / SCRHEIGHT);
			const float3 P = topLeft + u * (topRight - topLeft) + v * (bottomLeft - topLeft);
			return Ray(camPos, normalize(P - camPos));
		}

		Ray Get3DRay(const int x, const int y)
		{
			// calculate pixel position on virtual screen plane
			const float u = (float)x * (1.0f / (SCRWIDTH - SCRWIDTHOFFSET));
			const float v = (float)y * (1.0f / SCRHEIGHT);
			const float3 P = topLeft + u * (topRight - topLeft) + v * (bottomLeft - topLeft);
			return Ray(camPos, normalize(P - camPos));
			//return Ray(camPos + normalize(P - camPos) * 1000,float3(0,0,1));
		}

		void Move(Direction direction, bool reverse, float delta_time)
		{
			if (reverse)
				moveDir[(int)direction] = false;
			else
				moveDir[(int)direction] = true;

			delta_current_time = delta_time;
		}

		void Zoom(Direction direction, bool reverse, float delta_time)
		{
			if (reverse)
				moveDir[(int)direction] = false;
			else
				moveDir[(int)direction] = true;

			delta_current_time = delta_time;
		}

		void Rotate(Direction direction, bool reverse, float delta_time)
		{
			if (reverse)
				rotateDir[(int)direction] = false;
			else
				rotateDir[(int)direction] = true;

			delta_current_time = delta_time;
		}

		bool Update()
		{
			bool moved = false;

			if (moveDir[0] || moveDir[1] || moveDir[2] || moveDir[3] || moveDir[4] || moveDir[5])
			{
				float3 Vdir = normalize(topLeft - bottomLeft) * speed * delta_current_time;
				float3 Hdir = normalize(topLeft - topRight) * speed * delta_current_time;

				float3 translation = float3(0, 0, 0);

				if (moveDir[0] != moveDir[1])
				{
					if (moveDir[0])
						translation += Vdir;
					if (moveDir[1])
						translation -= Vdir;
				}

				if (moveDir[2] != moveDir[3])
				{
					if (moveDir[2])
						translation += Hdir;
					if (moveDir[3])
						translation -= Hdir;
				}

				if (moveDir[4] != moveDir[5])
				{
					if (moveDir[4])
						translation += normalize((bottomLeft + (topLeft - bottomLeft) / 2 + (topRight - topLeft) / 2) - camPos) * speed * delta_current_time;
					if (moveDir[5])
						translation -= normalize((bottomLeft + (topLeft - bottomLeft) / 2 + (topRight - topLeft) / 2) - camPos) * speed * delta_current_time;
				}
				translation = normalize(translation) * speed * delta_current_time;

				if (magnitude(translation) != 0)
				{
					camPos += translation;
					topLeft += translation;
					topRight += translation;
					bottomLeft += translation;

					moved = true;
				}
			}

			if (rotateDir[0] || rotateDir[1] || rotateDir[2] || rotateDir[3])
			{
				float3 Vdir = normalize(topLeft - bottomLeft) * speed * delta_current_time;
				float3 Hdir = normalize(topLeft - topRight) * speed * delta_current_time;

				float3 rotation = float3(0, 0, 0);

				if (rotateDir[0])
					rotation -= Hdir;
				if (rotateDir[1])
					rotation += Hdir;
				if (rotateDir[2])
					rotation += Vdir;
				if (rotateDir[3])
					rotation -= Vdir;

				if (magnitude(rotation) != 0)
				{
					mat4 rMatrix = mat4::Rotate(normalize(rotation), angular_vel * delta_current_time);
					topLeft = (float3)(rMatrix * (topLeft - camPos)) + camPos;
					topRight = (float3)(rMatrix * (topRight - camPos)) + camPos;
					bottomLeft = (float3)(rMatrix * (bottomLeft - camPos)) + camPos;
					moved = true;
				}
			}

			// GPU
			gpuCamProp[0] = float4(camPos, 0);
			gpuCamProp[1] = float4(topLeft, 0);
			gpuCamProp[2] = float4(topRight, 0);
			gpuCamProp[3] = float4(bottomLeft, 0);

			return moved;
		}

		/*
		float2 ToScreenPos(float3 gridPos)
		{
			float3 verticalPlane = topLeft - bottomLeft;
			float3 horizontalPlane = topRight - topLeft;
			float3 bottomRight = bottomLeft + horizontalPlane;

			float2 screenPos;
			float3 rayD = camPos - gridPos;
			float mag = magnitude(rayD);
			rayD = rayD / mag;

			// No intersection if ray and plane are parallel
			float3 edge1 = topRight - topLeft;
			float3 edge2 = bottomLeft - topLeft;

			float3 h = cross(rayD, edge2);
			float a = dot(edge1, h);

			if (a > -0.0001f && a < 0.0001f) 
				screenPos = float2(-1,-1); // ray parallel to triangle
			else {
				const float f = 1 / a;
				const float3 s = gridPos - topLeft;
				const float u = f * dot(s, h);
				if (u < 0 || u > 1) screenPos = float2(-1, -1);
				else
				{
					const float3 q = cross(s, edge1);
					const float v = f * dot(rayD, q);
					if (v < 0 || u + v > 1) screenPos = float2(-1, -1);
					else {
						const float t = f * dot(edge2, q);
						if (t > 0.0001f && t < mag) {
							float3 planePos = gridPos + rayD * t;
							float v_t = dot(verticalPlane, planePos - bottomLeft);
							float h_t = dot(horizontalPlane, planePos - topLeft);
							if (v_t >= 1 || v_t < 0 || h_t >= 1 || h_t < 0) screenPos = float2(-1, -1);
							else screenPos = float2(h_t * (SCRWIDTH - SCRWIDTHOFFSET), v_t * SCRHEIGHT);
						} else screenPos = float2(-1, -1);
					}
				}
			}

			if (screenPos.x != -1 && screenPos.y != -1) return screenPos;
			else
			{
				float3 edge1 = topRight - bottomRight;
				float3 edge2 = bottomLeft - bottomRight;
				float3 h = cross(rayD, edge2);
				float a = dot(edge1, h);

				const float f = 1 / a;
				const float3 s = gridPos - bottomRight;
				const float u = f * dot(s, h);
				if (u < 0 || u > 1) screenPos = float2(-1, -1);
				else
				{
					const float3 q = cross(s, edge1);
					const float v = f * dot(rayD, q);
					if (v < 0 || u + v > 1) screenPos = float2(-1, -1);
					else {
						const float t = f * dot(edge2, q);
						if (t > 0.0001f && t < mag) {
							float3 planePos = gridPos + rayD * t;
							float v_t = dot(verticalPlane, planePos - bottomLeft);
							float h_t = dot(horizontalPlane, planePos - topLeft);
							if (v_t >= 1 || v_t < 0 || h_t >= 1 || h_t < 0) screenPos = float2(-1, -1);
							else screenPos = float2(v_t * SCRHEIGHT, h_t * (SCRWIDTH - SCRWIDTHOFFSET));
						} else screenPos = float2(-1, -1);
					}
				}
			}

			return screenPos;
		}
		*/

		float3 ToScreenPos(float3 gridPos)
		{
			float3 bottomRight = bottomLeft + topRight - topLeft;

			// Define plane normal
			float3 normal = cross((topRight - topLeft), (bottomLeft - topLeft));

			// Calculate plane equation D value
			float D = -dot(normal, topLeft);

			// Define the ray
			float3 rayD = gridPos - camPos; // Fix here, the ray should go from camPos to gridPos
			rayD = normalize(rayD);

			// Calculate t, the parameter of the intersection point on the ray
			float t = -(dot(normal, camPos) + D) / dot(normal, rayD);

			// If t is negative, the point is behind the camera.
			//if (t < 0) return float3(-1, -1, -1);

			// Calculate the actual intersection point in 3D
			float3 intersection = camPos + rayD * t;

			// Compute the local (u, v) coordinates of intersection point
			float3 q = intersection - topLeft;
			float3 uVec = topRight - topLeft;
			float3 vVec = bottomLeft - topLeft;
			float u = dot(q, uVec); // You don't need to normalize u and v
			float v = dot(q, vVec);

			// If the point is outside the plane's bounds, reject it
			//if (u < 0 || u > dot(uVec, uVec) || v < 0 || v > dot(vVec, vVec)) return float3(-1, -1, -1);

			// Convert the normalized (u, v) coordinates to pixel coordinates
			float3 screenPos = float3(u / dot(uVec, uVec) * (SCRWIDTH - SCRWIDTHOFFSET) + SCRWIDTHOFFSET, v / dot(vVec, vVec) * SCRHEIGHT, min(1.0f, 800.0f / magnitude(gridPos - intersection))); // Normalize by dividing with the squares of the lengths of the base vectors

			return screenPos;
		}


		void Reset()
		{

			for (int i = 0; i < 6; ++i)
			{
				moveDir[i] = false;
			}

			for (int i = 0; i < 4; ++i)
			{
				rotateDir[i] = false;
			}
		}

		void ResetToInitial()
		{
			// setup a basic view frustum
			camPos = float3(0, 0, -2);
			topLeft = float3(-aspect, 1, 0);
			topRight = float3(aspect, 1, 0);
			bottomLeft = float3(-aspect, -1, 0);

			float3 translation = float3(400, 400, -800);
			camPos += translation;
			topLeft += translation;
			topRight += translation;
			bottomLeft += translation;

			// GPU
			gpuCamProp = new float4[4]{ float4(camPos, 0) , float4(topLeft, 0), float4(topRight, 0), float4(bottomLeft, 0) };
		}
		bool moveDir[6] = { false, false, false, false }; // UP, DOWN, LEFT, RIGHT, IN, OUT
		bool rotateDir[4] = { false, false, false, false }; // UP, DOWN, LEFT, RIGHT
		float speed = 1, angular_vel = PI / 5000;
	};
}