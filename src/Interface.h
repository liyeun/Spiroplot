#pragma once

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "../lib/stb_image.h"
#include "glad.h"

#include <ShellAPI.h>
#include <Windows.h>
#include <filesystem>
#include <iostream>

#include <iomanip>
#include <sstream>
#include <string>

namespace Tmpl8 {
	class Interface
	{
	public:
		bool UI_item_selected = false;

		enum Resolution2D { LOW = 800, MID = 1600, HIGH = 2000, ULTRA = 4000 }; // 800, 1600, 2000, 4000
		const Resolution2D allResolutions2D[4] = { LOW, MID, HIGH, ULTRA };
		Resolution2D currentResolution2D = Resolution2D::LOW;


		enum Template3D { CenterMidPoint, SingleAxisP2, MultiAxis, PlaneNormal};
		const Rotation::RotationMethod rotationMethods[4] = { Rotation::RotationMethod::CenterMidPoint, Rotation::RotationMethod::SingleAxisP2, Rotation::RotationMethod::MultiAxis, Rotation::RotationMethod::PlaneNormal};
		const string allTemplate3D[4] = { "Center-Midpoint", "Single-Axis-P2", "Multi-Axis", "Plane-Normal"};

		Interface() = default;
		Interface(GLFWwindow* window, Renderer* renderer) : renderer(renderer) {
			// Setup ImGui
			IMGUI_CHECKVERSION();
			ImGui::CreateContext();
			ImGuiIO& io = ImGui::GetIO();
			ImGui_ImplGlfw_InitForOpenGL(window, true);
			ImGui_ImplOpenGL3_Init("#version 150");
			ImGui::StyleColorsDark();

			ImGuiStyle& style = ImGui::GetStyle();

			// Set the padding and border size of the window and frame to 0
			//style.WindowPadding = ImVec2(0, style.WindowPadding.y);
			style.WindowBorderSize = 0;
			//style.FramePadding = ImVec2(0, style.FramePadding.y);
			style.FrameBorderSize = 0;

			style.Colors[ImGuiCol_WindowBg] = ImVec4(.5f, .5f, .5f, 1);
			// Set the transparten Button color for the ImageButton color to transparent
			style.Colors[ImGuiCol_Button] = ImVec4(0, 0, 0, 0);

			// INPUT FIELD COLORS
			style.Colors[ImGuiCol_FrameBg] = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);  // background color
			//style.Colors[ImGuiCol_Text] = ImVec4(0.0f, 0.0f, 0.0f, 1.0f);      // text color

			// Button Colot
			style.Colors[ImGuiCol_ButtonHovered] = ImVec4(0.9f, 0.9f, 0.1f, 1);

			// SEPRATOR
			style.Colors[ImGuiCol_Separator] = ImVec4(0.0f, 0.0f, 0.0f, 1.0f); // change the separator color
			style.FrameRounding = 0.0f; // change the rounding of the block within separators

			StopButton = LoadTexture("assets/stop.png");
			StartButton = LoadTexture("assets/start.png");
			LeftButton = LoadTexture("assets/left-arrow.png");
			RightButton = LoadTexture("assets/right-arrow.png");
			DeleteButton = LoadTexture("assets/delete.png");
			ClearButton = LoadTexture("assets/clear.png");
			HideButton_Open = LoadTexture("assets/eyes_open.png"); 
			HideButton_Closed = LoadTexture("assets/eyes_closed.png");
			CentreButton = LoadTexture("assets/centre.png");
			ExportButton = LoadTexture("assets/export.png");
			ButtonOutline = LoadTexture("assets/outline.png");
			HidePointButton = LoadTexture("assets/hide.png");

			SpeedButton[0] = LoadTexture("assets/speed_1x.png");
			SpeedButton[1] = LoadTexture("assets/speed_2x.png");
			SpeedButton[2] = LoadTexture("assets/speed_3x.png");

			SizeButton[0] = LoadTexture("assets/size_1x.png");
			SizeButton[1] = LoadTexture("assets/size_2x.png");
			SizeButton[2] = LoadTexture("assets/size_3x.png");
			SizeButton[3] = LoadTexture("assets/size_4x.png");
			SizeButton[4] = LoadTexture("assets/size_5x.png");

			BitButton[0] = LoadTexture("assets/bit_32.png");
			BitButton[1] = LoadTexture("assets/bit_64.png");
			BitButton[2] = LoadTexture("assets/bit_80.png");
			BitButton[3] = LoadTexture("assets/bit_128.png");
			BitButton[4] = LoadTexture("assets/bit_256.png");

			TwoDButton = LoadTexture("assets/2D.png");
			ThreeDButton = LoadTexture("assets/3D.png");

			CPUButton = LoadTexture("assets/cpu.png");
			GPUButton = LoadTexture("assets/gpu.png");
		}

		float blockHeight = 50;
		Renderer* renderer = 0;

		GLuint LoadTexture(const char* filename)
		{
			// Load the image using a library like stb_image
			int width, height, numChannels;
			unsigned char* data = stbi_load(filename, &width, &height, &numChannels, 0);

			if (!data) {
				// Handle image loading error
				return 0;
			}

			// Generate a new OpenGL texture ID
			GLuint textureID;
			glGenTextures(1, &textureID);

			// Bind the texture to the texture target (2D texture in this case)
			glBindTexture(GL_TEXTURE_2D, textureID);

			// Set texture parameters (e.g. filtering and wrapping modes)
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			// Upload the image data to the texture
			GLenum format = (numChannels == 4) ? GL_RGBA : GL_RGB;
			glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);

			// Free the image data
			stbi_image_free(data);

			return textureID;
		}

		void RenderInputRotation(Rotation* rotation, int id, PrecisionLevel precisionLevel)
		{
			ImGui::PushStyleColor(ImGuiCol_ChildBg, ImVec4(0.75f, 0.75f, 0.75f, 1.0f));
			std::string block = "Block" + std::to_string(id);
			std::string drawing = "Drawing" + std::to_string(id);
			std::string rotate = "Rotation" + std::to_string(id);

			ImGui::BeginChild(block.c_str(), ImVec2(ImGui::GetWindowWidth(), blockHeight));

				ImGui::BeginChild(drawing.c_str(), ImVec2(ImGui::GetContentRegionAvail().x * 0.6, ImGui::GetContentRegionAvail().y), false);

				ImVec2 center1 = ImVec2(ImGui::GetWindowPos().x + ImGui::GetContentRegionAvail().x * 0.3, ImGui::GetWindowPos().y + ImGui::GetContentRegionAvail().y * 0.5);
				ImVec2 center2 = ImVec2(center1.x + 100, center1.y);
				float radius = 15.0f;

				ImColor borderColor = ImColor(0.0f, 0.0f, 0.0f, 1.0f);

				float4 p1Color = RGB8_to_RGBF32(rotation->p1->color);
				float4 p2Color = RGB8_to_RGBF32(rotation->p2->color);
				ImGui::GetWindowDrawList()->AddCircle(center1, radius, borderColor, 32, 3.0f);
				ImGui::GetWindowDrawList()->AddCircleFilled(center1, radius - 1, ImColor(p1Color.x, p1Color.y, p1Color.z), 0);
				ImGui::GetWindowDrawList()->AddCircle(center2, radius, borderColor, 32, 3.0f);
				ImGui::GetWindowDrawList()->AddCircleFilled(center2, radius - 1, ImColor(p2Color.x, p2Color.y, p2Color.z), 0);
				ImGui::GetWindowDrawList()->AddLine(ImVec2(center1.x + radius, center1.y), ImVec2(center2.x - radius, center2.y), borderColor, 10.0f);

				ImGui::EndChild();

				ImGui::SameLine();

				ImGui::BeginChild(rotate.c_str(), ImVec2(0, ImGui::GetContentRegionAvail().y), ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);

				ImFont font = *ImGui::GetFont();
				font.Scale = 2;

				ImGui::PushFont(&font);

				ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.9);

				ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0, ImGui::GetStyle().FramePadding.y));
				ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.0f, 0.0f, 0.0f, 1.0f));

				if (ImGui::InputFloat("", &rotation->degrees))
				{
					rotation->PreCalcRotationAngles(precisionLevel);
				}

				if (ImGui::IsItemActive())
				{
					UI_item_selected = true;
				}

				ImGui::PopStyleColor();
				ImGui::PopStyleVar();

				ImGui::PopFont();

				ImGui::EndChild();

			ImGui::EndChild();


			ImGui::PopStyleColor(); // reset the separator color
		}

		void RenderInputPoint(Point* point, int id, PrecisionLevel precisionLevel)
		{
			ImGui::PushStyleColor(ImGuiCol_ChildBg, ImVec4(0.75f, 0.75f, 0.75f, 1.0f));
			std::string block = "Block" + std::to_string(id);
			std::string drawing = "Drawing" + std::to_string(id);
			std::string rotate = "Point" + std::to_string(id);

			ImGui::BeginChild(block.c_str(), ImVec2(ImGui::GetWindowWidth(), blockHeight));

			ImGui::BeginChild(drawing.c_str(), ImVec2(ImGui::GetContentRegionAvail().x * 0.1, ImGui::GetContentRegionAvail().y), false);

			ImVec2 center = ImVec2(ImGui::GetWindowPos().x + ImGui::GetContentRegionAvail().x * 0.5, ImGui::GetWindowPos().y + ImGui::GetContentRegionAvail().y * 0.5);
			float radius = 15.0f;

			ImColor borderColor = ImColor(0.0f, 0.0f, 0.0f, 1.0f);

			float4 p1Color = RGB8_to_RGBF32(point->color);
			ImGui::GetWindowDrawList()->AddCircle(center, radius, borderColor, 32, 3.0f);
			ImGui::GetWindowDrawList()->AddCircleFilled(center, radius - 1, ImColor(p1Color.x, p1Color.y, p1Color.z), 0);

			ImGui::EndChild();

			ImGui::SameLine();

			ImGui::BeginChild(rotate.c_str(), ImVec2(0, ImGui::GetContentRegionAvail().y), ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);

			ImFont font = *ImGui::GetFont();
			font.Scale = 2;

			ImGui::PushFont(&font);

			float totalRegionWidth = ImGui::GetContentRegionAvail().x;

			ImGui::SetNextItemWidth(totalRegionWidth * 0.25);
			ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0, ImGui::GetStyle().FramePadding.y));
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.0f, 0.0f, 0.0f, 1.0f));

			float3 pos = point->Position3D();
			if (ImGui::InputFloat("X", &pos.x, 0.0f, 0.0f, "%.0f"))
			{
				if (pos.x < 0)
					pos.x = 0;
				else if (pos.x >= renderer->scene.spiroplot3D.gridWidth)
					pos.x = renderer->scene.spiroplot3D.gridWidth - 1;

				point->SetX(pos.x);
			}
			ImGui::SameLine();
			ImGui::SetNextItemWidth(totalRegionWidth * 0.25);
			if (ImGui::InputFloat("Y", &pos.y, 0.0f, 0.0f, "%.0f"))
			{
				if (pos.y < 0)
					pos.y = 0;
				else if (pos.y >= renderer->scene.spiroplot3D.gridHeight)
					pos.y = renderer->scene.spiroplot3D.gridHeight - 1;

				point->SetY(pos.y);
			}
			ImGui::SameLine();
			ImGui::SetNextItemWidth(totalRegionWidth * 0.25);
			if (ImGui::InputFloat("Z", &pos.z, 0.0f, 0.0f, "%.0f"))
			{
				if (pos.z < 0)
					pos.z = 0;
				else if (pos.z >= renderer->scene.spiroplot3D.gridDepth)
					pos.z = renderer->scene.spiroplot3D.gridDepth - 1;

				point->SetZ(pos.z);
			}
			ImGui::PopStyleColor();
			ImGui::PopStyleVar();

			ImGui::PopFont();

			ImGui::EndChild();

			ImGui::EndChild();


			ImGui::PopStyleColor(); // reset the separator color
		}

		void Render()
		{
			// Feed Input to ImGui, start a new Frame.
			ImGui_ImplOpenGL3_NewFrame();
			ImGui_ImplGlfw_NewFrame();
			ImGui::NewFrame();
			// Begin the main menu bar
			if (ImGui::BeginMainMenuBar())
			{
				// Add menu items here using ImGui::MenuItem()
				if (ImGui::BeginMenu("File"))
				{
					// Handle the "File" menu item
					if (ImGui::MenuItem("Open Folder"))
					{
						std::string path = std::filesystem::current_path().string() + "/plots/";
						ShellExecuteA(NULL, "open", path.c_str(), NULL, NULL, 1);

					}

					if (ImGui::MenuItem("Open 3D Folder"))
					{
						std::string path = std::filesystem::current_path().string() + "/3D plots/";
						ShellExecuteA(NULL, "open", path.c_str(), NULL, NULL, 1);

					}

					ImGui::EndMenu();
				}

				if (ImGui::BeginMenu("Settings"))
				{
					if (ImGui::BeginMenu("2D Resolution"))
					{
						for (int i = 0; i < sizeof(allResolutions2D) / sizeof(allResolutions2D[0]); i++)
						{
							std::string label = std::to_string(allResolutions2D[i]) + "x" + std::to_string(allResolutions2D[i]);
							if (currentResolution2D == allResolutions2D[i])
							{
								ImGui::PushStyleColor(ImGuiCol_Text, IM_COL32(255, 0, 0, 255)); // Red color
								ImGui::MenuItem(label.c_str());
								ImGui::PopStyleColor();
							}
							else {
								if (ImGui::MenuItem(label.c_str()))
								{
									currentResolution2D = allResolutions2D[i];
									renderer->Change2DSpiroplotResolution(allResolutions2D[i]);
								}
							}
						}
						ImGui::EndMenu();
					}
					ImGui::EndMenu();
				}

				if (ImGui::BeginMenu("Templates"))
				{
					if (ImGui::BeginMenu("3D-Spiroplots"))
					{
						for (int i = 0; i < sizeof(allTemplate3D) / sizeof(allTemplate3D[0]); i++)
						{
							std::string label = allTemplate3D[i];

							if (ImGui::MenuItem(label.c_str()))
							{
								renderer->ChangeRotationMethod(rotationMethods[i]);
								renderer->Load3DSpiroplotTemplate(i);
							}
						}
						ImGui::EndMenu();
					}
					ImGui::EndMenu();
				}
				// End the main menu bar
				ImGui::EndMainMenuBar();
			}
			ImGui::SetNextWindowSize(ImVec2(287, 786 - (305 + 40)));
			ImGui::SetNextWindowPos(ImVec2(0, 19));
			ImGui::Begin("Spiroplot", nullptr, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);

			if (renderer->is3D)
			{
				if (renderer->disPlayRotation)
				{
					if (!renderer->scene.spiroplot3D.rotations.empty())
					{
						for (int i = 0; i < renderer->scene.spiroplot3D.totalRotations; i++)
						{
							RenderInputRotation(&renderer->scene.spiroplot3D.rotations[i], i, PrecisionLevel::DOUBLE);
							ImGui::Separator();
						}
					}
				}
				else
				{
					if (!renderer->scene.spiroplot3D.points.empty())
					{
						for (int i = 0; i < renderer->scene.spiroplot3D.totalPoints; i++)
						{
							RenderInputPoint(&renderer->scene.spiroplot3D.points[i], i, PrecisionLevel::DOUBLE);
							ImGui::Separator();
						}
					}
				}
			}
			else
			{
				if (!renderer->scene.spiroplot2D.rotations.empty())
				{
					for (int i = 0; i < renderer->scene.spiroplot2D.totalRotations; i++)
					{
						RenderInputRotation(&renderer->scene.spiroplot2D.rotations[i], i, renderer->precisionLevel);
						ImGui::Separator();
					}
				}
			}

			ImGui::End();

			ImGui::SetNextWindowSize(ImVec2(287, 300));
			ImGui::SetNextWindowPos(ImVec2(0, 500));
			ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.75f, 0.75f, 0.75f, 1.0f));
			ImGui::Begin("Spiroplot2", nullptr, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);

			ImGui::BeginChild("PointsColor", ImVec2(0, 50), true, ImGuiWindowFlags_HorizontalScrollbar | ImGuiWindowFlags_AlwaysHorizontalScrollbar);

			if (renderer->is3D) {
				for (int i = 0; i < renderer->scene.spiroplot3D.totalPoints; i++) {

					int idToFind = renderer->scene.spiroplot3D.pointsOrder[i];

					Point* currentPoint = &(*std::find_if(renderer->scene.spiroplot3D.points.begin(), renderer->scene.spiroplot3D.points.end(),
						[idToFind](const Point& point) { return point.GetID() == idToFind; }));

					//Point* currentPoint = &renderer->scene.spiroplot2D.points[renderer->scene.spiroplot2D.pointsOrder[i]];
					// Extract the individual color components
					ImVec4 color;

					color.x = static_cast<float>((currentPoint->color & 0xFF0000) >> 16) / 255.0f;
					color.y = static_cast<float>((currentPoint->color & 0x00FF00) >> 8) / 255.0f;
					color.z = static_cast<float>(currentPoint->color & 0x0000FF) / 255.0f;
					color.w = 1.0f;                     // Alpha component

					std::string colorWheelIdx = ("color wheel:" + std::to_string(i));

					if (ImGui::ColorButton(("color:" + std::to_string(i + 1)).c_str(), color) || currentPoint->menuState == Point::MenuState::OPEN) {

						if (!ImGui::IsPopupOpen(colorWheelIdx.c_str()))
							ImGui::OpenPopup(colorWheelIdx.c_str());

						currentPoint->ChangeMenuState(Point::MenuState::ACTIVE);


					}

					if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None)) {
						ImGui::SetDragDropPayload("COLORBLOCK", &i, sizeof(int));
						// Display a preview (or "ghost") of the source item while it's being dragged
						ImGui::ColorButton(("color:" + std::to_string(i + 1)).c_str(), color);
						ImGui::EndDragDropSource();
					}

					if (ImGui::BeginDragDropTarget()) {
						if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("COLORBLOCK")) {
							int source_index = *(const int*)payload->Data;
							// Swap the colors
							std::swap(renderer->scene.spiroplot3D.pointsOrder[i], renderer->scene.spiroplot3D.pointsOrder[source_index]);
						}
						ImGui::EndDragDropTarget();
					}

					if (renderer->scene.spiroplot3D.HiddenPoint(currentPoint->Id()))
					{
						ImGui::SameLine(0, 0);
						ImGui::SetCursorPosX(ImGui::GetCursorPosX() - 18);
						ImGui::SetCursorPosY(ImGui::GetCursorPosY() + 1);
						ImGui::Image((void*)(intptr_t)HidePointButton, ImVec2(17, 17));
					}

					if (ImGui::IsItemClicked(ImGuiMouseButton_Right))
					{
						renderer->scene.spiroplot3D.HidePoint(currentPoint->Id());
					}

					ImGui::SetNextWindowPos(ImVec2(0, 300));
					ImGui::SetNextWindowSize(ImVec2(288, 195));
					if (ImGui::BeginPopup(colorWheelIdx.c_str(), ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse)) {

						if (ImGui::ColorPicker4("color", (float*)&color)) {
							// color has been changed
							currentPoint->ChangeColor(color.x, color.y, color.z);
						}


						ImGui::EndPopup();
					}

					if (currentPoint->menuState == Point::MenuState::ACTIVE)
					{
						if (!ImGui::IsPopupOpen(colorWheelIdx.c_str()))
							currentPoint->ChangeMenuState(Point::MenuState::CLOSED);
					}

					ImGui::SameLine();
				}
			}
			else {
				for (int i = 0; i < renderer->scene.spiroplot2D.totalPoints; i++) {

					int idToFind = renderer->scene.spiroplot2D.pointsOrder[i];

					Point* currentPoint = &(*std::find_if(renderer->scene.spiroplot2D.points.begin(), renderer->scene.spiroplot2D.points.end(),
						[idToFind](const Point& point) { return point.GetID() == idToFind; }));

					//Point* currentPoint = &renderer->scene.spiroplot2D.points[renderer->scene.spiroplot2D.pointsOrder[i]];
					// Extract the individual color components
					ImVec4 color;

					color.x = static_cast<float>((currentPoint->color & 0xFF0000) >> 16) / 255.0f;
					color.y = static_cast<float>((currentPoint->color & 0x00FF00) >> 8) / 255.0f;
					color.z = static_cast<float>(currentPoint->color & 0x0000FF) / 255.0f;
					color.w = 1.0f;                     // Alpha component

					std::string colorWheelIdx = ("color wheel:" + std::to_string(i));

					if (ImGui::ColorButton(("color:" + std::to_string(i + 1)).c_str(), color) || currentPoint->menuState == Point::MenuState::OPEN) {

						if (!ImGui::IsPopupOpen(colorWheelIdx.c_str()))
							ImGui::OpenPopup(colorWheelIdx.c_str());

						currentPoint->ChangeMenuState(Point::MenuState::ACTIVE);


					}

					if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None)) {
						ImGui::SetDragDropPayload("COLORBLOCK", &i, sizeof(int));
						// Display a preview (or "ghost") of the source item while it's being dragged
						ImGui::ColorButton(("color:" + std::to_string(i + 1)).c_str(), color);
						ImGui::EndDragDropSource();
					}

					if (ImGui::BeginDragDropTarget()) {
						if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("COLORBLOCK")) {
							int source_index = *(const int*)payload->Data;
							// Swap the colors
							std::swap(renderer->scene.spiroplot2D.pointsOrder[i], renderer->scene.spiroplot2D.pointsOrder[source_index]);
						}
						ImGui::EndDragDropTarget();
					}

					if (renderer->scene.spiroplot2D.HiddenPoint(currentPoint->Id()))
					{
						ImGui::SameLine(0, 0);
						ImGui::SetCursorPosX(ImGui::GetCursorPosX() - 18);
						ImGui::SetCursorPosY(ImGui::GetCursorPosY() + 1);
						ImGui::Image((void*)(intptr_t)HidePointButton, ImVec2(17, 17));
					}

					if (ImGui::IsItemClicked(ImGuiMouseButton_Right))
					{
						renderer->scene.spiroplot2D.HidePoint(currentPoint->Id());
					}

					ImGui::SetNextWindowPos(ImVec2(0, 300));
					ImGui::SetNextWindowSize(ImVec2(288, 195));
					if (ImGui::BeginPopup(colorWheelIdx.c_str(), ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse)) {

						if (ImGui::ColorPicker4("color", (float*)&color)) {
							// color has been changed
							currentPoint->ChangeColor(color.x, color.y, color.z);
						}


						ImGui::EndPopup();
					}

					if (currentPoint->menuState == Point::MenuState::ACTIVE)
					{
						if (!ImGui::IsPopupOpen(colorWheelIdx.c_str()))
							currentPoint->ChangeMenuState(Point::MenuState::CLOSED);
					}

					ImGui::SameLine();
				}
			}
			ImGui::EndChild();


			/*
			// Sliders Begin
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.0f, 0.0f, 0.0f, 1.0f));
			ImGui::SliderInt("Speed", &renderer->speed, 1, 5, "%dx");
			if (ImGui::SliderInt("Size", &renderer->scene.pointSize, 1, 5, "%dx"))
			{
				renderer->scene.UpdatePointSize();
			}

			ImGui::PopStyleColor();
			// Sliders End
			*/

			static const ImVec2 IMAGE_SIZE = ImVec2(50, 50);

			if (ImGui::ImageButton((void*)(intptr_t)SpeedButton[renderer->speed], IMAGE_SIZE))
			{
				if (renderer->speed == 2)
					renderer->speed = (Renderer::SpeedState) 0;
				else
					renderer->speed = (Renderer::SpeedState) (renderer->speed + 1);
			}

			// Display the options as a popup
			if (ImGui::BeginPopup("Speed", ImGuiWindowFlags_NoMove))
			{
				for (int i = 0; i < 3; i++)
				{
					// Skip the current selection
					if (i == renderer->speed)
					{
						continue;
					}

					// Display the option as an image and set the current selection if clicked
					if (ImGui::ImageButton((void*)(intptr_t)SpeedButton[i], IMAGE_SIZE))
					{
						renderer->speed = (Renderer::SpeedState) i;
						ImGui::CloseCurrentPopup();
					}
				}

				ImGui::EndPopup();
			}

			ImGui::SameLine();

			// Display the current selection as an image
			if (ImGui::ImageButton((void*)(intptr_t)SizeButton[renderer->scene.pointSize - 1], IMAGE_SIZE))
			{
				ImVec2 pos = ImGui::GetCursorScreenPos();
				pos.y -= ImGui::GetWindowSize().y + 10;
				pos.x += 60;

				ImGui::SetNextWindowPos(pos);
				ImGui::OpenPopup("Size");
			}

			// Display the options as a popup
			if (ImGui::BeginPopup("Size", ImGuiWindowFlags_NoMove))
			{
				for (int i = 4; i >= 0; i--)
				{
					// Skip the current selection
					if (i == renderer->scene.pointSize - 1)
					{
						continue;
					}

					// Display the option as an image and set the current selection if clicked
					if (ImGui::ImageButton((void*)(intptr_t)SizeButton[i], IMAGE_SIZE))
					{
						renderer->scene.pointSize = i + 1;
						renderer->scene.UpdatePointSize();
						ImGui::CloseCurrentPopup();
					}
				}

				ImGui::EndPopup();
			}

			ImGui::SameLine();

			if (renderer->is3D) {
				if (renderer->useGPU)
				{
					if (ImGui::ImageButton((void*)(intptr_t)GPUButton, IMAGE_SIZE))
						renderer->ChangeDevice();
				}
				else
				{
					if (ImGui::ImageButton((void*)(intptr_t)CPUButton, IMAGE_SIZE))
						renderer->ChangeDevice();
				}

			}
			else {
				// Display the current selection as an image
				if (ImGui::ImageButton((void*)(intptr_t)BitButton[renderer->precisionLevel], IMAGE_SIZE))
				{
					ImVec2 pos = ImGui::GetCursorScreenPos();
					pos.y -= ImGui::GetWindowSize().y + 10;
					pos.x += 60 * 2;

					ImGui::SetNextWindowPos(pos);
					ImGui::OpenPopup("Bit");
				}

				// Display the options as a popup
				if (ImGui::BeginPopup("Bit", ImGuiWindowFlags_NoMove))
				{
					for (int i = 4; i >= 0; i--)
					{
						// Skip the current selection
						if (i == renderer->precisionLevel)
						{
							continue;
						}

						// Display the option as an image and set the current selection if clicked
						if (ImGui::ImageButton((void*)(intptr_t)BitButton[i], IMAGE_SIZE))
						{
							renderer->ChangePrecision((PrecisionLevel)i);
							ImGui::CloseCurrentPopup();
						}
					}

					ImGui::EndPopup();
				}
			}

			ImGui::SameLine();

			if (renderer->is3D)
			{
				if (ImGui::ImageButton((void*)(intptr_t)ThreeDButton, IMAGE_SIZE))
					renderer->ChangeDimension();
			}
			else
			{
				if (ImGui::ImageButton((void*)(intptr_t)TwoDButton, IMAGE_SIZE))
					renderer->ChangeDimension();
			}


			ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(0, 0, 0, 0));
			ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(0, 0, 0, 0));
			ImGui::SetCursorPos(ImVec2(4, 115));

			if (ImGui::ImageButton((void*)(intptr_t)LeftButton, ImVec2(60, 66)))
			{
			}

			ImGui::SetCursorPos(ImVec2(64, 115));

			if (renderer->currentGameState == Renderer::Play)
			{
				//ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.8f, 0.2f, 0.2f, 1.0f)); // Red color
				if (ImGui::ImageButton((void*)(intptr_t)StopButton, ImVec2(150, 66)))
				{
					renderer->ChangeGameState();
				}
				//ImGui::PopStyleColor();
			}
			else
			{
				//ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.0f, 1.0f, 0.0f, 1.0f)); // Green color
				if (ImGui::ImageButton((void*)(intptr_t)StartButton, ImVec2(150, 66)))
				{
					renderer->ChangeGameState();
				}
				//ImGui::PopStyleColor();
			}

			ImGui::SetCursorPos(ImVec2(214, 115));

			if (ImGui::ImageButton((void*)(intptr_t)RightButton, ImVec2(60, 66)))
			{
				renderer->ChangeAnimationState();
			}

			ImGui::PopStyleColor(2);

			ImDrawList* draw_list = ImGui::GetWindowDrawList();

			ImVec2 rect_min = ImVec2(0, 750);
			ImVec2 rect_max = ImVec2(287, 695);
			// Draw the rectangle fill
			draw_list->AddRectFilled(rect_min, rect_max, IM_COL32(50, 50, 50, 255));

			// Draw the rectangle border
			draw_list->AddRect(rect_min, rect_max, IM_COL32(50, 50, 50, 255), 0.0f, ImDrawCornerFlags_All, 3.0f);

			ImGui::SetCursorPos(ImVec2(5, 200));

			// Set the background color to be red
			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(1, 0, 0, 1));

			if (ImGui::ImageButton((void*)(intptr_t)DeleteButton, ImVec2(40, 40)))
			{
				renderer->Reset();
			}

			ImGui::SameLine();

			if (ImGui::ImageButton((void*)(intptr_t)ClearButton, ImVec2(40, 40)))
			{
				renderer->Clear();
			}

			// Restore the default background color
			ImGui::PopStyleColor();

			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.43f, 0.85f, 0.55f, 1));
			ImGui::SameLine();

			if (renderer->hideInput)
			{
				if (ImGui::ImageButton((void*)(intptr_t)HideButton_Closed, ImVec2(40, 40)))
				{
					renderer->HideInput();
				}
			}
			else
			{
				if (ImGui::ImageButton((void*)(intptr_t)HideButton_Open, ImVec2(40, 40)))
				{
					renderer->HideInput();
				}
			}

			ImGui::SameLine();

			if (ImGui::ImageButton((void*)(intptr_t)CentreButton, ImVec2(40, 40)))
			{
				renderer->CenterPoints();
			}

			ImGui::SameLine();

			static bool show_popup = false;
			static float popup_timer = 0.0f;

			if (ImGui::ImageButton((void*)(intptr_t)ExportButton, ImVec2(40, 40)))
			{
				auto now = chrono::system_clock::now();
				time_t current_time = chrono::system_clock::to_time_t(now);

				stringstream buffer;
				buffer << put_time(localtime(&current_time), "%d-%m-%Y %H.%M.%S");
				filename = buffer.str();

				if(renderer->is3D)
					renderer->Export3D(filename);
				else
					renderer->Export(filename);

				show_popup = true;
				popup_timer = 0.0f;
			}

			ImGui::PopStyleColor();

			ImGui::SetCursorPos(ImVec2(10, 260));
			ImFont font = *ImGui::GetFont();
			font.Scale = 2;
			ImGui::PushFont(&font);
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.0f, 0.0f, 0.0f, 1.0f));
			if(renderer->is3D)
				ImGui::Text("Iteration: %i", renderer->scene.spiroplot3D.currentIteration); // Render text with adjusted font size
			else
				ImGui::Text("Iteration: %i", renderer->scene.spiroplot2D.currentIteration); // Render text with adjusted font size
			ImGui::PopStyleColor();
			ImGui::PopFont();

			ImGui::End();

			ImGui::PopStyleColor();


			if (show_popup)
			{
				// Set up the popup window
				ImGui::SetNextWindowSize(ImVec2(200, 50));
				ImGui::SetNextWindowPos(ImVec2(862, 726));
				//ImGui::SetNextWindowPos(ImVec2(1000, 700);
				float transparency = 0.9f;
				if (popup_timer > 2.2f)
					transparency = 1 - popup_timer / popup_duration;

				ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.2f, 0.2f, 0.2f, transparency)); // Set background color to dark gray
				ImGui::Begin("Save", nullptr, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoScrollbar);

				std::string path = std::filesystem::current_path().string() + "/plots/";
				ImGui::Text(("Image Saved As:\n" + ("'" + filename) + ".png'").c_str());

				if (ImGui::IsItemClicked()) // Check if the button is clicked
				{
					ShellExecuteA(NULL, "open", (path + filename + ".png").c_str() , NULL, NULL, 1);
					show_popup = false;
					ImGui::CloseCurrentPopup();
				}

				ImGui::End();
				ImGui::PopStyleColor(); // Reset background color

				// Increase the timer
				popup_timer += ImGui::GetIO().DeltaTime;

				// If the timer has exceeded the desired duration, hide the popup
				if (popup_timer > popup_duration) // Change 3.0f to the desired duration
				{
					show_popup = false;
				}
			}

			ImGui::SetNextWindowSize(ImVec2(287, 40));
			ImGui::SetNextWindowPos(ImVec2(0, 460));
			ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.2, 0.2, 0.2, 0.95f));
			ImGui::PushStyleColor(ImGuiCol_FrameBg, ImVec4(0.3f, 0.3f, 0.3f, 1.0f)); 

			ImGui::Begin("Spiroplot3", nullptr, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);

			// Draw Method - Drop Down List
			static const char* draw_mode_text[] = { "LIFO", "FIFO", "ORDER"};

			// Rotate Method - Drop Down List
			static const char* rotation_mode_text[] = { "2D", "Center Midpoint", "Single Axis P1",  "Single Axis P2 ",  "Multi Axis",  "Plane Normal", "Plane Circle", "Plane Normal Circle" };

			// Create a combo box and store the result in currentOption
			if (renderer->is3D)
			{
				if (ImGui::Combo("Rotation", (int*)&renderer->currentRotationMethod, rotation_mode_text, IM_ARRAYSIZE(rotation_mode_text)))
				{
					renderer->ChangeRotationMethod(renderer->currentRotationMethod);
				}

				ImGui::SameLine();
				ImGui::Checkbox("", &renderer->disPlayRotation);
			}
			else
			{
				if (ImGui::Combo("Draw Mode", (int*)&renderer->plotState, draw_mode_text, IM_ARRAYSIZE(draw_mode_text)))
				{
					renderer->ChangePlotState(renderer->plotState);
				}
			}


			ImGui::End();
			ImGui::PopStyleColor(2);

			/*
			ImVec4 static color = ImVec4(1.0f, 1.0f, 1.0f, 1.0f); // initial color


			ImGui::SetNextWindowPos(ImVec2(500, 500));
			if (ImGui::ColorButton("color wheel button", color)) {
				ImGui::OpenPopup("color picker");
			}

			ImGui::SameLine();
			if (ImGui::ColorButton("color wheel button 1", color)) {
				ImGui::OpenPopup("color picker");
			}
			ImGui::SetNextWindowPos(ImVec2(0, 300));
			ImGui::SetNextWindowSize(ImVec2(288, 195));
			if (ImGui::BeginPopup("color picker")) {
	
				if (ImGui::ColorPicker4("color wheel", (float*)&color)) {
					// color has been changed
				}
				ImGui::EndPopup();
			}
			
			*/
			// Render ImGui to screen
			ImGui::Render();
			ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
		}

		float popup_duration = 4.0f;
		string filename;

		GLuint StopButton, StartButton;
		GLuint LeftButton, RightButton;
		GLuint DeleteButton, ClearButton, HideButton_Open, HideButton_Closed, CentreButton, ExportButton;
		GLuint ButtonOutline;
		GLuint HidePointButton;

		GLuint* SpeedButton = new GLuint[3];
		GLuint* SizeButton = new GLuint[5];
		GLuint* BitButton = new GLuint[5];
		GLuint TwoDButton, ThreeDButton;
		GLuint CPUButton, GPUButton;
	};

}