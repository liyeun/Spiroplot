__kernel void Finalize(__global uint* deviceBuffer, __global uint* pixelColors,
int scrWidth, int scrWidthOffset)						         	 			 // Static values
{   
    int threadId = get_global_id(0);
    int xWidth = scrWidth - scrWidthOffset;
    
    int x = threadId % xWidth;
    int y = threadId / xWidth;

    deviceBuffer[y * scrWidth + x + scrWidthOffset] = pixelColors[threadId] & 0xFFFFFF;
}