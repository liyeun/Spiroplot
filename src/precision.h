#include <mpfr.h>
#include <variant>

#include <sstream>
#include <limits>
#include <iomanip>

namespace Tmpl8 {
    enum PrecisionLevel { FLOAT, DOUBLE, MPFR_80, MPFR_128, MPFR_256, EnumSize};
    enum PlotState { LIFO, FIFO, ORDER };
    class MpfrWrapper {
    public:
        mpfr_t value;
        MpfrWrapper(mpfr_prec_t precision) {
            mpfr_init2(value, precision);
        }

        MpfrWrapper(const MpfrWrapper& other) {
            mpfr_init2(value, mpfr_get_prec(other.value));
            mpfr_set(value, other.value, MPFR_RNDN);
        }

        MpfrWrapper& operator=(const MpfrWrapper& other) {
            if (this != &other) {
                mpfr_set_prec(value, mpfr_get_prec(other.value));
                mpfr_set(value, other.value, MPFR_RNDN);
            }
            return *this;
        }

        ~MpfrWrapper() {
            mpfr_clear(value);
        }
    };

    using Number = std::variant<float, double, MpfrWrapper>;

    static class Calculator {
    public:
        static Number add(Number a, Number b) {
            if (std::holds_alternative<float>(a) && std::holds_alternative<float>(b)) {
                return std::get<float>(a) + std::get<float>(b);
            }
            if (std::holds_alternative<double>(a) && std::holds_alternative<double>(b)) {
                return std::get<double>(a) + std::get<double>(b);
            }
            if (std::holds_alternative<MpfrWrapper>(a) && std::holds_alternative<MpfrWrapper>(b)) {
                MpfrWrapper result(mpfr_get_prec(std::get<MpfrWrapper>(a).value));
                mpfr_add(result.value, std::get<MpfrWrapper>(a).value, std::get<MpfrWrapper>(b).value, MPFR_RNDN);
                return result;
            }
            throw std::invalid_argument("Incompatible types");
        }

        static Number sub(Number a, Number b) {
            if (std::holds_alternative<float>(a) && std::holds_alternative<float>(b)) {
                return std::get<float>(a) - std::get<float>(b);
            }
            if (std::holds_alternative<double>(a) && std::holds_alternative<double>(b)) {
                return std::get<double>(a) - std::get<double>(b);
            }
            if (std::holds_alternative<MpfrWrapper>(a) && std::holds_alternative<MpfrWrapper>(b)) {
                MpfrWrapper result(mpfr_get_prec(std::get<MpfrWrapper>(a).value));
                mpfr_sub(result.value, std::get<MpfrWrapper>(a).value, std::get<MpfrWrapper>(b).value, MPFR_RNDN);
                return result;
            }
            throw std::invalid_argument("Incompatible types");
        }

        static Number mul(Number a, Number b) {
            if (std::holds_alternative<float>(a) && std::holds_alternative<float>(b)) {
                return std::get<float>(a) * std::get<float>(b);
            }
            if (std::holds_alternative<double>(a) && std::holds_alternative<double>(b)) {
                return std::get<double>(a) * std::get<double>(b);
            }
            if (std::holds_alternative<MpfrWrapper>(a) && std::holds_alternative<MpfrWrapper>(b)) {
                MpfrWrapper result(mpfr_get_prec(std::get<MpfrWrapper>(a).value));
                mpfr_mul(result.value, std::get<MpfrWrapper>(a).value, std::get<MpfrWrapper>(b).value, MPFR_RNDN);
                return result;
            }
            throw std::invalid_argument("Incompatible types");
        }

        static MpfrWrapper mulByPi(MpfrWrapper a) {
            MpfrWrapper result(mpfr_get_prec(a.value));
            mpfr_const_pi(result.value, MPFR_RNDN);

            mpfr_mul(result.value, a.value, result.value, MPFR_RNDN);
            return result;
        }

        static Number div(Number a, Number b) {
            if (std::holds_alternative<float>(a) && std::holds_alternative<float>(b)) {
                return std::get<float>(a) / std::get<float>(b);
            }
            if (std::holds_alternative<double>(a) && std::holds_alternative<double>(b)) {
                return std::get<double>(a) / std::get<double>(b);
            }
            if (std::holds_alternative<MpfrWrapper>(a) && std::holds_alternative<MpfrWrapper>(b)) {
                MpfrWrapper result(mpfr_get_prec(std::get<MpfrWrapper>(a).value));
                mpfr_div(result.value, std::get<MpfrWrapper>(a).value, std::get<MpfrWrapper>(b).value, MPFR_RNDN);
                return result;
            }
            throw std::invalid_argument("Incompatible types");
        }

        static Number divideByTwo(Number a) {
            if (std::holds_alternative<float>(a)) {
                return std::get<float>(a) / 2.0f;
            }
            if (std::holds_alternative<double>(a)) {
                return std::get<double>(a) / 2.0;
            }
            if (std::holds_alternative<MpfrWrapper>(a)) {
                MpfrWrapper result(mpfr_get_prec(std::get<MpfrWrapper>(a).value));
                mpfr_div_ui(result.value, std::get<MpfrWrapper>(a).value, 2, MPFR_RNDN);
                return result;
            }
            throw std::invalid_argument("Incompatible types");
        }

        static MpfrWrapper divideBy180(MpfrWrapper a) {
            MpfrWrapper result(mpfr_get_prec(a.value));
            mpfr_div_ui(result.value, a.value, 180, MPFR_RNDN);
            return result;
        }

        static Number sin(Number a) {
            if (std::holds_alternative<float>(a)) {
                return std::sin(std::get<float>(a));
            }
            if (std::holds_alternative<double>(a)) {
                return std::sin(std::get<double>(a));
            }
            if (std::holds_alternative<MpfrWrapper>(a)) {
                MpfrWrapper result(mpfr_get_prec(std::get<MpfrWrapper>(a).value));
                mpfr_sin(result.value, std::get<MpfrWrapper>(a).value, MPFR_RNDN);
                return result;
            }
            throw std::invalid_argument("Incompatible types");
        }

        static Number cos(Number a) {
            if (std::holds_alternative<float>(a)) {
                return std::cos(std::get<float>(a));
            }
            if (std::holds_alternative<double>(a)) {
                return std::cos(std::get<double>(a));
            }
            if (std::holds_alternative<MpfrWrapper>(a)) {
                MpfrWrapper result(mpfr_get_prec(std::get<MpfrWrapper>(a).value));
                mpfr_cos(result.value, std::get<MpfrWrapper>(a).value, MPFR_RNDN);
                return result;
            }
            throw std::invalid_argument("Incompatible types");
        }

        static Number convertToRadians(float degrees, PrecisionLevel precisionLevel) {
            if (precisionLevel == PrecisionLevel::FLOAT) {
                return degrees * std::acos(-1.0f) / 180.0f;
            }
            else if (precisionLevel == PrecisionLevel::DOUBLE) {
                return degrees * std::acos(-1) / 180.0;
            }
            else {
                mpfr_prec_t mpfr_precision;

                if (precisionLevel == PrecisionLevel::MPFR_80)
                    mpfr_precision = 80;
                else if (precisionLevel ==PrecisionLevel::MPFR_128)
                    mpfr_precision = 128;
                else if (precisionLevel == PrecisionLevel::MPFR_256)
                    mpfr_precision = 256;

                MpfrWrapper mpfrWrapper(mpfr_precision);
                mpfr_set_flt(mpfrWrapper.value, degrees, MPFR_RNDN);

                return Calculator::divideBy180(Calculator::mulByPi(mpfrWrapper));
            }
            throw std::invalid_argument("Incompatible types");
        }

        static void changeToFloat(Number& a) {
            if (std::holds_alternative<double>(a)){
                a = static_cast<float>(std::get<double>(a));
            }
            else if (std::holds_alternative<MpfrWrapper>(a)) {
                a = mpfr_get_flt(std::get<MpfrWrapper>(a).value, MPFR_RNDN);
            }
        }

        static float returnFloat(Number a) {
            if (std::holds_alternative<float>(a))
                return std::get<float>(a);
            else if (std::holds_alternative<double>(a))
                return static_cast<float>(std::get<double>(a));
            else if (std::holds_alternative<MpfrWrapper>(a))
                return mpfr_get_flt(std::get<MpfrWrapper>(a).value, MPFR_RNDN);
        }

        static void changeToDouble(Number& a) {
            if (std::holds_alternative<float>(a)) {
                double new_a = static_cast<double>(std::get<float>(a));

                int decimal_places_float = std::log10(1.0 / std::numeric_limits<float>::epsilon());
                // Round the double value to the same number of decimal places as the float
                a = std::round(new_a * std::pow(10, decimal_places_float)) / std::pow(10, decimal_places_float);

            }
            else if (std::holds_alternative<MpfrWrapper>(a)) {
                a = mpfr_get_d(std::get<MpfrWrapper>(a).value, MPFR_RNDN);
            }
        }

        static void changeToMpfrPrecision(Number& a, mpfr_prec_t new_precision) {
            MpfrWrapper new_value(new_precision);

            if(std::holds_alternative<float>(a)) {

                std::stringstream string_value;
                string_value << std::setprecision(std::numeric_limits<float>::digits10 + 1) << std::get<float>(a);

                // Convert the string to the mpfr_t value
                mpfr_set_str(new_value.value, string_value.str().c_str(), 10, MPFR_RNDU);

            }
            else if (std::holds_alternative<double>(a))
            {
                std::stringstream string_value;
                string_value << std::setprecision(std::numeric_limits<double>::digits10 + 1) << std::get<double>(a);

                // Convert the string to the mpfr_t value
                mpfr_set_str(new_value.value, string_value.str().c_str(), 10, MPFR_RNDU);

            }
            else if (std::holds_alternative<MpfrWrapper>(a)) {
                // TODO CORRECT ROUNDINGS
                if(new_precision <= mpfr_get_prec(std::get<MpfrWrapper>(a).value))
                    mpfr_set(new_value.value, std::get<MpfrWrapper>(a).value, MPFR_RNDN);
                else {
                    mpfr_prec_t precision = mpfr_get_prec(std::get<MpfrWrapper>(a).value);

                    // Calculate the number of decimal places based on precision
                    int decimal_places = std::ceil(precision * std::log10(2));

                    char format[50]; // Buffer to store the format string
                    sprintf(format, "%%.%dRf", decimal_places); // Create the format string

                    char buffer[1024]; // Buffer to store the result
                    mpfr_sprintf(buffer, format, std::get<MpfrWrapper>(a).value); // Store the result in buffer

                    mpfr_set_str(new_value.value, buffer, 10, MPFR_RNDU);
                }
            }
            a = new_value;
        }

        static string toString(Number a)
        {
            if (std::holds_alternative<float>(a)) {
                std::stringstream ss;
                ss.precision(std::numeric_limits<float>::max_digits10 + 1);
                ss << std::get<float>(a);
                return ss.str();
            }
            else if (std::holds_alternative<double>(a))
            {
                std::stringstream ss;
                ss.precision(std::numeric_limits<double>::max_digits10 + 1);
                ss << std::get<double>(a);
                return ss.str();
            }
            else if (std::holds_alternative<MpfrWrapper>(a)) {
                // Convert mpfr value to a string 
                char* str;
                mpfr_exp_t exp;
                char* mpfr_char_str = mpfr_get_str(nullptr, &exp, 10, 0, std::get<MpfrWrapper>(a).value, MPFR_RNDN);
                std::string mpfr_str(mpfr_char_str);

                if (mpfr_str.front() != '-')
                {
                    if (exp > 0)
                        mpfr_str.insert(exp, 1, '.');
                    else
                        mpfr_str.insert(0, "0." + std::string(abs(exp), '0'));
                }
                else
                {
                    if (exp > 0)
                        mpfr_str.insert(exp + 1, 1, '.');
                    else
                        mpfr_str.insert(1, "0." + std::string(abs(exp), '0'));
                }
                return mpfr_str;

            }
        }

        static void print(Number a)
        {
            if (std::holds_alternative<float>(a)) {
                std::cout << "Float: " << std::setprecision(std::numeric_limits<float>::max_digits10 + 1) << std::get<float>(a) << std::endl;
            }
            else if (std::holds_alternative<double>(a))
            {
                std::cout << "Double: " << std::setprecision(std::numeric_limits<double>::max_digits10 + 1) << std::get<double>(a) << std::endl;
            }
            else if (std::holds_alternative<MpfrWrapper>(a)) {
                mpfr_prec_t precision = mpfr_get_prec(std::get<MpfrWrapper>(a).value);

                // Calculate the number of decimal places based on precision
                int decimal_places = std::ceil(precision * std::log10(2));

                // Create a format string
                std::string format_string = "MPFR" + std::to_string(precision) + ": %." + std::to_string(decimal_places) + "Rf\n";

                // Print the mpfr_t value with the whole decimal digits
                mpfr_printf(format_string.c_str(), std::get<MpfrWrapper>(a).value);
            }
        }

        // Implement other operations similarly...
    };
}
