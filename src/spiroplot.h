#pragma once
#include <precision.h>
#include <camera.h>
#include <filesystem>
namespace Tmpl8 {
#define GPU
	static uint globalPointIdx = 0;
	static uint backgroundColor = 0xFFFFFFFF;
	static std::vector<int> basicColors = {
	0xFF0000, // Red
	0x00FF00, // Green
	0x0000FF, // Blue
	0xFFFF00, // Yellow
	0xFF00FF, // Magenta
	0x00FFFF, // Cyan
	0xFFA500, // Orange
	0x800080, // Purple
	0x008000, // Dark Green
	0x800000, // Maroon
	0xFFFFF0, // Ivory
	0x00CED1, // Dark Turquoise
	0x228B22, // Forest Green
	0xFF6347, // Tomato
	0x483D8B, // Dark Slate Blue
	0x9400D3, // Dark Violet
	0x8B0000, // Dark Red
	0x2E8B57, // Sea Green
	0xB22222, // Fire Brick
	0x808000, // Olive
	0xFF69B4, // Hot Pink
	0x9932CC, // Dark Orchid
	0x556B2F, // Dark Olive Green
	0x8B008B, // Dark Magenta
	0x008080, // Teal
	0xA52A2A, // Brown
	0xFF8C00, // Dark Orange
	0x9932CD, // Medium Orchid
	0x2F4F4F, // Dark Slate Gray
	0xD2691E, // Chocolate
	// Add more colors here if needed
	};


	class Point {
		public:
			enum class PointState { INACTIVE, HOVERED, SELECTED, MOVE };
			enum class MenuState { OPEN, ACTIVE , CLOSED };

			Point() = default;
			Point(int x, int y, PrecisionLevel precisionLevel, int gridWidth, int gridHeight, uint color = 0, bool dummy = false)
			{
				if (precisionLevel == PrecisionLevel::FLOAT)
				{
					this->x = (float)x;
					this->y = (float)y;
				}
				else if (precisionLevel == PrecisionLevel::DOUBLE)
				{
					this->x = (double)x;
					this->y = (double)y;
				}
				else
				{
					mpfr_prec_t mpfr_precision;

					if (precisionLevel == PrecisionLevel::MPFR_80)
						mpfr_precision = 80;
					else if (precisionLevel == PrecisionLevel::MPFR_128)
						mpfr_precision = 128;
					else if (precisionLevel == PrecisionLevel::MPFR_256)
						mpfr_precision = 256;

					MpfrWrapper mpfrWrapper_x(mpfr_precision);
					MpfrWrapper mpfrWrapper_y(mpfr_precision);
					mpfr_set_si(mpfrWrapper_x.value, x, MPFR_RNDN);
					mpfr_set_si(mpfrWrapper_y.value, y, MPFR_RNDN);

					this->x = mpfrWrapper_x;
					this->y = mpfrWrapper_y;
				}

				if (!dummy)
				{
					uint id = globalPointIdx++;

					this->color = color | (id << 24);

					if (globalPointIdx == 256)
						globalPointIdx = 0;

					grid = new uint[gridWidth * gridHeight];
					memset(grid, backgroundColor, gridWidth * gridHeight * sizeof(uint));
				}
			}

			Point(int x, int y, int z, PrecisionLevel precisionLevel, uint color = 0, bool dummy = false)
			{
				is3D = true;
				if (precisionLevel == PrecisionLevel::FLOAT)
				{
					this->x = (float)x;
					this->y = (float)y;
					this->z = (float)z;
				}
				else if (precisionLevel == PrecisionLevel::DOUBLE)
				{
					this->x = (double)x;
					this->y = (double)y;
					this->z = (double)z;
				}
				else
				{
					mpfr_prec_t mpfr_precision;

					if (precisionLevel == PrecisionLevel::MPFR_80)
						mpfr_precision = 80;
					else if (precisionLevel == PrecisionLevel::MPFR_128)
						mpfr_precision = 128;
					else if (precisionLevel == PrecisionLevel::MPFR_256)
						mpfr_precision = 256;

					MpfrWrapper mpfrWrapper_x(mpfr_precision);
					MpfrWrapper mpfrWrapper_y(mpfr_precision);
					MpfrWrapper mpfrWrapper_z(mpfr_precision);
					mpfr_set_si(mpfrWrapper_x.value, x, MPFR_RNDN);
					mpfr_set_si(mpfrWrapper_y.value, y, MPFR_RNDN);
					mpfr_set_si(mpfrWrapper_z.value, y, MPFR_RNDN);

					this->x = mpfrWrapper_x;
					this->y = mpfrWrapper_y;
					this->z = mpfrWrapper_z;
				}

				if (!dummy)
				{
					uint id = globalPointIdx++;

					this->color = color | (id << 24);

					if (globalPointIdx == 256)
						globalPointIdx = 0;
				}
			}

			void Clear(int gridWidth, int gridHeight)
			{
				memset(grid, backgroundColor, gridWidth * gridHeight * sizeof(uint));
			}

			float2 Position()
			{
				return float2(Calculator::returnFloat(x), Calculator::returnFloat(y));
			}

			float3 Position3D()
			{
				return float3(Calculator::returnFloat(x), Calculator::returnFloat(y), Calculator::returnFloat(z));
			}

			uint GetID() const {
				return this->color >> 24;
			}

			Number GetX() const {
				return x;
			}

			Number GetY() const {
				return y;
			}

			Number GetZ() const {
				return z;
			}

			void SetX(Number val) {
				x = val;
			}

			void SetY(Number val) {
				y = val;
			}

			void SetZ(Number val) {
				z = val;
			}

			void Position(int newX, int newY, PrecisionLevel precisionLevel)
			{
				if (precisionLevel == PrecisionLevel::FLOAT)
				{
					this->x = (float)newX;
					this->y = (float)newY;
				}
				else if (precisionLevel == PrecisionLevel::DOUBLE)
				{
					this->x = (double)newX;
					this->y = (double)newY;
				}
				else
				{
					mpfr_prec_t mpfr_precision;

					if (precisionLevel == PrecisionLevel::MPFR_80)
						mpfr_precision = 80;
					else if (precisionLevel == PrecisionLevel::MPFR_128)
						mpfr_precision = 128;
					else if (precisionLevel == PrecisionLevel::MPFR_256)
						mpfr_precision = 256;

					MpfrWrapper mpfrWrapper_x(mpfr_precision);
					MpfrWrapper mpfrWrapper_y(mpfr_precision);
					mpfr_set_si(mpfrWrapper_x.value, newX, MPFR_RNDN);
					mpfr_set_si(mpfrWrapper_y.value, newY, MPFR_RNDN);

					this->x = mpfrWrapper_x;
					this->y = mpfrWrapper_y;
				}
			}
			
			void Position(int newX, int newY, int newZ, PrecisionLevel precisionLevel)
			{
				if (precisionLevel == PrecisionLevel::FLOAT)
				{
					this->x = (float)newX;
					this->y = (float)newY;
					this->z = (float)newZ;
				}
				else if (precisionLevel == PrecisionLevel::DOUBLE)
				{
					this->x = (double)newX;
					this->y = (double)newY;
					this->z = (double)newZ;
				}
				else
				{
					mpfr_prec_t mpfr_precision;

					if (precisionLevel == PrecisionLevel::MPFR_80)
						mpfr_precision = 80;
					else if (precisionLevel == PrecisionLevel::MPFR_128)
						mpfr_precision = 128;
					else if (precisionLevel == PrecisionLevel::MPFR_256)
						mpfr_precision = 256;

					MpfrWrapper mpfrWrapper_x(mpfr_precision);
					MpfrWrapper mpfrWrapper_y(mpfr_precision);
					MpfrWrapper mpfrWrapper_z(mpfr_precision);
					mpfr_set_si(mpfrWrapper_x.value, newX, MPFR_RNDN);
					mpfr_set_si(mpfrWrapper_y.value, newY, MPFR_RNDN);
					mpfr_set_si(mpfrWrapper_z.value, newY, MPFR_RNDN);
					this->x = mpfrWrapper_x;
					this->y = mpfrWrapper_y;
					this->z = mpfrWrapper_z;
				}
			}

			void Translate(int2 offset, PrecisionLevel precisionLevel)
			{
				if (precisionLevel == PrecisionLevel::FLOAT)
				{
					this->x = std::get<float>(this->x) + offset.x;
					this->y = std::get<float>(this->y) + offset.y;
				}
				else if (precisionLevel == PrecisionLevel::DOUBLE)
				{
					this->x = std::get<double>(this->x) + offset.x;
					this->y = std::get<double>(this->y) + offset.y;
				}
				else
				{
					mpfr_add_si(std::get<MpfrWrapper>(this->x).value, std::get<MpfrWrapper>(this->x).value, offset.x, MPFR_RNDN);
					mpfr_add_si(std::get<MpfrWrapper>(this->y).value, std::get<MpfrWrapper>(this->y).value, offset.y, MPFR_RNDN);
				}
			}

			void Translate(int3 offset, PrecisionLevel precisionLevel)
			{
				if (precisionLevel == PrecisionLevel::FLOAT)
				{
					this->x = std::get<float>(this->x) + offset.x;
					this->y = std::get<float>(this->y) + offset.y;
					this->z = std::get<float>(this->z) + offset.z;
				}
				else if (precisionLevel == PrecisionLevel::DOUBLE)
				{
					this->x = std::get<double>(this->x) + offset.x;
					this->y = std::get<double>(this->y) + offset.y;
					this->z = std::get<double>(this->z) + offset.z;
				}
				else
				{
					mpfr_add_si(std::get<MpfrWrapper>(this->x).value, std::get<MpfrWrapper>(this->x).value, offset.x, MPFR_RNDN);
					mpfr_add_si(std::get<MpfrWrapper>(this->y).value, std::get<MpfrWrapper>(this->y).value, offset.y, MPFR_RNDN);
					mpfr_add_si(std::get<MpfrWrapper>(this->z).value, std::get<MpfrWrapper>(this->z).value, offset.z, MPFR_RNDN);
				}
			}

			void UpdatePrecision(PrecisionLevel precisionLevel)
			{
				if (precisionLevel == PrecisionLevel::FLOAT) {
					Calculator::changeToFloat(x);
					Calculator::changeToFloat(y);
					if(is3D)
						Calculator::changeToFloat(z);
				}
				else if (precisionLevel == PrecisionLevel::DOUBLE) {
					Calculator::changeToDouble(x);
					Calculator::changeToDouble(y);
					if(is3D)
						Calculator::changeToDouble(z);
				}
				else {
					mpfr_prec_t mpfr_precision;

					if (precisionLevel == PrecisionLevel::MPFR_80)
						mpfr_precision = 80;
					else if (precisionLevel == PrecisionLevel::MPFR_128)
						mpfr_precision = 128;
					else if (precisionLevel == PrecisionLevel::MPFR_256)
						mpfr_precision = 256;

					Calculator::changeToMpfrPrecision(x, mpfr_precision);
					Calculator::changeToMpfrPrecision(y, mpfr_precision);
					if(is3D)
						Calculator::changeToMpfrPrecision(z, mpfr_precision);
				}
			}
			void ChangeState(PointState newState)
			{
				state = newState;
			}

			void ChangeMenuState(MenuState newMenuState)
			{
				menuState = newMenuState;
			}

			void ChangeColor(float r, float g, float b)
			{
				color = (color & 0xFF000000) |
						static_cast<uint>(r * 255.0f) << 16 |
					    static_cast<uint>(g * 255.0f) << 8 |
						static_cast<uint>(b * 255.0f);
			}

			uint Id()
			{
				return (color >> 24);
			}

			uint color;

			PointState state = PointState::INACTIVE;
			MenuState menuState = MenuState::CLOSED;

			uint* grid;
			bool is3D = false;
		private:
			Number x, y, z;
	};

	class Rotation {
			public:
				enum class RotationState { INACTIVE, HOVERED, SELECTED };
				enum class RotationMethod{ TwoDRotation, CenterMidPoint, SingleAxisP1, SingleAxisP2, MultiAxis, PlaneNormal, PlaneCircle, PlaneNormalCircle};

				Rotation() = default;
				Rotation(Point* p1, Point* p2, PrecisionLevel precisionLevel, float degrees) : p1(p1), p2(p2), degrees(degrees)
				{
					PreCalcRotationAngles(precisionLevel);

				}
				Rotation(int p1Idx, int p2Idx, PrecisionLevel precisionLevel, float degrees) : p1Idx(p1Idx), p2Idx(p2Idx), degrees(degrees)
				{
					PreCalcRotationAngles(precisionLevel);
				}
				Point* p1, *p2;
				int p1Idx, p2Idx;

				float degrees;
				Number cosRotation, sinRotation, radians;

				void PreCalcRotationAngles(PrecisionLevel precisionLevel)
				{
					radians = Calculator::convertToRadians(degrees, precisionLevel);
					cosRotation = Calculator::cos(radians);
					sinRotation = Calculator::sin(radians);
				}

				// 2D Rotation
				void Rotate()
				{
					Number p1Pos_x = p1->GetX();
					Number p1Pos_y = p1->GetY();

					Number p2Pos_x = p2->GetX();
					Number p2Pos_y = p2->GetY();

					Number midPoint_x = Calculator::divideByTwo(Calculator::add(p1Pos_x, p2Pos_x));
					Number midPoint_y = Calculator::divideByTwo(Calculator::add(p1Pos_y, p2Pos_y));

					Number p1Vec_x = Calculator::sub(p1Pos_x, midPoint_x);
					Number p1Vec_y = Calculator::sub(p1Pos_y, midPoint_y);

					Number p2Vec_x = Calculator::sub(p2Pos_x, midPoint_x);
					Number p2Vec_y = Calculator::sub(p2Pos_y, midPoint_y);

					p1->SetX(Calculator::add(Calculator::sub(Calculator::mul(p1Vec_x, cosRotation), Calculator::mul(p1Vec_y, sinRotation)), midPoint_x));
					p1->SetY(Calculator::add(Calculator::add(Calculator::mul(p1Vec_y, cosRotation), Calculator::mul(p1Vec_x, sinRotation)), midPoint_y));

					p2->SetX(Calculator::add(Calculator::sub(Calculator::mul(p2Vec_x, cosRotation), Calculator::mul(p2Vec_y, sinRotation)), midPoint_x));
					p2->SetY(Calculator::add(Calculator::add(Calculator::mul(p2Vec_y, cosRotation), Calculator::mul(p2Vec_x, sinRotation)), midPoint_y));
				}

				// 2D Animation Rotation
				void Rotate(Number p1Pos_x, Number p1Pos_y, Number p2Pos_x, Number p2Pos_y, PrecisionLevel precisionLevel, float frame)
				{

					Number midPoint_x = Calculator::divideByTwo(Calculator::add(p1Pos_x, p2Pos_x));
					Number midPoint_y = Calculator::divideByTwo(Calculator::add(p1Pos_y, p2Pos_y));

					Number p1Vec_x = Calculator::sub(p1Pos_x, midPoint_x);
					Number p1Vec_y = Calculator::sub(p1Pos_y, midPoint_y);

					Number p2Vec_x = Calculator::sub(p2Pos_x, midPoint_x);
					Number p2Vec_y = Calculator::sub(p2Pos_y, midPoint_y);


					Number radians = Calculator::convertToRadians(degrees * frame, precisionLevel);
					cosRotation = Calculator::cos(radians);
					sinRotation = Calculator::sin(radians);

					p1->SetX(Calculator::add(Calculator::sub(Calculator::mul(p1Vec_x, cosRotation), Calculator::mul(p1Vec_y, sinRotation)), midPoint_x));
					p1->SetY(Calculator::add(Calculator::add(Calculator::mul(p1Vec_y, cosRotation), Calculator::mul(p1Vec_x, sinRotation)), midPoint_y));

					p2->SetX(Calculator::add(Calculator::sub(Calculator::mul(p2Vec_x, cosRotation), Calculator::mul(p2Vec_y, sinRotation)), midPoint_x));
					p2->SetY(Calculator::add(Calculator::add(Calculator::mul(p2Vec_y, cosRotation), Calculator::mul(p2Vec_x, sinRotation)), midPoint_y));
				}

				// 3D Rotations 
				//Vector from the center of mass to the midpoint
				void RotateCenterMidPoint(float3 center)
				{
					float3 p1Pos = p1->Position3D();
					float3 p2Pos = p2->Position3D();

					float3 midPoint = (p1Pos + p2Pos) / 2;

					float3 rVec = normalize(midPoint - center);

					mat4 rMatrix = mat4::Rotate(rVec, Calculator::returnFloat(radians));
					float3 newP1Pos = (float3)(rMatrix * (p1Pos - midPoint)) + midPoint;
					float3 newP2Pos = (float3)(rMatrix * (p2Pos - midPoint)) + midPoint;

					p1->SetX(static_cast<double>(newP1Pos.x));
					p1->SetY(static_cast<double>(newP1Pos.y));
					p1->SetZ(static_cast<double>(newP1Pos.z));

					p2->SetX(static_cast<double>(newP2Pos.x));
					p2->SetY(static_cast<double>(newP2Pos.y));
					p2->SetZ(static_cast<double>(newP2Pos.z));
				}

				//Vector from the center of mass to the midpoint
				void Rotate3DSingleAxisP1(float3 center)
				{
					float3 p1Pos = p1->Position3D();
					float3 p2Pos = p2->Position3D();

					float3 midPoint = (p1Pos + p2Pos) / 2;

					float3 rVec = p1Pos - center;

					mat4 rMatrix = mat4::Rotate(normalize(rVec), Calculator::returnFloat(radians));
					float3 newP1Pos = (float3)(rMatrix * (p1Pos - midPoint)) + midPoint;
					float3 newP2Pos = (float3)(rMatrix * (p2Pos - midPoint)) + midPoint;

					p1->SetX(static_cast<double>(newP1Pos.x));
					p1->SetY(static_cast<double>(newP1Pos.y));
					p1->SetZ(static_cast<double>(newP1Pos.z));

					p2->SetX(static_cast<double>(newP2Pos.x));
					p2->SetY(static_cast<double>(newP2Pos.y));
					p2->SetZ(static_cast<double>(newP2Pos.z));
				}

				//Vector from the center of mass to the midpoint
				void Rotate3DSingleAxisP2(float3 center)
				{
					float3 p1Pos = p1->Position3D();
					float3 p2Pos = p2->Position3D();

					float3 midPoint = (p1Pos + p2Pos) / 2;

					float3 rVec = p2Pos - center;

					mat4 rMatrix = mat4::Rotate(normalize(rVec), Calculator::returnFloat(radians));
					float3 newP1Pos = (float3)(rMatrix * (p1Pos - midPoint)) + midPoint;
					float3 newP2Pos = (float3)(rMatrix * (p2Pos - midPoint)) + midPoint;

					p1->SetX(static_cast<double>(newP1Pos.x));
					p1->SetY(static_cast<double>(newP1Pos.y));
					p1->SetZ(static_cast<double>(newP1Pos.z));

					p2->SetX(static_cast<double>(newP2Pos.x));
					p2->SetY(static_cast<double>(newP2Pos.y));
					p2->SetZ(static_cast<double>(newP2Pos.z));
				}

				void Rotate3DMultiAxis(float3 center)
				{
					float3 p1Pos = p1->Position3D();
					float3 p2Pos = p2->Position3D();

					float3 midPoint = (p1Pos + p2Pos) / 2;

					float3 r1Vec = p2Pos - center;
					float3 r2Vec = p1Pos - center;

					mat4 r1Matrix = mat4::Rotate(normalize(r1Vec), Calculator::returnFloat(radians));
					mat4 r2Matrix = mat4::Rotate(normalize(r2Vec), Calculator::returnFloat(radians));

					float3 newP1Pos = (float3)(r1Matrix * (p1Pos - midPoint)) + midPoint;
					float3 newP2Pos = (float3)(r2Matrix * (p2Pos - midPoint)) + midPoint;

					p1->SetX(static_cast<double>(newP1Pos.x));
					p1->SetY(static_cast<double>(newP1Pos.y));
					p1->SetZ(static_cast<double>(newP1Pos.z));

					p2->SetX(static_cast<double>(newP2Pos.x));
					p2->SetY(static_cast<double>(newP2Pos.y));
					p2->SetZ(static_cast<double>(newP2Pos.z));
				}

				void Rotate3DPlaneNormal(float3 center)
				{
					float3 p1Pos = p1->Position3D();
					float3 p2Pos = p2->Position3D();

					float3 midPoint = (p1Pos + p2Pos) / 2;

					float3 r1Vec = p1Pos - center;
					float3 r2Vec = p2Pos - center;

					float3 nVec = normalize(cross(r1Vec, r2Vec));

					mat4 rMatrix = mat4::Rotate(nVec, Calculator::returnFloat(radians));

					float3 newP1Pos = (float3)(rMatrix * (p1Pos - midPoint)) + midPoint;
					float3 newP2Pos = (float3)(rMatrix * (p2Pos - midPoint)) + midPoint;

					p1->SetX(static_cast<double>(newP1Pos.x));
					p1->SetY(static_cast<double>(newP1Pos.y));
					p1->SetZ(static_cast<double>(newP1Pos.z));

					p2->SetX(static_cast<double>(newP2Pos.x));
					p2->SetY(static_cast<double>(newP2Pos.y));
					p2->SetZ(static_cast<double>(newP2Pos.z));
				}

				void Rotate3DPlaneCircle(float3 center)
				{
					float3 p1Pos = p1->Position3D();
					float3 p2Pos = p2->Position3D();

					float3 midPoint = (p1Pos + p2Pos) / 2;

					float3 r1Vec = p1Pos - center;
					float3 r2Vec = p2Pos - center;

					float3 nVec = normalize(cross(r1Vec, r2Vec));

					float angle = Calculator::returnFloat(radians);
					float3 p1PosOrigin = p1Pos - midPoint;
					float3 p2PosOrigin = p2Pos - midPoint;

					float3 newP1Pos = midPoint + p1PosOrigin * cosf(angle) + cross(nVec, p1PosOrigin) * sinf(angle) + nVec * dot(nVec, p1PosOrigin) * (1 - cosf(angle));
					float3 newP2Pos = midPoint + p2PosOrigin * cosf(angle) + cross(nVec, p2PosOrigin) * sinf(angle) + nVec * dot(nVec, p2PosOrigin) * (1 - cosf(angle));

					p1->SetX(static_cast<double>(newP1Pos.x));
					p1->SetY(static_cast<double>(newP1Pos.y));
					p1->SetZ(static_cast<double>(newP1Pos.z));

					p2->SetX(static_cast<double>(newP2Pos.x));
					p2->SetY(static_cast<double>(newP2Pos.y));
					p2->SetZ(static_cast<double>(newP2Pos.z));
				}

				void Rotate3DPlaneNormalCircle(float3 center)
				{
					float3 p1Pos = p1->Position3D();
					float3 p2Pos = p2->Position3D();

					float3 midPoint = (p1Pos + p2Pos) / 2;

					float3 rVec = normalize(midPoint - center);


					float angle = Calculator::returnFloat(radians);
					float3 p1PosOrigin = p1Pos - midPoint;
					float3 p2PosOrigin = p2Pos - midPoint;

					float3 newP1Pos = midPoint + p1PosOrigin * cosf(angle) + cross(rVec, p1PosOrigin) * sinf(angle) + rVec * dot(rVec, p1PosOrigin) * (1 - cosf(angle));
					float3 newP2Pos = midPoint + p2PosOrigin * cosf(angle) + cross(rVec, p2PosOrigin) * sinf(angle) + rVec * dot(rVec, p2PosOrigin) * (1 - cosf(angle));
					p1->SetX(static_cast<double>(newP1Pos.x));
					p1->SetY(static_cast<double>(newP1Pos.y));
					p1->SetZ(static_cast<double>(newP1Pos.z));

					p2->SetX(static_cast<double>(newP2Pos.x));
					p2->SetY(static_cast<double>(newP2Pos.y));
					p2->SetZ(static_cast<double>(newP2Pos.z));
				}

				void ChangeState(RotationState newState)
				{
					state = newState;
				}

				RotationState state = RotationState::INACTIVE;
	};

	class Spiroplot2D
	{
	public:
		bool* hidePoint = new bool[256];

		PlotState plotState;

		Spiroplot2D() = default;
		Spiroplot2D(int gridWidth, int gridHeight, int pointSize, PlotState plotState) : gridWidth(gridWidth), gridHeight(gridHeight), pointSize(pointSize), plotState(plotState)
		{
			memset(hidePoint, false, 256 * sizeof(bool));

			points.clear();
			pointsOrder.clear();

			rotations.clear();

			totalPoints = 0;
			totalRotations = 0;

			startRotation = 0;
			currentIteration = 0;

			grid = new uint[gridWidth * gridHeight];
			memset(grid, backgroundColor, gridWidth * gridHeight * sizeof(uint));
		}

		void AddPoint(float x, float y, uint color, PrecisionLevel precisionLevel, bool randomColor = false)
		{
			uint newColor = color;
			if (randomColor && globalPointIdx < basicColors.size())
				newColor = basicColors[globalPointIdx];

			Point newPoint = Point(x, y, precisionLevel, gridWidth, gridHeight, newColor);

			hidePoint[newPoint.Id()] = false;

			if (totalPoints == 0 || totalRotations == 0)
			{
				points.push_back(newPoint);
				pointsOrder.push_back(newPoint.GetID());
			}
			else
			{
				int* p1Idxs = new int[totalRotations];
				int* p2Idxs = new int[totalRotations];
				for (int i = 0; i < totalRotations; i++)
				{
					bool p1 = false, p2 = false;
					for (int j = 0; j < totalPoints; j++)
					{
						if (rotations[i].p1 == &points[j])
						{
							p1Idxs[i] = j;

							if (p2)
								break;

							p1 = true;
						}
						else if (rotations[i].p2 == &points[j])
						{
							p2Idxs[i] = j;

							if (p1)
								break;

							p2 = true;
						}
					}
				}

				points.push_back(newPoint);
				pointsOrder.push_back(newPoint.GetID());

				for (int i = 0; i < totalRotations; i++)
				{
					rotations[i].p1 = &points[p1Idxs[i]];
					rotations[i].p2 = &points[p2Idxs[i]];
				}
			}

			totalPoints++;
		}

		void AddRotation(Point* p1, Point* p2, PrecisionLevel precisionLevel, float rotation = 90)
		{
			Rotation newRotation = Rotation(p1, p2, precisionLevel, rotation);
			rotations.push_back(newRotation);
			totalRotations++;
		}

		void AddRotation(Rotation rotation)
		{
			rotations.push_back(rotation);
			totalRotations++;
		}


		// MAY HAVE SOME BUGGS WHEN DELETING POINTS THAT HAVE A ROTATION
		void DeletePoint(Point* delPoint)
		{
			if (totalPoints == 1)
			{
				points.clear();
				pointsOrder.clear();

				globalPointIdx = 0;
			}
			else
			{
				// Find the index of the element to delete
				int indexToDelete;
				for (int i = 0; i < totalPoints; i++) {
					if (&points[i] == delPoint) {
						indexToDelete = i;
						break;
					}
				}

				std::list<Rotation> newRotations;

				for (int i = 0; i < totalRotations; i++)
				{
					if (rotations[i].p1 != delPoint && rotations[i].p2 != delPoint)
						newRotations.push_back(rotations[i]);
				}

				int newTotalRotations = newRotations.size();

				if (newTotalRotations == 0)
				{
					rotations.clear();
					totalRotations = 0;


					uint pointOrderIdToDelete = delPoint->GetID();
					for (auto pointOrder = pointsOrder.begin(); pointOrder != pointsOrder.end(); ++pointOrder) {
						if (*pointOrder == pointOrderIdToDelete) {
							pointsOrder.erase(pointOrder);
							break;
						}
					}

					points.erase(points.begin() + indexToDelete);
				}
				else
				{
					if (newTotalRotations != totalRotations)
					{
						rotations.clear();

						for (auto rotation = newRotations.begin(); rotation != newRotations.end(); ++rotation) {
							rotations.push_back(*rotation);
						}

						totalRotations = newTotalRotations;
					}

					int* p1Idxs = new int[totalRotations];
					int* p2Idxs = new int[totalRotations];
					for (int i = 0; i < totalRotations; i++)
					{
						bool p1 = false, p2 = false;
						for (int j = 0; j < totalPoints; j++)
						{
							if (rotations[i].p1 == &points[j])
							{
								if (j < indexToDelete)
									p1Idxs[i] = j;
								else if (j > indexToDelete)
									p1Idxs[i] = j - 1;
								else
									std::cout << "error: point is already deleted";

								if (p2)
									break;

								p1 = true;
							}
							else if (rotations[i].p2 == &points[j])
							{
								if (j < indexToDelete)
									p2Idxs[i] = j;
								else if (j > indexToDelete)
									p2Idxs[i] = j - 1;
								else
									std::cout << "error: point is already deleted";

								if (p1)
									break;

								p2 = true;
							}
						}
					}

					uint pointOrderIdToDelete = delPoint->GetID();
					for (auto pointOrder = pointsOrder.begin(); pointOrder != pointsOrder.end(); ++pointOrder) {
						if (*pointOrder == pointOrderIdToDelete) {
							pointsOrder.erase(pointOrder);
							break;
						}
					}

					points.erase(points.begin() + indexToDelete);

					// Reference the points
					for (int i = 0; i < totalRotations; i++)
					{
						rotations[i].p1 = &points[p1Idxs[i]];
						rotations[i].p2 = &points[p2Idxs[i]];
					}
				}
			}

			totalPoints--;
		}

		void DeleteRotation(Rotation* delRotation)
		{
			if (totalRotations == 1)
			{
				rotations.clear();
			}
			else
			{
				// Find the index of the element to delete
				int indexToDelete;
				for (int i = 0; i < totalRotations; i++) {
					if (&rotations[i] == delRotation) {
						indexToDelete = i;
						break;
					}
				}

				rotations.erase(rotations.begin() + indexToDelete);

				if (startRotation == indexToDelete && indexToDelete == totalRotations - 1)
					startRotation = 0;
				else if (startRotation > indexToDelete)
					startRotation--;
			}

			totalRotations--;
		}

		void HidePoint(uint id)
		{
			hidePoint[id] = !hidePoint[id];
		}

		bool HiddenPoint(uint id)
		{
			return hidePoint[id];
		}

		void CenterAndScalePoints(PrecisionLevel precisionLevel)
		{
			Center(precisionLevel);
		}

		void Center(PrecisionLevel precisionLevel)
		{
			if (!points.empty())
			{
				float2 centroid = float2(0);

				for (int i = 0; i < totalPoints; i++)
				{
					centroid += points[i].Position();
				}

				centroid /= totalPoints;
				float2 offset = float2(gridWidth / 2, gridHeight / 2) - centroid;

				int2 rounded_offset = int2(std::round(offset.x), std::round(offset.y));
				for (int i = 0; i < totalPoints; i++)
				{
					points[i].Translate(rounded_offset, precisionLevel);
				}
			}
		}

		void ScaleToFitScreen(PrecisionLevel precisionLevel)
		{

			// 1. Compute the centroid of the points.
			float2 centroid = float2(0);
			for (int i = 0; i < totalPoints; i++)
			{
				centroid += points[i].Position();
			}
			centroid /= totalPoints;

			// 2. Find the distance r_max to the farthest point from the centroid.
			float r_max = 0.0f;
			for (int i = 0; i < totalPoints; i++)
			{
				float distance = length(points[i].Position() - centroid);
				if (distance > r_max)
					r_max = distance;
			}

			// 3. Compute the scaling factor.
			float scale_factor = std::min((gridWidth / 2 ) / r_max, (gridHeight / 2) / r_max);

			// 4. Scale the points.
			for (int i = 0; i < totalPoints; i++)
			{
				float2 relative_position = points[i].Position() - centroid;
				float2 scaled_position = centroid + relative_position * scale_factor;
				points[i].Position(scaled_position.x, scaled_position.y, precisionLevel);
			}
		}


		void Scale(PrecisionLevel precisionLevel)
		{
			const float MARGIN = 100.0f;  // Distance to stay within the border

			if (!points.empty())
			{
				// 1. Compute the centroid of the points.
				float2 centroid = float2(0);
				for (int i = 0; i < totalPoints; i++)
				{
					centroid += points[i].Position();
				}
				centroid /= totalPoints;

				// 2. Determine the maximum scaling factor.
				float max_scale = FLT_MAX;
				for (int i = 0; i < totalPoints; i++)
				{
					float2 to_point = points[i].Position() - centroid;
					float scale_x, scale_y;

					if (to_point.x != 0)
						scale_x = (to_point.x > 0 ? (gridWidth - MARGIN - centroid.x) : (MARGIN - centroid.x)) / to_point.x;
					else
						scale_x = FLT_MAX;

					if (to_point.y != 0)
						scale_y = (to_point.y > 0 ? (gridHeight - MARGIN - centroid.y) : (MARGIN - centroid.y)) / to_point.y;
					else
						scale_y = FLT_MAX;

					float min_scale_for_point = std::min(scale_x, scale_y);
					if (min_scale_for_point < max_scale)
						max_scale = min_scale_for_point;
				}

				// 3. Scale all points using the determined maximum scaling factor.
				for (int i = 0; i < totalPoints; i++)
				{
					float2 offset = (points[i].Position() - centroid) * max_scale;
					float2 new_position = centroid + offset - points[i].Position();
					int2 rounded_position = int2(std::round(new_position.x), std::round(new_position.y));
					points[i].Translate(rounded_position , precisionLevel);
				}
			}
		}


		void Import(std::list<Point> points, std::list<Rotation> rotations)
		{
			totalPoints = points.size();
			totalRotations = rotations.size();

			if (totalPoints == 0 || totalRotations == 0)
				return;

			this->points.clear();

			for (auto point = points.begin(); point != points.end(); ++point) {
				this->points.push_back(*point);
				this->pointsOrder.push_back(this->points.back().GetID());
			}

			this->rotations.clear();
			int j = 0;
			for (auto rotation = rotations.begin(); rotation != rotations.end(); ++rotation) {
				this->rotations.push_back(*rotation);
				this->rotations[j].p1 = &this->points[this->rotations[j].p1Idx];
				this->rotations[j].p2 = &this->points[this->rotations[j].p2Idx];
				j++;
			}

		}

		Rotation CurrentRotation()
		{
			int currentRotation = (currentIteration + startRotation) % totalRotations;
			return rotations[currentRotation];
		}
		void Iterate()
		{
			Rotation rotation = CurrentRotation();

			rotation.Rotate();

			Fill(rotation.p1, rotation.p2);

			currentIteration++;
		}

		void AnimatedRotation(float frame, Number originalP1PosX, Number originalP1PosY, Number originalP2PosX, Number originalP2PosY, PrecisionLevel precisionLevel, bool finish)
		{
			Rotation rotation = CurrentRotation();

			if (!finish)
				rotation.Rotate(originalP1PosX, originalP1PosY, originalP2PosX, originalP2PosY, precisionLevel, frame);
			else
			{
				rotation.Rotate(originalP1PosX, originalP1PosY, originalP2PosX, originalP2PosY, precisionLevel, 1);
				Fill(rotation.p1, rotation.p2);
				currentIteration++;
			}
		}

		void Fill(Point* p1, Point* p2)
		{
			// PCHECK
			float2 p1_pos = p1->Position();
			float2 p2_pos = p2->Position();

			if (plotState == LIFO)
			{
				FillWithCircleLIFO(round(p1_pos.x), gridHeight - round(p1_pos.y), p1->color); // Y-axis inversed
				FillWithCircleLIFO(round(p2_pos.x), gridHeight - round(p2_pos.y), p2->color); // Y-axis inversed
			}
			else if (plotState == FIFO)
			{
				FillWithCircleFIFO(round(p1_pos.x), gridHeight - round(p1_pos.y), p1->color); // Y-axis inversed
				FillWithCircleFIFO(round(p2_pos.x), gridHeight - round(p2_pos.y), p2->color); // Y-axis inversed
			}
			else
			{
				FillWithCircleORDER(round(p1_pos.x), gridHeight - round(p1_pos.y), p1->color, p1); // Y-axis inversed
				FillWithCircleORDER(round(p2_pos.x), gridHeight - round(p2_pos.y), p2->color, p2); // Y-axis inversed
			}
		}

		void FillWithCircleLIFO(int center_x, int center_y, uint color)
		{
			for (int x = center_x - pointSize * (gridWidth / GRIDWIDTH); x <= center_x + pointSize * (gridWidth / GRIDWIDTH); x++) {
				for (int y = center_y - pointSize * (gridWidth / GRIDWIDTH); y <= center_y + pointSize * (gridWidth / GRIDWIDTH); y++) {

					// Check if the coords are within the bounds of the grid 
					if (x >= 0 && x < gridWidth && y >= 0 && y < gridHeight) {
						// Calculate the distance between the current point and the center of the circle
						double distance = sqrt(pow(x - center_x, 2) + pow(y - center_y, 2));

						// If the distance is less than or equal to the radius, mark the point as part of the circle
						if (distance <= pointSize * (gridWidth / GRIDWIDTH)) {
							grid[x + y * gridWidth] = color;
						}
					}
				}
			}
		}

		void FillWithCircleFIFO(int center_x, int center_y, uint color)
		{
			for (int x = center_x - pointSize * (gridWidth / GRIDWIDTH); x <= center_x + pointSize * (gridWidth / GRIDWIDTH); x++) {
				for (int y = center_y - pointSize * (gridWidth / GRIDWIDTH); y <= center_y + pointSize * (gridWidth / GRIDWIDTH); y++) {
					// Check if the coords are within the bounds of the grid 
					if (x >= 0 && x < gridWidth && y >= 0 && y < gridHeight) {
						// Calculate the distance between the current point and the center of the circle
						double distance = sqrt(pow(x - center_x, 2) + pow(y - center_y, 2));

						// SKIP plot if another point has drawn the pixel.
						if (grid[x + y * gridWidth] == backgroundColor)
						{
							// If the distance is less than or equal to the radius, mark the point as part of the circle
							if (distance <= pointSize * (gridWidth / GRIDWIDTH)) {
								grid[x + y * gridWidth] = color;
							}
						}

					}
				}
			}
		}

		void FillWithCircleORDER(int center_x, int center_y, uint color, Point* p)
		{
			for (int x = center_x - pointSize * (gridWidth / GRIDWIDTH); x <= center_x + pointSize * (gridWidth / GRIDWIDTH); x++) {
				for (int y = center_y - pointSize * (gridWidth / GRIDWIDTH); y <= center_y + pointSize * (gridWidth / GRIDWIDTH); y++) {

					// Check if the coords are within the bounds of the grid 
					if (x >= 0 && x < gridWidth && y >= 0 && y < gridHeight) {
						// Calculate the distance between the current point and the center of the circle
						double distance = sqrt(pow(x - center_x, 2) + pow(y - center_y, 2));

						// If the distance is less than or equal to the radius, mark the point as part of the circle
						if (distance <= pointSize * (gridWidth / GRIDWIDTH)) {
							p->grid[x + y * gridWidth] = color;
						}
					}
				}
			}
		}

		void ChangePointSize(int newSize)
		{
			pointSize = newSize;
		}

		void UpdatePrecision(PrecisionLevel precisionLevel)
		{
			for (int i = 0; i < totalPoints; i++)
			{
				points[i].UpdatePrecision(precisionLevel);
			}

			for (int i = 0; i < totalRotations; i++)
			{
				rotations[i].PreCalcRotationAngles(precisionLevel);
			}

		}

		void UpdatePlotState(PlotState newPlotState)
		{
			plotState = newPlotState;
		}

		void Clear()
		{
			memset(grid, backgroundColor, gridWidth * gridHeight * sizeof(uint));

			if (totalPoints > 0)
			{
				for (int i = 0; i < totalPoints; i++)
					points[i].Clear(gridWidth, gridHeight);
			}

			if (totalRotations != 0)
				startRotation = (currentIteration + startRotation) % totalRotations;
			else
				startRotation = 0;

			currentIteration = 0;
		}

		void Reset()
		{
			points.clear();
			pointsOrder.clear();

			rotations.clear();

			totalPoints = 0;
			totalRotations = 0;

			globalPointIdx = 0;

			memset(hidePoint, false, 256 * sizeof(bool));

			Clear();
		}
		uint* grid;
		int gridWidth, gridHeight;
		int pointSize;

		std::vector<Point> points;
		std::vector<uint> pointsOrder;

		std::vector<Rotation> rotations;

		int totalPoints;
		int totalRotations;

		int startRotation;
		int currentIteration;
	};

	class Spiroplot3D
	{
	public:
		bool* hidePoint = new bool[256];

		PlotState plotState;

		Spiroplot3D() = default;
		Spiroplot3D(int gridWidth, int gridHeight, int gridDepth, int pointSize, PlotState plotState) : gridWidth(gridWidth), gridHeight(gridHeight), gridDepth(gridDepth), pointSize(pointSize), plotState(plotState)
		{
			memset(hidePoint, false, 256 * sizeof(bool));

			points.clear();
			pointsOrder.clear();

			rotations.clear();

			totalPoints = 0;
			totalRotations = 0;

			startRotation = 0;
			currentIteration = 0;

			grid = new uint[gridWidth * gridHeight * gridDepth];
			downSampledGrid = new uint[(gridWidth / gridWidthBlock) * (gridHeight / gridHeightBlock) * (gridDepth / gridDepthBlock)];
			
			bbox = new int[6]{ std::numeric_limits<int>::max(), std::numeric_limits<int>::max(), std::numeric_limits<int>::max(), -1, -1, -1 } ;
			updatedCells = new int[1000 * 2 * 4];
			//memset(grid, 0, gridWidth * gridHeight * gridDepth * sizeof(uint));
			memset(grid, backgroundColor, gridWidth * gridHeight * gridDepth * sizeof(uint));
			memset(downSampledGrid, 0, (gridWidth / gridWidthBlock) * (gridHeight / gridHeightBlock) * (gridDepth / gridDepthBlock) * sizeof(uint));
			memset(updatedCells, 0, 1000 * 2 * 4);
			int z = 400;
			PrecisionLevel defPrecision = PrecisionLevel::DOUBLE;

			// GPU
#ifdef GPU
			gridBuffer = new Buffer(gridWidth * gridHeight * gridDepth * sizeof(uint), grid, 0);
			downSampledGridBuffer = new Buffer((gridWidth / gridWidthBlock) * (gridHeight / gridHeightBlock) * (gridDepth / gridDepthBlock) * sizeof(uint), downSampledGrid, 0);
			bboxBuffer = new Buffer(6 * sizeof(int), bbox, 0);
			updatedCellsBuffer = new Buffer(1000 * 2 * 4 * sizeof(int), updatedCells, 0);
#endif
			/*
			// 2D Rotation
			AddPoint(200, 500, z, 0xFF0000, defPrecision);
			AddPoint(500, 500, z, 0x00FF00, defPrecision);
			AddPoint(500, 200, z, 0x0000FF, defPrecision);

			AddRotation(&points[0], &points[1], defPrecision, 90);
			AddRotation(&points[1], &points[2], defPrecision, 90);
			*/

			// RotateCenterMidPoint
			AddPoint(200, 500, 200, 0xFF0000, defPrecision);
			AddPoint(500, 500, 200, 0x00FF00, defPrecision);
			AddPoint(500, 200, 500, 0x0000FF, defPrecision);
			AddPoint(200, 200, 500, 0x00FFFF, defPrecision);
			
			AddRotation(&points[0], &points[1], defPrecision, 90);
			AddRotation(&points[1], &points[2], defPrecision, 45);
			AddRotation(&points[2], &points[3], defPrecision, 15);


			/*
			// Rotate3DSingleAxisP2
			AddPoint(12, 500, 100, 0xFF0000, defPrecision);
			AddPoint(500, 600, 400, 0x00FF00, defPrecision);
			AddPoint(345, 500, 400, 0x0000FF, defPrecision);

			AddRotation(&points[0], &points[1], defPrecision, 1);
			AddRotation(&points[1], &points[2], defPrecision, 1);
			*/

			/*
			// Rotate3DMultiAxis
			AddPoint(200, 500, 200, 0xFF0000, defPrecision);
			AddPoint(500, 500, 200, 0x00FF00, defPrecision);
			AddPoint(500, 200, 500, 0x0000FF, defPrecision);
			AddPoint(200, 200, 500, 0x00FFFF, defPrecision);

			AddRotation(&points[0], &points[1], defPrecision, 1);
			AddRotation(&points[1], &points[2], defPrecision, 1);
			AddRotation(&points[2], &points[3], defPrecision, 1);
			*/

			// Rotate3DPlaneNormal
			/*
			AddPoint(200, 500, 200, 0xFF0000, defPrecision);
			AddPoint(500, 500, 200, 0x00FF00, defPrecision);
			AddPoint(500, 200, 500, 0x0000FF, defPrecision);
			AddPoint(200, 200, 500, 0x00FFFF, defPrecision);

			AddRotation(&points[0], &points[1], defPrecision, 1);
			AddRotation(&points[1], &points[2], defPrecision, 90);
			AddRotation(&points[2], &points[3], defPrecision, 1);
			*/

			
			int i = 0;

			/*
			float3 initialCentroid = Center();
			std::cout << "Initial centroid = " << initialCentroid.x << " | "  << initialCentroid.y << " | " << initialCentroid.z << std::endl;
			*/

			/*
			while (i < 100000)
			{
				Iterate(Rotation::RotationMethod::PlaneNormal);
				i++;
			}*/
			

			//Export("3D Rotation.obj");


		}

		void LoadTemplate(int i, bool useGPU)
		{
			Reset(useGPU);
			PrecisionLevel defPrecision = PrecisionLevel::DOUBLE;
			switch (i)
			{
				case 0:
					// RotateCenterMidPoint
					AddPoint(200, 500, 200, 0xFF0000, defPrecision);
					AddPoint(500, 500, 200, 0x00FF00, defPrecision);
					AddPoint(500, 200, 500, 0x0000FF, defPrecision);
					AddPoint(200, 200, 500, 0x00FFFF, defPrecision);

					AddRotation(&points[0], &points[1], defPrecision, 90);
					AddRotation(&points[1], &points[2], defPrecision, 45);
					AddRotation(&points[2], &points[3], defPrecision, 15);
					break;
				case 1:
					// Rotate3DSingleAxisP2
					AddPoint(12, 500, 100, 0xFF0000, defPrecision);
					AddPoint(500, 600, 400, 0x00FF00, defPrecision);
					AddPoint(345, 500, 400, 0x0000FF, defPrecision);

					AddRotation(&points[0], &points[1], defPrecision, 1);
					AddRotation(&points[1], &points[2], defPrecision, 1);
					break;
				case 2:
					// Rotate3DMultiAxis
					AddPoint(200, 500, 200, 0xFF0000, defPrecision);
					AddPoint(500, 500, 200, 0x00FF00, defPrecision);
					AddPoint(500, 200, 500, 0x0000FF, defPrecision);
					AddPoint(200, 200, 500, 0x00FFFF, defPrecision);

					AddRotation(&points[0], &points[1], defPrecision, 1);
					AddRotation(&points[1], &points[2], defPrecision, 1);
					AddRotation(&points[2], &points[3], defPrecision, 1);
					break;
				case 3:
					// Rotate3DPlaneNormal
					AddPoint(200, 500, 200, 0xFF0000, defPrecision);
					AddPoint(500, 500, 200, 0x00FF00, defPrecision);
					AddPoint(500, 200, 500, 0x0000FF, defPrecision);
					AddPoint(200, 200, 500, 0x00FFFF, defPrecision);

					AddRotation(&points[0], &points[1], defPrecision, 1);
					AddRotation(&points[1], &points[2], defPrecision, 90);
					AddRotation(&points[2], &points[3], defPrecision, 1);
					break;
				default:
					return;

			}
		}
		// Function to calculate distance
		double distance(int x1, int y1, int z1, int x2, int y2, int z2) {
			return std::sqrt(std::pow(x2 - x1, 2) + std::pow(y2 - y1, 2) + std::pow(z2 - z1, 2));
		}

		double distance(float3 a, float3 b) {
			return std::sqrt(std::pow(b.x - a.x, 2) + std::pow(b.y - a.y, 2) + std::pow(b.z - a.z, 2));
		}

		uint GetPixel(Ray ray) {

			//uint color = 0;
			uint color = backgroundColor;
			if (!IntersectCheck(ray))
				return color;

			int3 currentPos;// = IntersectGrid(ray);
			currentPos.x = std::floor(ray.O.x);
			currentPos.y = std::floor(ray.O.y);
			currentPos.z = std::floor(ray.O.z);

			if (currentPos.x < -1 || currentPos.x > gridWidth || currentPos.y < -1 || currentPos.y > gridHeight || currentPos.z < -1 || currentPos.z > gridDepth)
				return color;


			float tDeltaX = (ray.D.x == 0.0f) ? std::numeric_limits<float>::max() : abs(1.0 / ray.D.x);
			float tDeltaY = (ray.D.y == 0.0f) ? std::numeric_limits<float>::max() : abs(1.0 / ray.D.y);
			float tDeltaZ = (ray.D.z == 0.0f) ? std::numeric_limits<float>::max() : abs(1.0 / ray.D.z);

			int stepX = (ray.D.x > 0) ? 1 : -1;
			int stepY = (ray.D.y > 0) ? 1 : -1;
			int stepZ = (ray.D.z > 0) ? 1 : -1;

			float tMaxX = tDeltaX * ((stepX > 0) ? (currentPos.x + 1 - ray.O.x) : (currentPos.x - ray.O.x));
			float tMaxY = tDeltaY * ((stepY > 0) ? (currentPos.y + 1 - ray.O.y) : (currentPos.y - ray.O.y));
			float tMaxZ = tDeltaZ * ((stepZ > 0) ? (currentPos.z + 1 - ray.O.z) : (currentPos.z - ray.O.z));

			int justOutX = (stepX > 0) ? gridWidth : -1;
			int justOutY = (stepY > 0) ? gridHeight : -1;
			int justOutZ = (stepZ > 0) ? gridDepth : -1;


			while (true) {

				// SPEED UP
				//if (currentPos.x < 0 || currentPos.x >= gridWidth || currentPos.y < 0 || currentPos.y >= gridHeight || currentPos.z < 0 || currentPos.z >= gridDepth)
				//	return color;

				if (currentPos.x >= 0 && currentPos.x < gridWidth && currentPos.y >= 0 && currentPos.y < gridHeight && currentPos.z >= 0 && currentPos.z < gridDepth) {
					if (grid[currentPos.x + gridWidth * (currentPos.y + gridHeight * currentPos.z)] != color)
						return grid[currentPos.x + gridWidth * (currentPos.y + gridHeight * currentPos.z)];
				}

				if (tMaxX < tMaxY) {
					if (tMaxX < tMaxZ) {
						currentPos.x += stepX;
						if (currentPos.x == justOutX)
							return color;
						tMaxX += tDeltaX;
					}
					else {
						currentPos.z += stepZ;
						if (currentPos.z == justOutZ)
							return color;
						tMaxZ += tDeltaZ;
					}
				}
				else {
					if (tMaxY < tMaxZ) {
						currentPos.y += stepY;
						if (currentPos.y == justOutY)
							return color;
						tMaxY += tDeltaY;
					}
					else {
						currentPos.z += stepZ;
						if (currentPos.z == justOutZ)
							return color;
						tMaxZ += tDeltaZ;
					}
				}
			}
		}

		int3 GetPixelPos(Ray ray) {

			//uint color = 0;
			uint color = backgroundColor;
			if (!IntersectCheck(ray))
				return int3(-1,-1,-1);

			int3 currentPos;
			currentPos.x = std::floor(ray.O.x);
			currentPos.y = std::floor(ray.O.y);
			currentPos.z = std::floor(ray.O.z);

			if (currentPos.x < -1 || currentPos.x > gridWidth || currentPos.y < -1 || currentPos.y > gridHeight || currentPos.z < -1 || currentPos.z > gridDepth)
				return int3(-1, -1, -1);
			
			currentPos.x = min(max(0, currentPos.x), gridWidth - 1);
			currentPos.y = min(max(0, currentPos.y), gridHeight - 1);
			currentPos.z = min(max(0, currentPos.z), gridDepth - 1);

			return currentPos;
		}
	/*
	uint GetPixel(Ray ray) {

		float3 originalO = ray.O;
		//uint color = 0;
		uint color = backgroundColor;
		if (!IntersectCheck(ray))
			return color;

		int3 currentPos;// = IntersectGrid(ray);
		currentPos.x = std::floor(ray.O.x);
		currentPos.y = std::floor(ray.O.y);
		currentPos.z = std::floor(ray.O.z);

		if (currentPos.x < -1 || currentPos.x > gridWidth || currentPos.y < -1 || currentPos.y > gridHeight || currentPos.z < -1 || currentPos.z > gridDepth)
			return color;

		int3 downSampledPos;
		downSampledPos.x = currentPos.x / gridWidthBlock;
		downSampledPos.y = currentPos.y / gridHeightBlock;
		downSampledPos.z = currentPos.z / gridDepthBlock;

		float downSampledtDeltaX = (ray.D.x == 0.0f) ? std::numeric_limits<float>::max() : abs(gridWidthBlock / ray.D.x);
		float downSampledtDeltaY = (ray.D.y == 0.0f) ? std::numeric_limits<float>::max() : abs(gridHeightBlock / ray.D.y);
		float downSampledtDeltaZ = (ray.D.z == 0.0f) ? std::numeric_limits<float>::max() : abs(gridDepthBlock / ray.D.z);


		int stepX = (ray.D.x > 0) ? 1 : -1;
		int stepY = (ray.D.y > 0) ? 1 : -1;
		int stepZ = (ray.D.z > 0) ? 1 : -1;

		float downSampledtMaxX = downSampledtDeltaX * ((stepX > 0) ? (downSampledPos.x + 1 - ray.O.x / gridWidthBlock) : (ray.O.x / gridWidthBlock - downSampledPos.x));
		float downSampledtMaxY = downSampledtDeltaY * ((stepY > 0) ? (downSampledPos.y + 1 - ray.O.y / gridHeightBlock) : (ray.O.y / gridHeightBlock - downSampledPos.y));
		float downSampledtMaxZ = downSampledtDeltaZ * ((stepZ > 0) ? (downSampledPos.z + 1 - ray.O.z / gridDepthBlock) : (ray.O.z / gridDepthBlock - downSampledPos.z));

		int downSampledtjustOutX = (stepX > 0) ? gridWidth / gridWidthBlock : -1;
		int downSampledtjustOutY = (stepY > 0) ? gridHeight / gridHeightBlock : -1;
		int downSampledtjustOutZ = (stepZ > 0) ? gridDepth / gridDepthBlock : -1;

		float tDeltaX = (ray.D.x == 0.0f) ? std::numeric_limits<float>::max() : abs(1.0 / ray.D.x);
		float tDeltaY = (ray.D.y == 0.0f) ? std::numeric_limits<float>::max() : abs(1.0 / ray.D.y);
		float tDeltaZ = (ray.D.z == 0.0f) ? std::numeric_limits<float>::max() : abs(1.0 / ray.D.z);

		while (true) {

			if (downSampledPos.x >= 0 && downSampledPos.x < gridWidth / gridWidthBlock && downSampledPos.y >= 0 && downSampledPos.y < gridHeight / gridHeightBlock && downSampledPos.z >= 0 && downSampledPos.z < gridDepth / gridDepthBlock) {
				if (downSampledGrid[downSampledPos.x + gridWidth / gridWidthBlock * (downSampledPos.y + gridHeight / gridHeightBlock * downSampledPos.z)] == 1)
				{
					ray.O = originalO;
					Intersect(ray, downSampledPos.x * gridWidthBlock, downSampledPos.y * gridHeightBlock, downSampledPos.z * gridDepthBlock, (downSampledPos.x + 1) * gridWidthBlock - 1, (downSampledPos.y + 1) * gridHeightBlock - 1, (downSampledPos.z + 1) * gridDepthBlock - 1);

					currentPos.x = std::floor(ray.O.x);
					currentPos.y = std::floor(ray.O.y);
					currentPos.z = std::floor(ray.O.z);

					float tMaxX = tDeltaX * ((stepX > 0) ? (currentPos.x + 1 - ray.O.x) : (currentPos.x - ray.O.x));
					float tMaxY = tDeltaY * ((stepY > 0) ? (currentPos.y + 1 - ray.O.y) : (currentPos.y - ray.O.y));
					float tMaxZ = tDeltaZ * ((stepZ > 0) ? (currentPos.z + 1 - ray.O.z) : (currentPos.z - ray.O.z));

					int justOutX = (stepX > 0) ? (downSampledPos.x + 1) * gridWidthBlock : downSampledPos.x * gridWidthBlock - 1;
					int justOutY = (stepY > 0) ? (downSampledPos.y + 1) * gridHeightBlock : downSampledPos.y * gridHeightBlock - 1;
					int justOutZ = (stepZ > 0) ? (downSampledPos.z + 1) * gridDepthBlock : downSampledPos.z * gridDepthBlock - 1;

					while (true) {

						if (currentPos.x >= 0 && currentPos.x < gridWidth && currentPos.y >= 0 && currentPos.y < gridHeight && currentPos.z >= 0 && currentPos.z < gridDepth) {
							if (grid[currentPos.x + gridWidth * (currentPos.y + gridHeight * currentPos.z)] != color)
							{
								return grid[currentPos.x + gridWidth * (currentPos.y + gridHeight * currentPos.z)];
							}
						}

						if (tMaxX < tMaxY) {
							if (tMaxX < tMaxZ) {
								currentPos.x += stepX;
								if (currentPos.x == justOutX)
									break;
								tMaxX += tDeltaX;
							}
							else {
								currentPos.z += stepZ;
								if (currentPos.z == justOutZ)
									break;
								tMaxZ += tDeltaZ;
							}
						}
						else {
							if (tMaxY < tMaxZ) {
								currentPos.y += stepY;
								if (currentPos.y == justOutY)
									break;
								tMaxY += tDeltaY;
							}
							else {
								currentPos.z += stepZ;
								if (currentPos.z == justOutZ)
									break;
								tMaxZ += tDeltaZ;
							}
						}
					}
				}
			}

			if (downSampledtMaxX < downSampledtMaxY) {
				if (downSampledtMaxX < downSampledtMaxZ) {
					downSampledPos.x += stepX;
					if (downSampledPos.x == downSampledtjustOutX)
						return color;
					downSampledtMaxX += downSampledtDeltaX;
				}
				else {
					downSampledPos.z += stepZ;
					if (downSampledPos.z == downSampledtjustOutZ)
						return color;
					downSampledtMaxZ += downSampledtDeltaZ;
				}
			}
			else {
				if (downSampledtMaxY < downSampledtMaxZ) {
					downSampledPos.y += stepY;
					if (downSampledPos.y == downSampledtjustOutY)
						return color;
					downSampledtMaxY += downSampledtDeltaY;
				}
				else {
					downSampledPos.z += stepZ;
					if (downSampledPos.z == downSampledtjustOutZ)
						return color;
					downSampledtMaxZ += downSampledtDeltaZ;
				}
			}
		}
	}

	*/
		uint GetPixelFast(Ray ray) {

			float3 originalO = ray.O;
			//uint color = 0;
			uint color = backgroundColor;
			if (!IntersectCheckFast(ray))
				return color;

			int3 currentPos;// = IntersectGrid(ray);
			currentPos.x = std::floor(ray.O.x);
			currentPos.y = std::floor(ray.O.y);
			currentPos.z = std::floor(ray.O.z);

			if (currentPos.x < bbox[0] -1 || currentPos.x > bbox[3] + 1 || currentPos.y < bbox[1] -1 || currentPos.y > bbox[4] + 1 || currentPos.z < bbox[2] - 1 || currentPos.z > bbox[5] + 1)
				return color;

			int3 downSampledPos;
			downSampledPos.x = currentPos.x / gridWidthBlock;
			downSampledPos.y = currentPos.y / gridHeightBlock;
			downSampledPos.z = currentPos.z / gridDepthBlock;

			float downSampledtDeltaX = (ray.D.x == 0.0f) ? std::numeric_limits<float>::max() : abs(gridWidthBlock / ray.D.x);
			float downSampledtDeltaY = (ray.D.y == 0.0f) ? std::numeric_limits<float>::max() : abs(gridHeightBlock / ray.D.y);
			float downSampledtDeltaZ = (ray.D.z == 0.0f) ? std::numeric_limits<float>::max() : abs(gridDepthBlock / ray.D.z);


			int stepX = (ray.D.x > 0) ? 1 : -1;
			int stepY = (ray.D.y > 0) ? 1 : -1;
			int stepZ = (ray.D.z > 0) ? 1 : -1;

			float downSampledtMaxX = downSampledtDeltaX * ((stepX > 0) ? (downSampledPos.x + 1 - ray.O.x / gridWidthBlock) : (ray.O.x / gridWidthBlock - downSampledPos.x));
			float downSampledtMaxY = downSampledtDeltaY * ((stepY > 0) ? (downSampledPos.y + 1 - ray.O.y / gridHeightBlock) : (ray.O.y / gridHeightBlock - downSampledPos.y));
			float downSampledtMaxZ = downSampledtDeltaZ * ((stepZ > 0) ? (downSampledPos.z + 1 - ray.O.z / gridDepthBlock) : (ray.O.z / gridDepthBlock - downSampledPos.z));

	
			int downSampledtjustOutX = (stepX > 0) ? ceil((float) bbox[3] / (float) gridWidthBlock) + 1 : floor((float) bbox[0] / (float) gridWidthBlock) - 1;
			int downSampledtjustOutY = (stepY > 0) ? ceil((float) bbox[4] / (float) gridHeightBlock) + 1: floor((float) bbox[1] / (float) gridHeightBlock) -1;
			int downSampledtjustOutZ = (stepZ > 0) ? ceil((float) bbox[5] / (float) gridDepthBlock) + 1: floor((float) bbox[2] / (float) gridDepthBlock) -1;

			float tDeltaX = (ray.D.x == 0.0f) ? std::numeric_limits<float>::max() : abs(1.0 / ray.D.x);
			float tDeltaY = (ray.D.y == 0.0f) ? std::numeric_limits<float>::max() : abs(1.0 / ray.D.y);
			float tDeltaZ = (ray.D.z == 0.0f) ? std::numeric_limits<float>::max() : abs(1.0 / ray.D.z);

			while (true) {

				if (downSampledPos.x >= 0 && downSampledPos.x < gridWidth / gridWidthBlock && downSampledPos.y >= 0 && downSampledPos.y < gridHeight / gridHeightBlock && downSampledPos.z >= 0 && downSampledPos.z < gridDepth / gridDepthBlock) {
					if (downSampledGrid[downSampledPos.x + gridWidth / gridWidthBlock * (downSampledPos.y + gridHeight / gridHeightBlock * downSampledPos.z)] == 1)
					{
						ray.O = originalO;
						Intersect(ray, downSampledPos.x * gridWidthBlock, downSampledPos.y * gridHeightBlock, downSampledPos.z * gridDepthBlock, (downSampledPos.x + 1) * gridWidthBlock - 1, (downSampledPos.y + 1) * gridHeightBlock - 1, (downSampledPos.z + 1) * gridDepthBlock - 1);

						currentPos.x = std::floor(ray.O.x);
						currentPos.y = std::floor(ray.O.y);
						currentPos.z = std::floor(ray.O.z);

						float tMaxX = tDeltaX * ((stepX > 0) ? (currentPos.x + 1 - ray.O.x) : (currentPos.x - ray.O.x));
						float tMaxY = tDeltaY * ((stepY > 0) ? (currentPos.y + 1 - ray.O.y) : (currentPos.y - ray.O.y));
						float tMaxZ = tDeltaZ * ((stepZ > 0) ? (currentPos.z + 1 - ray.O.z) : (currentPos.z - ray.O.z));

						int justOutX = (stepX > 0) ? (downSampledPos.x + 1) * gridWidthBlock : downSampledPos.x * gridWidthBlock - 1;
						int justOutY = (stepY > 0) ? (downSampledPos.y + 1) * gridHeightBlock : downSampledPos.y * gridHeightBlock - 1;
						int justOutZ = (stepZ > 0) ? (downSampledPos.z + 1) * gridDepthBlock : downSampledPos.z * gridDepthBlock - 1;

						while (true) {

							if (currentPos.x >= 0 && currentPos.x < gridWidth && currentPos.y >= 0 && currentPos.y < gridHeight && currentPos.z >= 0 && currentPos.z < gridDepth) {
								if (grid[currentPos.x + gridWidth * (currentPos.y + gridHeight * currentPos.z)] != color)
								{
									return grid[currentPos.x + gridWidth * (currentPos.y + gridHeight * currentPos.z)];
								}
							}

							if (tMaxX < tMaxY) {
								if (tMaxX < tMaxZ) {
									currentPos.x += stepX;
									if (currentPos.x == justOutX)
										break;
									tMaxX += tDeltaX;
								}
								else {
									currentPos.z += stepZ;
									if (currentPos.z == justOutZ)
										break;
									tMaxZ += tDeltaZ;
								}
							}
							else {
								if (tMaxY < tMaxZ) {
									currentPos.y += stepY;
									if (currentPos.y == justOutY)
										break;
									tMaxY += tDeltaY;
								}
								else {
									currentPos.z += stepZ;
									if (currentPos.z == justOutZ)
										break;
									tMaxZ += tDeltaZ;
								}
							}
						}
					}
				}

				if (downSampledtMaxX < downSampledtMaxY) {
					if (downSampledtMaxX < downSampledtMaxZ) {
						downSampledPos.x += stepX;
						if (downSampledPos.x == downSampledtjustOutX)
							return color;
						downSampledtMaxX += downSampledtDeltaX;
					}
					else {
						downSampledPos.z += stepZ;
						if (downSampledPos.z == downSampledtjustOutZ)
							return color;
						downSampledtMaxZ += downSampledtDeltaZ;
					}
				}
				else {
					if (downSampledtMaxY < downSampledtMaxZ) {
						downSampledPos.y += stepY;
						if (downSampledPos.y == downSampledtjustOutY)
							return color;
						downSampledtMaxY += downSampledtDeltaY;
					}
					else {
						downSampledPos.z += stepZ;
						if (downSampledPos.z == downSampledtjustOutZ)
							return color;
						downSampledtMaxZ += downSampledtDeltaZ;
					}
				}
			}
		}

		bool IntersectCheck(Ray& ray) {

			float tx1 = (0 - ray.O.x) * ray.rD.x, tx2 = (gridWidth - ray.O.x) * ray.rD.x;
			float tmin = min(tx1, tx2), tmax = max(tx1, tx2);
			float ty1 = (0 - ray.O.y) * ray.rD.y, ty2 = (gridHeight - ray.O.y) * ray.rD.y;
			tmin = max(tmin, min(ty1, ty2)), tmax = min(tmax, max(ty1, ty2));
			float tz1 = (0 - ray.O.z) * ray.rD.z, tz2 = (gridDepth - ray.O.z) * ray.rD.z;
			tmin = max(tmin, min(tz1, tz2)), tmax = min(tmax, max(tz1, tz2));

			ray.O += ray.D * tmin;
			return tmax >= tmin && tmax > 0;
		}

		bool IntersectCheckFast(Ray& ray) {

			float tx1 = (bbox[0] - ray.O.x) * ray.rD.x, tx2 = (bbox[3] - ray.O.x) * ray.rD.x;
			float tmin = min(tx1, tx2), tmax = max(tx1, tx2);
			float ty1 = (bbox[1] - ray.O.y) * ray.rD.y, ty2 = (bbox[4] - ray.O.y) * ray.rD.y;
			tmin = max(tmin, min(ty1, ty2)), tmax = min(tmax, max(ty1, ty2));
			float tz1 = (bbox[2] - ray.O.z) * ray.rD.z, tz2 = (bbox[5] - ray.O.z) * ray.rD.z;
			tmin = max(tmin, min(tz1, tz2)), tmax = min(tmax, max(tz1, tz2));

			ray.O += ray.D * tmin;
			return tmax >= tmin && tmax > 0;
		}

		void Intersect(Ray& ray, int xMin, int yMin, int zMin, int xMax, int yMax, int zMax) {

			float tx1 = (xMin - ray.O.x) * ray.rD.x, tx2 = (xMax - ray.O.x) * ray.rD.x;
			float tmin = min(tx1, tx2), tmax = max(tx1, tx2);
			float ty1 = (yMin - ray.O.y) * ray.rD.y, ty2 = (yMax - ray.O.y) * ray.rD.y;
			tmin = max(tmin, min(ty1, ty2)), tmax = min(tmax, max(ty1, ty2));
			float tz1 = (zMin - ray.O.z) * ray.rD.z, tz2 = (zMax - ray.O.z) * ray.rD.z;
			tmin = max(tmin, min(tz1, tz2)), tmax = min(tmax, max(tz1, tz2));

			ray.O += ray.D * tmin;
		}


		void AddPoint(float x, float y, float z, uint color, PrecisionLevel precisionLevel, bool randomColor = false)
		{
			uint newColor = color;
			if (randomColor && globalPointIdx < basicColors.size())
				newColor = basicColors[globalPointIdx];

			Point newPoint = Point(x, y, z, precisionLevel, newColor);

			hidePoint[newPoint.Id()] = false;

			if (totalPoints == 0 || totalRotations == 0)
			{
				points.push_back(newPoint);
				pointsOrder.push_back(newPoint.GetID());
			}
			else
			{
				int* p1Idxs = new int[totalRotations];
				int* p2Idxs = new int[totalRotations];
				for (int i = 0; i < totalRotations; i++)
				{
					bool p1 = false, p2 = false;
					for (int j = 0; j < totalPoints; j++)
					{
						if (rotations[i].p1 == &points[j])
						{
							p1Idxs[i] = j;

							if (p2)
								break;

							p1 = true;
						}
						else if (rotations[i].p2 == &points[j])
						{
							p2Idxs[i] = j;

							if (p1)
								break;

							p2 = true;
						}
					}
				}

				points.push_back(newPoint);
				pointsOrder.push_back(newPoint.GetID());

				for (int i = 0; i < totalRotations; i++)
				{
					rotations[i].p1 = &points[p1Idxs[i]];
					rotations[i].p2 = &points[p2Idxs[i]];
				}
			}

			totalPoints++;
		}

		void AddRotation(Point* p1, Point* p2, PrecisionLevel precisionLevel, float rotation = 90)
		{
			Rotation newRotation = Rotation(p1, p2, precisionLevel, rotation);
			rotations.push_back(newRotation);
			totalRotations++;
		}

		void AddRotation(Rotation rotation)
		{
			rotations.push_back(rotation);
			totalRotations++;
		}


		// MAY HAVE SOME BUGGS WHEN DELETING POINTS THAT HAVE A ROTATION
		void DeletePoint(Point* delPoint)
		{
			if (totalPoints == 1)
			{
				points.clear();
				pointsOrder.clear();

				globalPointIdx = 0;
			}
			else
			{
				// Find the index of the element to delete
				int indexToDelete;
				for (int i = 0; i < totalPoints; i++) {
					if (&points[i] == delPoint) {
						indexToDelete = i;
						break;
					}
				}

				std::list<Rotation> newRotations;

				for (int i = 0; i < totalRotations; i++)
				{
					if (rotations[i].p1 != delPoint && rotations[i].p2 != delPoint)
						newRotations.push_back(rotations[i]);
				}

				int newTotalRotations = newRotations.size();

				if (newTotalRotations == 0)
				{
					rotations.clear();
					totalRotations = 0;


					uint pointOrderIdToDelete = delPoint->GetID();
					for (auto pointOrder = pointsOrder.begin(); pointOrder != pointsOrder.end(); ++pointOrder) {
						if (*pointOrder == pointOrderIdToDelete) {
							pointsOrder.erase(pointOrder);
							break;
						}
					}

					points.erase(points.begin() + indexToDelete);
				}
				else
				{
					if (newTotalRotations != totalRotations)
					{
						rotations.clear();

						for (auto rotation = newRotations.begin(); rotation != newRotations.end(); ++rotation) {
							rotations.push_back(*rotation);
						}

						totalRotations = newTotalRotations;
					}

					int* p1Idxs = new int[totalRotations];
					int* p2Idxs = new int[totalRotations];
					for (int i = 0; i < totalRotations; i++)
					{
						bool p1 = false, p2 = false;
						for (int j = 0; j < totalPoints; j++)
						{
							if (rotations[i].p1 == &points[j])
							{
								if (j < indexToDelete)
									p1Idxs[i] = j;
								else if (j > indexToDelete)
									p1Idxs[i] = j - 1;
								else
									std::cout << "error: point is already deleted";

								if (p2)
									break;

								p1 = true;
							}
							else if (rotations[i].p2 == &points[j])
							{
								if (j < indexToDelete)
									p2Idxs[i] = j;
								else if (j > indexToDelete)
									p2Idxs[i] = j - 1;
								else
									std::cout << "error: point is already deleted";

								if (p1)
									break;

								p2 = true;
							}
						}
					}

					uint pointOrderIdToDelete = delPoint->GetID();
					for (auto pointOrder = pointsOrder.begin(); pointOrder != pointsOrder.end(); ++pointOrder) {
						if (*pointOrder == pointOrderIdToDelete) {
							pointsOrder.erase(pointOrder);
							break;
						}
					}

					points.erase(points.begin() + indexToDelete);

					// Reference the points
					for (int i = 0; i < totalRotations; i++)
					{
						rotations[i].p1 = &points[p1Idxs[i]];
						rotations[i].p2 = &points[p2Idxs[i]];
					}
				}
			}

			totalPoints--;
		}

		void DeleteRotation(Rotation* delRotation)
		{
			if (totalRotations == 1)
			{
				rotations.clear();
			}
			else
			{
				// Find the index of the element to delete
				int indexToDelete;
				for (int i = 0; i < totalRotations; i++) {
					if (&rotations[i] == delRotation) {
						indexToDelete = i;
						break;
					}
				}

				rotations.erase(rotations.begin() + indexToDelete);

				if (startRotation == indexToDelete && indexToDelete == totalRotations - 1)
					startRotation = 0;
				else if (startRotation > indexToDelete)
					startRotation--;
			}

			totalRotations--;
		}

		void HidePoint(uint id)
		{
			hidePoint[id] = !hidePoint[id];
		}

		bool HiddenPoint(uint id)
		{
			return hidePoint[id];
		}

		void CenterPoints(PrecisionLevel precisionLevel)
		{
			if (!points.empty())
			{
				float3 centroid = float3(0);

				for (int i = 0; i < totalPoints; i++)
				{
					centroid += points[i].Position3D();
				}

				centroid /= totalPoints;
				float3 offset = float3(gridWidth / 2, gridHeight / 2, gridDepth / 2) - centroid;

				int3 rounded_offset = int3(std::round(offset.x), std::round(offset.y), std::round(offset.z));
				for (int i = 0; i < totalPoints; i++)
				{
					points[i].Translate(rounded_offset, precisionLevel);
				}
			}
		}

		float3 Center()
		{
			if (!points.empty())
			{
				float3 centroid = float3(0);

				for (int i = 0; i < totalPoints; i++)
				{
					centroid += points[i].Position3D();
				}

				centroid /= totalPoints;
				return centroid;
			}
		}

		void Import(std::list<Point> points, std::list<Rotation> rotations)
		{
			totalPoints = points.size();
			totalRotations = rotations.size();

			if (totalPoints == 0 || totalRotations == 0)
				return;

			this->points.clear();

			for (auto point = points.begin(); point != points.end(); ++point) {
				this->points.push_back(*point);
				this->pointsOrder.push_back(this->points.back().GetID());
			}

			this->rotations.clear();
			int j = 0;
			for (auto rotation = rotations.begin(); rotation != rotations.end(); ++rotation) {
				this->rotations.push_back(*rotation);
				this->rotations[j].p1 = &this->points[this->rotations[j].p1Idx];
				this->rotations[j].p2 = &this->points[this->rotations[j].p2Idx];
				j++;
			}

		}

		Rotation CurrentRotation()
		{
			int currentRotation = (currentIteration + startRotation) % totalRotations;
			return rotations[currentRotation];
		}
		void Iterate(Rotation::RotationMethod rotationMethod)
		{
			Rotation rotation = CurrentRotation();

			//float3 curentCentroid = Center();
			//std::cout << "old distance = " << pow(distance(rotation.p1->Position3D(), curentCentroid), 2) + pow(distance(rotation.p2->Position3D(), curentCentroid), 2) << std::endl;
			
			if(rotationMethod == Rotation::RotationMethod::TwoDRotation)
				rotation.Rotate();
			else if (rotationMethod == Rotation::RotationMethod::CenterMidPoint)
				rotation.RotateCenterMidPoint(Center());
			else if (rotationMethod == Rotation::RotationMethod::SingleAxisP1)
				rotation.Rotate3DSingleAxisP1(Center());
			else if (rotationMethod == Rotation::RotationMethod::SingleAxisP2)
				rotation.Rotate3DSingleAxisP2(Center());
			else if (rotationMethod == Rotation::RotationMethod::MultiAxis)
				rotation.Rotate3DMultiAxis(Center());
			else if (rotationMethod == Rotation::RotationMethod::PlaneNormal)
				rotation.Rotate3DPlaneNormal(Center());
			else if (rotationMethod == Rotation::RotationMethod::PlaneCircle)
				rotation.Rotate3DPlaneCircle(Center());
			else if (rotationMethod == Rotation::RotationMethod::PlaneNormalCircle)
				rotation.Rotate3DPlaneNormalCircle(Center());
			

			

			//std::cout << "Current centroid = " << curentCentroid.x << " | " << curentCentroid.y << " | " << curentCentroid.z << std::endl;
			//std::cout << "new distance = " << pow(distance(rotation.p1->Position3D(), curentCentroid), 2) + pow(distance(rotation.p2->Position3D(), curentCentroid), 2) << std::endl;

			// Update bmin
			bbox[0] = min(bbox[0], static_cast<int>(min(round(Calculator::returnFloat(rotation.p1->GetX())), round(Calculator::returnFloat(rotation.p2->GetX())))));
			bbox[1] = min(bbox[1], static_cast<int>(min(round(Calculator::returnFloat(rotation.p1->GetY())), round(Calculator::returnFloat(rotation.p2->GetY())))));
			bbox[2] = min(bbox[2], static_cast<int>(min(round(Calculator::returnFloat(rotation.p1->GetZ())), round(Calculator::returnFloat(rotation.p2->GetZ())))));

			// Update bmax
			bbox[3] = max(bbox[3], static_cast<int>(max(round(Calculator::returnFloat(rotation.p1->GetX())), round(Calculator::returnFloat(rotation.p2->GetX())))));
			bbox[4] = max(bbox[4], static_cast<int>(max(round(Calculator::returnFloat(rotation.p1->GetY())), round(Calculator::returnFloat(rotation.p2->GetY())))));
			bbox[5] = max(bbox[5], static_cast<int>(max(round(Calculator::returnFloat(rotation.p1->GetZ())), round(Calculator::returnFloat(rotation.p2->GetZ())))));

			Fill(rotation.p1, rotation.p2);

			currentIteration++;
		}

		void Iterate(Rotation::RotationMethod rotationMethod, int i)
		{
			Rotation rotation = CurrentRotation();

			if (rotationMethod == Rotation::RotationMethod::TwoDRotation)
				rotation.Rotate();
			else if (rotationMethod == Rotation::RotationMethod::CenterMidPoint)
				rotation.RotateCenterMidPoint(Center());
			else if (rotationMethod == Rotation::RotationMethod::SingleAxisP1)
				rotation.Rotate3DSingleAxisP1(Center());
			else if (rotationMethod == Rotation::RotationMethod::SingleAxisP2)
				rotation.Rotate3DSingleAxisP2(Center());
			else if (rotationMethod == Rotation::RotationMethod::MultiAxis)
				rotation.Rotate3DMultiAxis(Center());
			else if (rotationMethod == Rotation::RotationMethod::PlaneNormal)
				rotation.Rotate3DPlaneNormal(Center());

			// Update bmin
			bbox[0] = min(bbox[0], static_cast<int>(min(round(Calculator::returnFloat(rotation.p1->GetX())), round(Calculator::returnFloat(rotation.p2->GetX())))));
			bbox[1] = min(bbox[1], static_cast<int>(min(round(Calculator::returnFloat(rotation.p1->GetY())), round(Calculator::returnFloat(rotation.p2->GetY())))));
			bbox[2] = min(bbox[2], static_cast<int>(min(round(Calculator::returnFloat(rotation.p1->GetZ())), round(Calculator::returnFloat(rotation.p2->GetZ())))));

			// Update bmax
			bbox[3] = max(bbox[3], static_cast<int>(max(round(Calculator::returnFloat(rotation.p1->GetX())), round(Calculator::returnFloat(rotation.p2->GetX())))));
			bbox[4] = max(bbox[4], static_cast<int>(max(round(Calculator::returnFloat(rotation.p1->GetY())), round(Calculator::returnFloat(rotation.p2->GetY())))));
			bbox[5] = max(bbox[5], static_cast<int>(max(round(Calculator::returnFloat(rotation.p1->GetZ())), round(Calculator::returnFloat(rotation.p2->GetZ())))));

			Fill(rotation.p1, rotation.p2, i);

			currentIteration++;
		}
		/* TODO MISSING Z
		void AnimatedRotation(float frame, Number originalP1PosX, Number originalP1PosY, Number originalP2PosX, Number originalP2PosY, PrecisionLevel precisionLevel, bool finish)
		{
			Rotation rotation = CurrentRotation();

			if (!finish)
				rotation.Rotate(originalP1PosX, originalP1PosY, originalP2PosX, originalP2PosY, precisionLevel, frame);
			else
			{
				rotation.Rotate(originalP1PosX, originalP1PosY, originalP2PosX, originalP2PosY, precisionLevel, 1);
				Fill(rotation.p1, rotation.p2);
				currentIteration++;
			}
		}
		*/

		void Fill(Point* p1, Point* p2)
		{
			// PCHECK
			float3 p1_pos = p1->Position3D();
			float3 p2_pos = p2->Position3D();

			if (plotState == LIFO)
			{
				FillWithCircleLIFO(round(p1_pos.x), round(p1_pos.y), round(p1_pos.z), p1->color);
				FillWithCircleLIFO(round(p2_pos.x), round(p2_pos.y), round(p2_pos.z), p2->color);
			}
			else if (plotState == FIFO)
			{
				FillWithCircleFIFO(round(p1_pos.x), round(p1_pos.y), round(p1_pos.z), p1->color);
				FillWithCircleFIFO(round(p2_pos.x),  round(p2_pos.y), round(p2_pos.z), p2->color);
			}
		}

		void Fill(Point* p1, Point* p2, int i)
		{
			// PCHECK
			float3 p1_pos = p1->Position3D();
			float3 p2_pos = p2->Position3D();

			int3 rounded_p1_pos = int3(round(p1_pos.x), round(p1_pos.y), round(p1_pos.z));
			int3 rounded_p2_pos = int3(round(p2_pos.x), round(p2_pos.y), round(p2_pos.z));

			if (plotState == LIFO)
			{
				FillWithCircleLIFO(rounded_p1_pos.x, rounded_p1_pos.y, rounded_p1_pos.z, p1->color);
				FillWithCircleLIFO(rounded_p2_pos.x, rounded_p2_pos.y, rounded_p2_pos.z, p2->color);
			}
			else if (plotState == FIFO)
			{
				FillWithCircleFIFO(rounded_p1_pos.x, rounded_p1_pos.y, rounded_p1_pos.z, p1->color);
				FillWithCircleFIFO(rounded_p2_pos.x, rounded_p2_pos.y, rounded_p2_pos.z, p2->color);
			}

			int offset_p1 = i * 4;
			int offset_p2 = i * 4 + 4;
			updatedCells[offset_p1] = rounded_p1_pos.x;
			updatedCells[offset_p1 + 1] = rounded_p1_pos.y;
			updatedCells[offset_p1 + 2] = rounded_p1_pos.z;
			updatedCells[offset_p1 + 3] = p1->color & 0xFFFFFFFF;

			updatedCells[offset_p2] = rounded_p2_pos.x;
			updatedCells[offset_p2 + 1] = rounded_p2_pos.y;
			updatedCells[offset_p2 + 2] = rounded_p2_pos.z;
			updatedCells[offset_p2 + 3] = p2->color & 0xFFFFFFFF;
		}


		void FillWithCircleLIFO(int center_x, int center_y, int center_z, uint color)
		{
			for (int x = center_x - pointSize; x <= center_x + pointSize; x++) {
				for (int y = center_y - pointSize; y <= center_y + pointSize; y++) {
					for (int z = center_z - pointSize; z <= center_z + pointSize; z++) {
						// Check if the coords are within the bounds of the grid 
						if (x >= 0 && x < gridWidth && y >= 0 && y < gridHeight && z >= 0 && z < gridDepth) {
							// If the distance is less than or equal to the radius, mark the point as part of the sphere
							if (distance(x, y, z, center_x, center_y, center_z) <= pointSize) {
								grid[x + gridWidth * (y + gridHeight * z)] = color;
								downSampledGrid[(x / gridWidthBlock) + (gridWidth / gridWidthBlock) * ((y / gridHeightBlock) + (gridHeight / gridHeightBlock) * (z / gridDepthBlock))] = 1;
							}
						}
					}
				}
			}
		}

		void FillWithCircleFIFO(int center_x, int center_y, int center_z, uint color)
		{
			for (int x = center_x - pointSize; x <= center_x + pointSize; x++) {
				for (int y = center_y - pointSize; y <= center_y + pointSize; y++) {
					for (int z = center_z - pointSize; z <= center_z + pointSize; z++) {
						// Check if the coords are within the bounds of the grid 
						if (x >= 0 && x < gridWidth && y >= 0 && y < gridHeight && z >= 0 && z < gridDepth) {
							// SKIP plot if another point has drawn the pixel.
							if (grid[x + gridWidth * (y + gridHeight * z)] == backgroundColor)
							{
								// If the distance is less than or equal to the radius, mark the point as part of the circle
								if (distance(x, y, z, center_x, center_y, center_z) <= pointSize) {
									grid[x + gridWidth * (y + gridHeight * z)] = color;
									downSampledGrid[(x / gridWidthBlock) + (gridWidth / gridWidthBlock) * ((y / gridHeightBlock) + (gridHeight / gridHeightBlock) * (z / gridDepthBlock))] = 1;
								}
							}

						}
					}
				}
			}
		}

		void ChangePointSize(int newSize)
		{
			pointSize = newSize;
		}

		void UpdatePrecision(PrecisionLevel precisionLevel)
		{
			for (int i = 0; i < totalPoints; i++)
			{
				points[i].UpdatePrecision(precisionLevel);
			}

			for (int i = 0; i < totalRotations; i++)
			{
				rotations[i].PreCalcRotationAngles(precisionLevel);
			}

		}

		void UpdatePlotState(PlotState newPlotState)
		{
			plotState = newPlotState;
		}

		void Clear(bool useGPU)
		{
			memset(grid, backgroundColor, gridWidth * gridHeight * gridDepth * sizeof(uint));
			memset(downSampledGrid, 0, (gridWidth / gridWidthBlock) * (gridHeight / gridHeightBlock) * (gridDepth / gridDepthBlock) * sizeof(uint));
			bbox = new int[6]{ std::numeric_limits<int>::max(), std::numeric_limits<int>::max(), std::numeric_limits<int>::max(), -1, -1, -1 };
			memset(updatedCells, 0, 1000 * 2 * 4 * sizeof(int));

			/*
			if (totalPoints > 0)
			{
				for (int i = 0; i < totalPoints; i++)
					points[i].Clear(gridWidth, gridHeight);
			}
			*/

			if (totalRotations != 0)
				startRotation = (currentIteration + startRotation) % totalRotations;
			else
				startRotation = 0;

			currentIteration = 0;

			if (useGPU)
			{
				bboxBuffer->CopyToDevice(false);
				gridBuffer->CopyToDevice(false);
				downSampledGridBuffer->CopyToDevice(true);
			}
		}

		void Reset(bool useGPU)
		{
			points.clear();
			pointsOrder.clear();

			rotations.clear();

			totalPoints = 0;
			totalRotations = 0;

			globalPointIdx = 0;

			memset(hidePoint, false, 256 * sizeof(bool));

			Clear(useGPU);
		}
		void Export(std::string fileName, string dir = "3D plots/") {
			if (dir != "3D plots/")
				dir = "3D plots/" + dir + "/";

			if (!std::filesystem::exists(dir))
				std::filesystem::create_directory(dir);

			std::ofstream objFile(dir + fileName + ".obj");

			int vertCount = 0;

			// Loop through each cell in the grid
			for (int z = 0; z < gridDepth; ++z) {
				for (int y = 0; y < gridHeight; ++y) {
					for (int x = 0; x < gridWidth; ++x) {
						// If the cell is not empty
						if (grid[x + gridWidth * (y + gridHeight * z)] != backgroundColor) {

							// Color conversion
							uint color = grid[x + gridWidth * (y + gridHeight * z)];
							float r = static_cast<float>((color & 0xFF0000) >> 16) / 255.0f;
							float g = static_cast<float>((color & 0x00FF00) >> 8) / 255.0f;
							float b = static_cast<float>(color & 0x0000FF) / 255.0f;

							// Create separate vertices for each face of the cube
							for (int face = 0; face < 6; ++face) {
								int vertStart = vertCount * 4 + 1; // Vertices start at 1 and there are 4 vertices per face

								// Write vertices for each face
								switch (face) {
								case 0: // front
									writeFace(objFile, vertStart, x, x + 1, y, y + 1, z, z, r, g, b);
									break;
								case 1: // back
									writeFace(objFile, vertStart, x, x + 1, y, y + 1, z + 1, z + 1, r, g, b);
									break;
								case 2: // left
									writeFace(objFile, vertStart, x, x, y, y + 1, z, z + 1, r, g, b);
									break;
								case 3: // right
									writeFace(objFile, vertStart, x + 1, x + 1, y, y + 1, z, z + 1, r, g, b);
									break;
								case 4: // bottom
									writeFace(objFile, vertStart, x, x + 1, y, y, z, z + 1, r, g, b);
									break;
								case 5: // top
									writeFace(objFile, vertStart, x, x + 1, y + 1, y + 1, z, z + 1, r, g, b);
									break;
								}

								vertCount++;
							}
						}
					}
				}
			}

			objFile.close();
		}

		void writeFace(std::ofstream& objFile, int vertStart, float x1, float x2, float y1, float y2, float z1, float z2, float r, float g, float b) {
			objFile << "v " << x1 << " " << y1 << " " << z1 << " " << r << " " << g << " " << b << "\n";
			objFile << "v " << x2 << " " << y1 << " " << z1 << " " << r << " " << g << " " << b << "\n";
			objFile << "v " << x2 << " " << y2 << " " << z2 << " " << r << " " << g << " " << b << "\n";
			objFile << "v " << x1 << " " << y2 << " " << z2 << " " << r << " " << g << " " << b << "\n";

			objFile << "f " << vertStart << " " << vertStart + 1 << " " << vertStart + 2 << " " << vertStart + 3 << "\n";
		}

		uint* grid;
		uint* downSampledGrid;
		int* bbox;
		int* updatedCells;

		int gridWidth, gridHeight, gridDepth;
		int gridWidthBlock = 10, gridHeightBlock = 10, gridDepthBlock = 10;
		int pointSize;

		std::vector<Point> points;
		std::vector<uint> pointsOrder;

		std::vector<Rotation> rotations;

		int totalPoints;
		int totalRotations;

		int startRotation;
		int currentIteration;

		// GPU
		static inline Buffer* gridBuffer;
		static inline Buffer* downSampledGridBuffer;
		static inline Buffer* bboxBuffer;
		static inline Buffer* updatedCellsBuffer;
	};
}
