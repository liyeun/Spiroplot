__kernel void FillWithCircleLIFO(__global int* cellInfos,
__global uint* grid, __global uint* downSampledGrid,
int gridWidth, int gridHeight, int gridDepth, int pointSize,
int gridWidthBlock, int gridHeightBlock, int gridDepthBlock)
{
    int threadId = get_global_id(0) * 4;

    int center_x = cellInfos[threadId];
    int center_y = cellInfos[threadId + 1];
    int center_z = cellInfos[threadId + 2]; 
    uint color = cellInfos[threadId + 3] & 0xFFFFFFFF;

    for (int x = center_x - pointSize; x <= center_x + pointSize; x++) {
        for (int y = center_y - pointSize; y <= center_y + pointSize; y++) {
            for (int z = center_z - pointSize; z <= center_z + pointSize; z++) {
                // Check if the coords are within the bounds of the grid 
                if (x >= 0 && x < gridWidth && y >= 0 && y < gridHeight && z >= 0 && z < gridDepth) {
                    // If the distance is less than or equal to the radius, mark the point as part of the sphere
                    if (sqrt((float)((center_x - x) * (center_x - x) + (center_y - y) * (center_y - y) + (center_z - z) * (center_z - z))) <= pointSize) {
                        grid[x + gridWidth * (y + gridHeight * z)] = color;
                        downSampledGrid[(x / gridWidthBlock) + (gridWidth / gridWidthBlock) * ((y / gridHeightBlock) + (gridHeight / gridHeightBlock) * (z / gridDepthBlock))] = 1;
                    }
                }
            }
        }
    }
}

__kernel void FillWithCircleFIFO(__global int* cellInfos,
__global uint* grid, __global uint* downSampledGrid,
int gridWidth, int gridHeight, int gridDepth, int pointSize,
int gridWidthBlock, int gridHeightBlock, int gridDepthBlock,
__global uint* backgroundColor)
{
    int threadId = get_global_id(0) * 4;

    int center_x = cellInfos[threadId];
    int center_y = cellInfos[threadId + 1];
    int center_z = cellInfos[threadId + 2]; 
    uint color = cellInfos[threadId + 3] & 0xFFFFFFFF;

    for (int x = center_x - pointSize; x <= center_x + pointSize; x++) {
        for (int y = center_y - pointSize; y <= center_y + pointSize; y++) {
            for (int z = center_z - pointSize; z <= center_z + pointSize; z++) {
                // Check if the coords are within the bounds of the grid 
                if (x >= 0 && x < gridWidth && y >= 0 && y < gridHeight && z >= 0 && z < gridDepth) {
                    // SKIP plot if another point has drawn the pixel.
                    if (grid[x + gridWidth * (y + gridHeight * z)] == backgroundColor)
                    {
                        // If the distance is less than or equal to the radius, mark the point as part of the sphere
                        if (sqrt((float)((center_x - x) * (center_x - x) + (center_y - y) * (center_y - y) + (center_z - z) * (center_z - z))) <= pointSize) {
                            grid[x + gridWidth * (y + gridHeight * z)] = color;
                            downSampledGrid[(x / gridWidthBlock) + (gridWidth / gridWidthBlock) * ((y / gridHeightBlock) + (gridHeight / gridHeightBlock) * (z / gridDepthBlock))] = 1;
                        }
                    }
                }
            }
        }
    }
}